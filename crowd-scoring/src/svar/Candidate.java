/*
 * Requires 	: Zipped file containing candidate information
 * 
 * Produces 	: One candidate object consisting of both TIO information and other information
 * 
 * Methods		:	1. Candidate.parseSerializedTIO()
 * 					
 */

package svar;
import java.io.*;
import java.util.*;
//import org.lorecraft.phparser.SerializedPhpParser;
//mohit change
import org.lorecraft.phparser.SerializedPhpParser;

public class Candidate {
	public String candidateID;
	String sourcePath;
	String phoneNumber;
	String allowedModules;
	public String currentModule;
	String testformNumber;
	String amcatID;
	String passKey;
	String noisyStatus;
	int isMobile;
	boolean isAMCAT = false;
	boolean isAdaptive = false;
	public HashMap<Integer, ArrayList<Integer>> categoryVersusQuestionID;
	HashMap<Integer, Integer> categoryVersusQuestionID2;
	Object TIO;
	Property property = new Property();
	HashMap<String, String> candidateTag;
	public String deliverModuleVersion;
	
	
	Candidate(String inputCandidateID){
		candidateID = inputCandidateID;
		categoryVersusQuestionID = new HashMap<Integer, ArrayList<Integer>>();
		categoryVersusQuestionID2 = new HashMap<Integer, Integer>();
		candidateTag = new HashMap<String, String>();
		deliverModuleVersion = "-1";
	}
	
	int parseTIO(){
		UnserializeTIO obj = new UnserializeTIO( (property.useNoiseReducedFiles == 1)?property.noiseReducedFilesLocation:property.unzippedFilesLocation,candidateID);
		try {
			TIO = obj.retrieveObject();
			subCatVersusUtterance(TIO); //add by ankit @ 13 feb to calculate whole tio thing at starting
			NumberDetailsFromDB checkMobObj = new NumberDetailsFromDB();
			phoneNumber = checkMobObj.getFinalNumber(parsePhoneNumberFromTIO(obj.tioContent));
			isMobile = checkMobObj.getPhoneNumberDetail(phoneNumber);
			
			/*
			 * Commented by ankit for check the mobile number from the VS data base
			 */
			//checkMobileNumber(parsePhoneNumberFromTIO(obj.tioContent);
			
			allowedModules = getAllowedModules();
			currentModule = getCurrentModule();
			testformNumber = getTestformNumber();
			amcatID = getamcatID();
			passKey = getPassKey();
			isAdaptive = getIsAdaptive(obj);
			candidateTag = getCandidateTag();
			deliverModuleVersion = getModuleVersionID();
			getModuleVariables(currentModule, deliverModuleVersion);
			//System.out.println(TIO);
			return 1;
		} catch (Exception e) {
			System.out.println("Error in retrieving object\n");
			e.printStackTrace();
			
			try{
				/*FileWriter fstream = new FileWriter("/mnt/Disk-2/SVAR/errorLog.txt");
				fstream.append("Candidate ID : "+candidateID+"\n Error : Parsing TIO\n ---------------------- \n");
				fstream.close();*/
				DatabaseWrite dbObj = new DatabaseWrite();
				dbObj.writeDB(this, -2, "-2");
				
			}
			catch(Exception e1){
				System.out.println("Exception in exception! Unable to create Error log");
				e1.printStackTrace();
			}
			
			e.printStackTrace();
			return 0;
		}
		
	}
//	System.out.println(((Map)((Map)((SerializedPhpParser.PhpObject)result).attributes.get("testResults")).get(0)).get("questionID"));
	boolean checkMobile(String number){
		//number = "919896123456";
		try {
			
			boolean errFlag = false;
			if(number.length() != 10){
				if(number.length() == 11){
					if(number.charAt(0)=='0')
						number = number.substring(1);
					else{
						System.out.println("ERROR IN PHONE NUMBER : 11 digit incorrect detected");
						errFlag = true;
					}
				}
				else
					if(number.length() == 12){
						if(number.substring(0, 2).equals("91"))
							number = number.substring(2);
						else{
							System.out.println("ERROR IN PHONE NUMBER : 12 digit incorrect detected");
							errFlag = true;
						}
					}
				
			}
			if(!errFlag){
				if(property.listMobiles.contains(number.substring(0, 4))){
					phoneNumber = number;
					return true;
				}	
				else{
					phoneNumber = number;
					return false;					
				}
			}
		}
		catch(Exception e){
			System.out.println("There's an error in the reduction in noise in the files\n Error number : "+candidateID);
			e.printStackTrace();
		}
		return true;
	}
	String parsePhoneNumberFromTIO(String line){
		String token = "agi_callerid";
		int idx1 = line.indexOf(token);
		int idx2 = line.indexOf("\"", token.length()+idx1+1);
		int idx3 = line.indexOf("\"", idx2+1);
		return line.substring(idx2+1, idx3);
	}
	
	String getAllowedModules(){
		StringBuffer diffModules = new StringBuffer();
		Map modules = ((Map)((SerializedPhpParser.PhpObject)TIO).attributes.get("allowedTestModules"));
		Iterator<Map.Entry> it = modules.entrySet().iterator();
		int run = 1;
		while(it.hasNext()){
			Map.Entry entry = (Map.Entry)it.next();
			if(run == 1)
				diffModules.append(entry.getValue());
			else
				diffModules.append("_"+entry.getValue());
			run ++;
		}
		return diffModules.toString();
	}
	
	String getCurrentModule(){
		String moduleID = ((SerializedPhpParser.PhpObject)TIO).attributes.get("moduleID").toString();
		return moduleID;
	}
	
	void getModuleVariables(String module, String moduleVersion){
		Property propObj = new Property();
		propObj.moduleVariables(module, moduleVersion);
	}
	String getTestformNumber(){
		String testformNumber = ((SerializedPhpParser.PhpObject)TIO).attributes.get("testformNumber").toString();
		return testformNumber;
	}
	
	String getamcatID(){
		if(((SerializedPhpParser.PhpObject)TIO).attributes.containsKey("amcatID"))
		{
		String amcatID = ((SerializedPhpParser.PhpObject)TIO).attributes.get("amcatID").toString();
		if(amcatID.equalsIgnoreCase("NULL") || amcatID.trim().equals("") || amcatID == null)
			return "1111111";
		return amcatID;
		}else
			return "1111111";
	}
	
	String getPassKey(){
		if(((SerializedPhpParser.PhpObject)TIO).attributes.containsKey("passKey"))
		{
		String passKey = "";
		try	
		{
		passKey = ((SerializedPhpParser.PhpObject)TIO).attributes.get("passKey").toString();
		}catch(Exception e)
		{
			passKey = "0000";	
		}
		if(passKey.equalsIgnoreCase("NULL") || passKey == null || passKey.trim().equals(""))
			return "0000";
		return passKey;
		}else
			return "0000";
	}
	
	boolean getIsAdaptive(UnserializeTIO obj){
		boolean adaptive = false;
		try	
		{
			String token = "isAdaptive";
			String line = obj.tioContent;
			int idx1 = line.indexOf(token);
			int idx2 = line.indexOf("\"", token.length()+idx1+1);
			int idx3 = line.indexOf("\"", idx2+1);
			if(line.substring(idx2+1, idx3).equals("0"))
				adaptive= false;
			else
				adaptive = true;			
		}catch(Exception e)
		{
			adaptive = false;	
		}
		return adaptive;
	}
	
	void subCatVersusUtterance(Object TIO) {
		try{
			int size = ((Map) ((SerializedPhpParser.PhpObject) TIO).attributes.get("testResults")).size();
//			int j=0;
			for (int i = 0; i < size; ++i) {
				int qID = Integer.parseInt(((Map) ((Map) ((SerializedPhpParser.PhpObject) TIO).attributes.get("testResults")).get(i)).get("questionID").toString());
				int subCatID = Integer.parseInt(((Map) ((Map) ((SerializedPhpParser.PhpObject) TIO).attributes.get("testResults")).get(i)).get("subcategoryID").toString());
				if (categoryVersusQuestionID.containsKey(subCatID)) {
					ArrayList<Integer> tempList = (ArrayList<Integer>) categoryVersusQuestionID.get(subCatID);
					tempList.add(qID);
					categoryVersusQuestionID.put(subCatID, tempList);
				} else {
					ArrayList<Integer> tempList = new ArrayList<Integer>();
					tempList.add(qID);
					categoryVersusQuestionID.put(subCatID, tempList);			
					categoryVersusQuestionID2.put(subCatID, qID);			
				}			
			}//end of for
		}
		catch(Exception e){ System.out.println("Exception in parse Tio");}
	}//end of function
	
	HashMap<String, String> getCandidateTag(){
		HashMap<String, String> candidateTag = new HashMap<String, String>();
		try
		{
			String tempTag = ((SerializedPhpParser.PhpObject)TIO).attributes.get("candidateTag").toString();
			tempTag = (String) tempTag.subSequence(1, tempTag.length()-1);
			String tagData[] = tempTag.split(",");
			for (String value : tagData) {
				String hashVal[] = value.split("=");
				if(hashVal.length > 1)
					candidateTag.put(hashVal[0].trim(), hashVal[1].trim());
				else
					candidateTag.put(hashVal[0].trim(), "");
			}
		}catch (Exception e) {
			
			System.out.println("Candidate tags are not Found");
			//e.printStackTrace();
			candidateTag.put("employeeID", "");
			candidateTag.put("name", "");
			candidateTag.put("tag1", "");
			candidateTag.put("tag2", "");
			candidateTag.put("tag3", "");
			
		}
		return candidateTag;
	}
	
	String getModuleVersionID(){
		String versionID = "-1";
		try
		{
			versionID = ((SerializedPhpParser.PhpObject)TIO).attributes.get("moduleVersionID").toString();
		}catch (Exception e) {
			
			System.out.println("module versionID not found");
			versionID = "-1";
		}
		return versionID;
	}
}

