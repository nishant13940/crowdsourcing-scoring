package svar;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import static java.nio.file.StandardWatchEventKinds.*;

import java.net.*;
import java.nio.file.*;
import java.io.*;

import org.lorecraft.phparser.SerializedPhpParser;

class PooledFileProcessing {
	private WatchService watcher;
	private WatchKey key;
    private LinkedBlockingQueue<Runnable> workQueue = new LinkedBlockingQueue<Runnable>();
    private ExecutorService executor = new ThreadPoolExecutor(
    		8, 
            8, 
            8, TimeUnit.MINUTES,//  idle thread dies in one minute
            workQueue
    );

    @SuppressWarnings("unchecked")
    static <T> WatchEvent<T> cast(WatchEvent<?> event) {
        return (WatchEvent<T>)event;
    }
    
    Property property = new Property();
    PooledFileProcessing(Path dir) throws Exception{
    	this.watcher = FileSystems.getDefault().newWatchService();
    	this.key = dir.register(watcher, ENTRY_CREATE);
    	System.out.format("register: %s\n", dir);
    }
    
    void sniffFolder(){
    	if(property.usePolling == 1){
	    	while(true){
	    		WatchKey keySniffer;
	    		try{
	    			keySniffer = watcher.take();
	    		}
	    		catch(Exception e){
	    			System.out.println("Error caught in WatchKey");
	    			keySniffer = null;
	    			e.printStackTrace();
	    		}
	    		if(keySniffer.equals(key)){
	    			for (WatchEvent<?> event: keySniffer.pollEvents()){
	    				WatchEvent.Kind<?> kind = event.kind();
	    				if(kind == OVERFLOW)
	    					System.out.println("In OVERFLOW");
	                    WatchEvent<Path> ev = cast(event);
	                    Path name = ev.context();
	                    //process(name.toString());
	                    System.out.println("Going to process - "+name.toString()+" ..");
	                    //executor.execute(new SVAR(name.toString()));
	                    System.out.println(workQueue.size() + " jobs still in queue");
	    			}
	    		}
	    		keySniffer.reset();
	    	}
    	}
    	else
	    	process();
    }

    public void process() {
    	// Shall contain method belonging to a Listener class to listen to folder to receive incoming folders' full-path
    	String fileNames	= new String();
    	
    	//Insert Server code
        //String outputLine;
    	
    	Property property = new Property();
        ServerSocket serverSocket = null;
		try {
			serverSocket = new ServerSocket(property.NETWORK_PORT);
		}
		catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		};
		
        System.out.println("Socket Created");
        String inputLine = new String();	
        
    	while(true){
    		System.out.println("Server Ready");
    		Socket clientSocket = null;
    		try {
    			clientSocket = serverSocket.accept();
    		}
    		catch (IOException e1) {
			// TODO Auto-generated catch block
    			e1.printStackTrace();
    		}
        	System.out.println("Socket Connected");
        	BufferedReader in = null;
			try {
				in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			}
			catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
        	System.out.println("Buffered Reader Created");
        	CrowdInput crowdInput = new CrowdInput();
        	try {
        		inputLine = in.readLine();
        		System.out.println(inputLine);
        		SerializedPhpParser serializedPhpParser = new SerializedPhpParser(inputLine);
        		crowdInput = new CrowdInput((Map<String, String>) serializedPhpParser.parse());
        	}
        	catch (IOException e1) {
        		e1.printStackTrace();
        	}
        	fileNames=crowdInput.candId;
        	
        	if (fileNames!=null){
        		if (fileNames.equals("close")){ 
        			closeProgram(serverSocket, clientSocket);
        			System.out.println("Exiting Program. Bye!!"); 
        			System.exit(0);
        		}
        		else{
        			DatabaseWrite dbObject = new DatabaseWrite();
        			//System.out.println(fileNames);
        			dbObject.changeScoringStatus(1,fileNames);	//Added by Vishal to implement scheduler. Status 1 means that server has received the scoring request
        			executor.execute(new SVAR(crowdInput)); //currently crowdInput sends information received from the crowd.
        			System.out.println(workQueue.size() + " jobs still in queue");
        		}
        	}
        	else{
        		System.out.println("Null Value Encountered!!");	
        	}
        	//executor.shutdown();
        }
   }
   
    public void closeProgram(ServerSocket serverSocket, Socket clientSocket ){
    	System.out.println(workQueue.size() + " jobs still in queue");
	    executor.shutdown();
	    try {
			serverSocket.close();
		}
	    catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    try {
			clientSocket.close();
		}
	    catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
	public static void main(String commandLine[]){
		Property property = new Property();
		Path dir = Paths.get(property.pollPath); 
		try{
 			System.out.println("start");
			new PooledFileProcessing(dir).sniffFolder();
			System.out.print("Processing Done");
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
}