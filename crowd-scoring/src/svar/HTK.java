package svar;

import java.io.*;
import java.lang.reflect.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

import org.lorecraft.phparser.SerializedPhpParser;
import static java.nio.file.StandardCopyOption.*;
public class HTK {
	
	Candidate candObj;
	String candidateID;
	Property property = new Property();
	
	HTK( Candidate inputCandObj){//, String inputMFCCLocation, String inputcommonLocation, String inputWavConfigFileName, String inputRECLocation, String inputListMFCCLocation) throws Exception{
		candObj			= inputCandObj;
		candidateID		= candObj.candidateID;
	}
	int createMFCC(boolean flag,HashMap<Integer, Integer> categoryVersusQuestionID2, HashMap<Integer, ArrayList<Integer>> categoryVersusQuestionID) throws Exception{
		System.out.println("In createMFCC()");
		property.moduleVariables(candObj.currentModule, candObj.deliverModuleVersion);
		String sourcePath = (property.useNoiseReducedFiles == 1)?property.noiseReducedFilesLocation:property.unzippedFilesLocation;
		String[] files = new File(sourcePath+candidateID).list();
		Process process = Runtime.getRuntime().exec("mkdir -p "+property.MFCCLocation+candidateID);
		process.waitFor();
		process = Runtime.getRuntime().exec("mkdir -p "+property.listMFCCLocation+candidateID);
		process.waitFor();
		FileWriter fstream = new FileWriter(property.listMFCCLocation+candidateID+"/"+"list.txt");
		BufferedWriter out = new BufferedWriter(fstream);
		for(int i=0; i<files.length; ++i){
			int idx = files[i].lastIndexOf('.');
			String fileName		= files[i].substring(0,idx);
			String extension 	= files[i].substring(idx+1); 
			if(extension.equalsIgnoreCase("wav")){			
				idx = fileName.indexOf('-');
				int val = Integer.parseInt(fileName.substring(0,idx));
				//System.out.println(val);
				boolean generateMFCC = false;
				for(int j =0; j< property.allowedMFCCCategory.length; j++){
					if(categoryVersusQuestionID.containsKey(Integer.parseInt(property.allowedMFCCCategory[j])) && categoryVersusQuestionID.get(Integer.parseInt(property.allowedMFCCCategory[j])).contains(val)){
						generateMFCC = true;
						break;
					}
				}		
				if(generateMFCC && val!=categoryVersusQuestionID2.get(property.LRCategory)){	
					if(flag){
						String commandLine = "HCopy -n 5 -C "+property.commonLocation+property.wavConfigFileName+" "+sourcePath+candidateID+"/"+files[i]+" "+property.MFCCLocation+candidateID+"/"+fileName+".mfcc";
						System.out.println(commandLine);
						process = Runtime.getRuntime().exec(commandLine);
					}
					out.write(property.MFCCLocation+candidateID+"/"+fileName+".mfcc\n");
				}
			}
		}
		process.waitFor();
		out.close();
		return 1;
	}
	int createREC(String type) throws Exception{
		System.out.println("In createREC()");
		
		Process process;
			//Use in case of normal model
			//String commandLine = "HVite -o N -C "+property.commonLocation+"config -H "+property.commonLocation+"hmmdefs -H "+property.commonLocation+"macros -i "+property.RECLocation+candidateID+"/align"+i+".mlf -I "+property.commonLocation+"Module_"+candObj.currentModule+"/"+"words"+testformNumber+".mlf -m "+property.commonLocation+"Dictionary"+i+".txt "+property.commonLocation+"tiedlist -S "+property.listMFCCLocation+candidateID+"/list.txt ";

		//	 In Use Original 
/*			String commandLine = "HVite -m -o N -C "+property.commonLocation+"config -H "+property.commonLocation
			+"hmmdefs -H "+property.commonLocation+"macros -i "+property.RECLocation+candidateID+"/align"+i+".mlf -I "
			+property.commonLocation+"Module_"+candObj.currentModule+"/"+"words_"+candObj.currentModule+".mlf  "
			+property.commonLocation+"Dictionary"+i+".txt "+property.commonLocation+"tiedlist" +
					" -S "+property.listMFCCLocation+candidateID+"/list.txt";
	*/
		String commandLine = "";
			
		for(int i = 0; i<property.numberAntiModels+1; ++i){
			
			//  Working  For State
			if(type.equals("state") || type.equals("stateWarped"))
			{
			commandLine = "HVite -m -f -o N -C "+property.commonLocation+"config -H "+property.commonLocation
			+"hmmdefs -H "+property.commonLocation+"macros -i "+property.RECLocation+type+"/"+candidateID+"/align"+i+".mlf -I "
			+property.mlfLocation+ candidateID +"/"+candObj.categoryVersusQuestionID.get(601).get(0).toString()+".mlf  "
			+property.dictLocation+candidateID+"/"+candObj.categoryVersusQuestionID.get(601).get(0).toString()+"_"+i+".txt "+
			property.commonLocation+"tiedlist" +" -S "+property.listMFCCLocation+candidateID+"/list.txt"; 
			}
			else if(type.equals("org") || type.equals("orgWarped"))
			{
			commandLine = "HVite -m -o N -C "+property.commonLocation+"config -H "+property.commonLocation
			+"hmmdefs -H "+property.commonLocation+"macros -i "+property.RECLocation+type+"/"+candidateID+"/align"+i+".mlf -I "
			+property.mlfLocation+ candidateID +"/"+candObj.categoryVersusQuestionID.get(601).get(0).toString()+".mlf  "
			+property.dictLocation+candidateID +"/"+candObj.categoryVersusQuestionID.get(601).get(0).toString()+"_"+i+".txt "
			+property.commonLocation+"tiedlist" +" -S "+property.listMFCCLocation+candidateID+"/list.txt"; 
			}
			// Current edited
///			S\ystem.out.println(commandLine);
			System.out.println(commandLine);
			String cwd = System.getProperty("user.dir");
			System.setProperty("user.dir",property.MFCCLocation);
			process = Runtime.getRuntime().exec(commandLine);
			InputStream in = process.getErrorStream();
			int c; StringBuffer temp = new StringBuffer();
		
			try{
			while((c = in.read()) != -1){
				System.out.print((char)c);
			}}catch(Exception e)
			{
				e.printStackTrace();
			}
			in.close();
			System.setProperty("user.dir",cwd);
		}
		return 1;
	}
	
	
	int createREC(String type,Property property) throws Exception{
		System.out.println("In createREC()");
		
		Process process;
			//Use in case of normal model
			//String commandLine = "HVite -o N -C "+property.commonLocation+"config -H "+property.commonLocation+"hmmdefs -H "+property.commonLocation+"macros -i "+property.RECLocation+candidateID+"/align"+i+".mlf -I "+property.commonLocation+"Module_"+candObj.currentModule+"/"+"words"+testformNumber+".mlf -m "+property.commonLocation+"Dictionary"+i+".txt "+property.commonLocation+"tiedlist -S "+property.listMFCCLocation+candidateID+"/list.txt ";

		//	 In Use Original 
/*			String commandLine = "HVite -m -o N -C "+property.commonLocation+"config -H "+property.commonLocation
			+"hmmdefs -H "+property.commonLocation+"macros -i "+property.RECLocation+candidateID+"/align"+i+".mlf -I "
			+property.commonLocation+"Module_"+candObj.currentModule+"/"+"words_"+candObj.currentModule+".mlf  "
			+property.commonLocation+"Dictionary"+i+".txt "+property.commonLocation+"tiedlist" +
					" -S "+property.listMFCCLocation+candidateID+"/list.txt";
	*/
		String commandLine = "";
			
		for(int i = 0; i<property.numberAntiModels+1; ++i){
			
			//  Working  For State
			if(type.equals("state") || type.equals("stateWarped"))
			{
			commandLine = "HVite -m -f -o N -C "+property.commonLocation+"config -H "+property.commonLocation
			+"hmmdefs -H "+property.commonLocation+"macros -i "+property.RECLocation+type+"/"+candidateID+"/align"+i+".mlf -I "
			+property.mlfLocation+candidateID+"/"+candObj.categoryVersusQuestionID.get(601).get(0).toString()+".mlf  "
			+property.dictLocation+candidateID+"/"+candObj.categoryVersusQuestionID.get(601).get(0).toString()+"_"+i+".txt "+
			property.commonLocation+"tiedlist" +" -S "+property.listMFCCLocation+candidateID+"/list.txt"; 
			}
			else if(type.equals("org") || type.equals("orgWarped"))
			{
			commandLine = "HVite -m -o N -C "+property.commonLocation+"config -H "+property.commonLocation
			+"hmmdefs -H "+property.commonLocation+"macros -i "+property.RECLocation+type+"/"+candidateID+"/align"+i+".mlf -I "
			+property.mlfLocation+candidateID+"/"+candObj.categoryVersusQuestionID.get(601).get(0).toString()+".mlf  "
			+property.dictLocation+candidateID+"/"+candObj.categoryVersusQuestionID.get(601).get(0).toString()+"_"+i+".txt "
			+property.commonLocation+"tiedlist" +" -S "+property.listMFCCLocation+candidateID+"/list.txt"; 
			}
			// Current edited
///			S\ystem.out.println(commandLine);
			System.out.println(commandLine);
			String cwd = System.getProperty("user.dir");
			System.setProperty("user.dir",property.MFCCLocation);
			process = Runtime.getRuntime().exec(commandLine);
			InputStream in = process.getErrorStream();
			int c; StringBuffer temp = new StringBuffer();
			//System.out.println("here2");
		//	System.out.println(in.read());
			
		//	System.out.println("SSSS");
			try{
			while((c = in.read()) != -1){
				System.out.print((char)c);
			}}catch(Exception e)
			{
				e.printStackTrace();
			}
			in.close();
			System.setProperty("user.dir",cwd);
		}
		return 1;
	}
	

	
	
	public ArrayList<ArrayList<feature_total>> parseFeatures(String extpath,int numAntimodel){
		property.numberAntiModels=numAntimodel;
		String bk = property.RECLocation;
		property.RECLocation = property.RECLocation+extpath+"/";
		float ll_temp = 0;
		ArrayList<ArrayList<feature_total>> extracted=new ArrayList<ArrayList<feature_total>>();
		File root = new File(property.RECLocation+candidateID);
		FilenameFilter filter = new FilenameFilter(){	
			@Override
			public boolean accept(File dir, String name){
				return name.contains(".mlf");
			}
		};
		String[] mlf_list=root.list(filter);
		
	//	if(mlf_list.length == property.numberAntiModels+1)
				for(int j=0;j<property.numberAntiModels+1;j++){
					String fileName=property.RECLocation+candidateID+"/align"+j+".mlf";
					
					// Added by nitesh
//					fileName = property.RECLocation+candidateID+"/"+mlf_list[j];
					
					ArrayList<feature_total> userFeaturesAll=new ArrayList<feature_total>();
					int count=-1;
					try {
						feature_total user_features=new feature_total();
						BufferedReader br = new BufferedReader(new FileReader(fileName));
						boolean flag=false;
						String read=null;
						boolean flag2=false;
						float ll=0;
						boolean flag4=false;
						while( (read = br.readLine()) != null)
						{
 							if(read.contains(".rec"))
							{
								count++;
								String[] temp=read.split("/");
								String[] utterance_id=temp[temp.length-1].split("\\.");
								user_features = new feature_total();
								int idx = utterance_id[0].indexOf('-');
								int idx2 = utterance_id[0].indexOf('_');
								int idx3 = utterance_id[0].indexOf('_',idx2+1);
								user_features.utterance_id	= utterance_id[0].substring(0,idx);
								user_features.model_id	  	= utterance_id[0].substring(idx2+1, idx3);
//								int Hi = 0;
//								if(user_features.utterance_id.equals("16"))
//									Hi = 1;
								user_features.user_id = candidateID;
							}
							else if(read.contains("sent-start"))
							{
								
							//	read = rea
							//	System.out.println(read);
								String[] temp=read.split(" ");
																								
								user_features.TimeTotal=new Float(temp[1]);
								user_features.time_word.add(Float.parseFloat(temp[0]));
								//Changed-LL follows
								user_features.likelihood_word.add((Float.parseFloat(temp[3])));
										//*250000)/(Float.parseFloat(temp[1])-Float.parseFloat(temp[0])));
								flag4=false;
								flag=true;
							}
							else if(read.contains("sent-end"))
							{
								if(flag4==true)
								{
									user_features.likelihood_word.add(ll_temp);
									ll_temp=0;
									ll=0;
								}
								
								String[] temp=read.split(" ");
								user_features.TimeTotal=new Float(temp[0])-user_features.TimeTotal;
								user_features.time_word.add(Float.parseFloat(temp[0]));
								user_features.time_word.add(Float.parseFloat(temp[1]));
								//Changed-LL follows
								user_features.likelihood_word.add((Float.parseFloat(temp[3])));
								//*250000)/(Float.parseFloat(temp[1])-Float.parseFloat(temp[0])));
								flag=false;
								//System.out.print(count+" ");
								userFeaturesAll.add(user_features);
							}
							
							else if(flag)
							{
								String[] temp=read.split(" ");
								if(!flag2)
								{	
									// Why these type of specific Hacks?
									if(temp.length==5)
									{
										flag2=true;
										if(flag4==true)
										{
											user_features.likelihood_word.add(ll);
											ll=0;
											ll_temp=0;
											flag4=false;
										}
										user_features.numph_word.add(user_features.numph);
										user_features.time_word.add(Float.parseFloat(temp[0]));
										user_features.numph++;
										user_features.numword++;
										//Changed LL follows
										ll=ll+((Float.parseFloat(temp[3])));
										//*250000)/(Float.parseFloat(temp[1])-Float.parseFloat(temp[0])));
									}
								}
								else
								{
									
									if(temp[2].equalsIgnoreCase("sp"))
									{
										flag2=false;
										
										if(Float.parseFloat(temp[3])!=0)
										{
											//Changed LL follows
											ll=ll+((Float.parseFloat(temp[3])));
											//*250000)/(Float.parseFloat(temp[1])-Float.parseFloat(temp[0])));
											ll_temp=ll;
											//Changed LL follows
											user_features.ll_sp.add((Float.parseFloat(temp[3])));
											//*250000)/(Float.parseFloat(temp[1])-Float.parseFloat(temp[0])));
											user_features.time_sp.add(Float.parseFloat(temp[1])-Float.parseFloat(temp[0]));
											user_features.sp++;
											user_features.numph_silpause.add(user_features.numph);
											flag4=true;
										}
										else
										{	
											user_features.likelihood_word.add(ll);												
											ll=0;
										}
									}
									else
									{
										//Changed LL follows
										ll=ll+((Float.parseFloat(temp[3])));
										//*250000)/(Float.parseFloat(temp[1])-Float.parseFloat(temp[0])));
										flag4=false;
										user_features.numph++;
									}
								}
							}							
						}//END_WHILE
						br.close();
						
					}//END_TRY
					catch (Exception e) 
					{
						e.printStackTrace();
					}
					extracted.add(userFeaturesAll);
				}//END_FOR
			property.RECLocation = bk;
			return extracted;
	}//function end
	
	public HashMap<String, Feature> calcFeatures(ArrayList<ArrayList<feature_total>> features_all) 
	{
		HashMap<String, Feature> feature = new HashMap<String, Feature>();
		ArrayList<feature_total> user_feature=features_all.get(0);
		
		// Changed it now do not calc diff etc.
		for(int j=0;j<user_feature.size();j++){
			Feature featurePerAlign = new Feature();
			String utteranceID 	= user_feature.get(j).utterance_id;
//			int Hi = 0;
//			if(utteranceID.equals("16"))
//				Hi = 1;
			String modelID		= user_feature.get(j).model_id;
			Float ia[] = new Float[user_feature.get(j).likelihood_word.size()];
			ia = user_feature.get(j).likelihood_word.toArray(ia);
			if(ia.length!=0){
				float sum=0;
				for (Float i1 : ia)
					sum += i1;
				sum=sum-ia[0]-ia[ia.length-1];
				featurePerAlign.stdLlWords =std(ia,sum);
				featurePerAlign.llTotal = sum;
				featurePerAlign.numWords = user_feature.get(j).numword;
				featurePerAlign.numPh = user_feature.get(j).numph;
				ia=new Float[user_feature.get(j).ll_sp.size()];
				ia=user_feature.get(j).ll_sp.toArray(ia);
				sum=0;
				for (Float i1 : ia)
				      sum += i1;
				featurePerAlign.stdLlSil= std(ia,sum);
			    featurePerAlign.llSp= sum;
			    featurePerAlign.ll = featurePerAlign.llTotal-featurePerAlign.llSp;
			    featurePerAlign.llN = featurePerAlign.ll/featurePerAlign.numPh;
			    featurePerAlign.llTotalN = featurePerAlign.llTotal/featurePerAlign.numPh;
			    featurePerAlign.numberOfSil= user_feature.get(j).sp;
				featurePerAlign.llSpN = (featurePerAlign.numberOfSil!=0)? (featurePerAlign.llSp/featurePerAlign.numberOfSil) : 0;
				featurePerAlign.llSpNumPh = featurePerAlign.llSp/featurePerAlign.numPh;
				featurePerAlign.llStart= user_feature.get(j).likelihood_word.get(0);
				featurePerAlign.llEnd= user_feature.get(j).likelihood_word.get(user_feature.get(j).likelihood_word.size()-1);
				featurePerAlign.timeTotal= user_feature.get(j).TimeTotal;
				ia=new Float[user_feature.get(j).time_sp.size()];
				ia=user_feature.get(j).time_sp.toArray(ia);
				sum=0;

				int countloungpause=0;
				ArrayList<Float> timelongpause=new ArrayList<Float>();
				
				
				for (Float i1 : ia){
					sum += i1;
					if(i1>5000000){
						countloungpause=countloungpause+1;
						timelongpause.add(i1);
					}
				}
				featurePerAlign.timeSil	= sum;
				featurePerAlign.timeStart	= (user_feature.get(j).time_word.get(1)-user_feature.get(j).time_word.get(0));
				featurePerAlign.timeEnd 	= (user_feature.get(j).time_word.get(user_feature.get(j).time_word.size()-1)-user_feature.get(j).time_word.get(user_feature.get(j).time_word.size()-2));
				featurePerAlign.timeWord 	= featurePerAlign.timeTotal-featurePerAlign.timeSil;
				featurePerAlign.meanDevSilPause	= meand(ia,sum);
				featurePerAlign.freqLongPause	= countloungpause;
				featurePerAlign.freqLongPauseToNumWords = featurePerAlign.freqLongPause/featurePerAlign.numWords;
				if(timelongpause.size()>0){
					sum=0;
					ia = new Float[timelongpause.size()];
					ia = timelongpause.toArray(ia);
					for (Float i1 : ia){
						sum+=i1;
					}
					featurePerAlign.meanLongPause = (sum/countloungpause);
					featurePerAlign.stdLongPause		= std(ia,sum);
					featurePerAlign.meanDevLongPause	= meand(ia,sum);
				}
				else{
					featurePerAlign.freqLongPause 	= 0;
					featurePerAlign.meanLongPause 	= 0;
					featurePerAlign.stdLongPause 	= 0;
					featurePerAlign.meanDevLongPause = 0;
				}
				float density=0;
				for(int k=1;k<user_feature.get(j).likelihood_word.size()-1;k++)
					density=density+(user_feature.get(j).likelihood_word.get(k)/user_feature.get(j).time_word.get(k));
				
				featurePerAlign.den= density/user_feature.get(j).numword;
				 
				sum=0;
				
				ArrayList<Integer>index=new ArrayList<Integer>();
				for (int l=0;l<user_feature.get(j).numph_silpause.size()-1;l++){
					sum+=user_feature.get(j).numph_silpause.get(l+1)-user_feature.get(j).numph_silpause.get(l);
					if(user_feature.get(j).time_sp.get(l)>2000000)
						index.add(l);
				}
				
				if(user_feature.get(j).numph_silpause.size()>1)
					featurePerAlign.meanPhSilPause = sum/(user_feature.get(j).numph_silpause.size()-1);
				else
					featurePerAlign.meanPhSilPause = 0;
				
				float sum2 = 0;
				for(int l=0;l<index.size()-1;l++){
					sum2+=user_feature.get(j).numph_silpause.get(index.get(l+1))-user_feature.get(j).numph_silpause.get(index.get(l));
				}
				if(index.size()>1)
					featurePerAlign.meanPhSilPauseGreaterPointTwo = sum2/(index.size()-1);
				else
					featurePerAlign.meanPhSilPauseGreaterPointTwo = 0;
								
				//Diff calculation
				ArrayList<Float[]> llAcrossUtterance = new ArrayList<Float[]>();
				ArrayList<Float[]> spAcrossUtterance = new ArrayList<Float[]>();
				for(int i = 0; i<features_all.size(); ++i){
 					for(int k = 0; k<features_all.get(i).size(); k++){
						if(features_all.get(i).get(k).utterance_id.equals(utteranceID) && features_all.get(i).get(k).model_id.equals(modelID)){
							Float arr[] = new Float[features_all.get(i).get(k).likelihood_word.size()];
							arr = features_all.get(i).get(k).likelihood_word.toArray(arr);
							llAcrossUtterance.add(arr);
							Float arr1[] = new Float[features_all.get(i).get(k).ll_sp.size()];
							arr1=features_all.get(i).get(k).ll_sp.toArray(arr1);
							spAcrossUtterance.add(arr1);
						}
					}
				}
				float sumSp = 0; float sumLl = 0;
				for(int i = 1; i<llAcrossUtterance.size(); ++i){
					float llTotal_temp = -1;
					float llSilence_temp;
					if(llAcrossUtterance.get(i).length >=2 ){
						llTotal_temp = (sum(llAcrossUtterance.get(i))-llAcrossUtterance.get(i)[0]-llAcrossUtterance.get(i)[llAcrossUtterance.get(i).length-1]);
						sumLl += llTotal_temp;
					}
					llSilence_temp = sum(spAcrossUtterance.get(i));
					sumSp += llSilence_temp;
					try{
						Field  llTotal_i = featurePerAlign.getClass().getDeclaredField("llTotal"+((Integer)i).toString());
						llTotal_i.setFloat(featurePerAlign, llTotal_temp);

						Field  llSilence_i = featurePerAlign.getClass().getDeclaredField("llSilence"+((Integer)i).toString());
						llSilence_i.setFloat(featurePerAlign, llSilence_temp);
						

						Field  ll_i = featurePerAlign.getClass().getDeclaredField("ll"+((Integer)i).toString());
						ll_i.setFloat(featurePerAlign, (llTotal_temp - llSilence_temp));
					}
					catch(Exception e){
						System.out.println("Error in LLAcrossUtterance calculation");
						e.printStackTrace();
						
					}
					//System.out.println("LL Total = "+sumLl);
				}
 				if(property.numberAntiModels>1){
				featurePerAlign.diff = featurePerAlign.ll - (sumLl-sumSp)/(features_all.size()-1);
				featurePerAlign.diffN =  featurePerAlign.diff/featurePerAlign.numPh;
				featurePerAlign.diffTotal = (featurePerAlign.llTotal)-(sumLl/(features_all.size()-1));
				featurePerAlign.diffTotalN = featurePerAlign.diffTotal/featurePerAlign.numPh;
				//Get the sum of the matrix
				float sumAcrossUtterance = 0;
				float sumAcrossUtterance1 = 0;
				float probCount = 0;
				//System.out.println("Utterance ID : "+utteranceID);
				if(!llAcrossUtterance.isEmpty()){
					///* Edited Nitesh
					try{
						for(int i = 1; i< llAcrossUtterance.get(0).length -1; ++i){
							float sumAcrossWord = 0;
							float sumAcrossWord1 = 0;
							int k;
							for(k = 1; k<llAcrossUtterance.size();++k){
								if(llAcrossUtterance.get(k)[i] <= llAcrossUtterance.get(0)[i]){
									sumAcrossWord += 1;
								}
							sumAcrossWord1 += llAcrossUtterance.get(0)[i] - llAcrossUtterance.get(k)[i]; 
							}				
							sumAcrossWord	/= (k-1);
							sumAcrossWord1	/= (k-1);
							if(sumAcrossWord1>0)
								probCount += 1;
							sumAcrossUtterance  += sumAcrossWord;
							sumAcrossUtterance1 += sumAcrossWord1;
						}//*/
					}
					catch(Exception e){
						System.out.println("There's an error in the calculating sum across word. : "+candObj.candidateID);
					}
					}
				featurePerAlign.meanDiffWordWise 	= sumAcrossUtterance1/featurePerAlign.numWords;
				featurePerAlign.probSpeakCorrect 	= sumAcrossUtterance/featurePerAlign.numWords;
				featurePerAlign.sumCorrectWords 	= probCount/featurePerAlign.numWords;
				}
				featurePerAlign.rateOfSpeech		= featurePerAlign.numPh/featurePerAlign.timeTotal;
				featurePerAlign.articulationRate 	= featurePerAlign.numPh/(featurePerAlign.timeTotal-featurePerAlign.timeSil);
				featurePerAlign.numSilToNumWords	= featurePerAlign.numberOfSil/featurePerAlign.numWords;
				featurePerAlign.freqLongPauseToNumWords = featurePerAlign.freqLongPause/featurePerAlign.numWords;
				featurePerAlign.timeTotalNumPh		= featurePerAlign.timeTotal/featurePerAlign.numPh;
				featurePerAlign.timeWordsToNumPh	= featurePerAlign.timeWord/featurePerAlign.numPh;	
				featurePerAlign.timeSilN			= (featurePerAlign.numberOfSil != 0)?featurePerAlign.timeSil/featurePerAlign.numberOfSil:0;
				featurePerAlign.phonationTotime		= featurePerAlign.timeWord/featurePerAlign.timeTotal;
				featurePerAlign.numSilToTimetotal	= featurePerAlign.numberOfSil/featurePerAlign.timeTotal;
			}//END_IF
			feature.put(utteranceID,featurePerAlign);
		}//END_FOR
		return feature;
	}

	private float median(Float[] ia) {
		if(ia.length==0)
			return 0;
		Arrays.sort(ia);
		int middle = ia.length/2;
		if (ia.length%2 == 1)
	        // Odd number of elements -- return the middle one.
	        return ia[middle];
	    else 
	       // Even number -- return average of middle two
	       // Must cast the numbers to double before dividing.
	       return (float) ((ia[middle-1] + ia[middle]) / 2.0);		
	}

	private float std(Float[] ia,float sum) {
		float sum2=0;
		float mean=0;
		if(ia.length==0)
			return 0;
		mean=sum/ia.length;
		for(int i=0;i<ia.length;i++)
			sum2=(float) (sum2+Math.pow((ia[i]-mean), 2));
		return (float) Math.sqrt(sum2);
		
	}
	
	private float meand(Float[] ia,float sum){
		float sum2=0;
		float mean=0;
		if(ia.length==0)
			return 0;
		mean=sum/ia.length;
		for(int i=0;i<ia.length;i++)
			sum2=(float) (sum2+Math.abs(ia[i]-mean));
		return (float) sum2/ia.length;
		
	}
	
	private float sum(Float[] arr){
		float sum = 0;
		for(int i = 0; i<arr.length; ++i)
			sum += arr[i];
		return sum;
	}
}
