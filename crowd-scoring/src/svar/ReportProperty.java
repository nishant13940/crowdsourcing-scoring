package svar;


import java.io.FileInputStream;
import java.util.*;

public class ReportProperty {
	 String fluencyThresholds;
	 String pronunciationThresholds;
	 String activeListeningThresholds;
	 String spokenEnglishThresholds;
	 String vocabularyThresholds;
	 String grammarThresholds;
	 String cefrPath;
	 String totalScoreNormPath;
	 double mean;
	 double sigmma;
	 String ts_eval;
	 String jobSuitabilityPath;
	 int isGV;
	  
	 LinkedHashMap<String, String> cefr = new LinkedHashMap<String, String> ();
	 	 
	
	ReportProperty(String allowedModules, String filePath, String svarVersion){
		Properties configFile = new Properties();
		try{
			configFile.load(new FileInputStream(filePath+"rubrics.properties"));
			fluencyThresholds				= configFile.getProperty("fluency");
			pronunciationThresholds			= configFile.getProperty("pronunciation");
			activeListeningThresholds		= configFile.getProperty("activeListening");
			spokenEnglishThresholds 		= configFile.getProperty("spokenEnglish");
			vocabularyThresholds			= configFile.getProperty("vocabulary");
			grammarThresholds				= configFile.getProperty("grammar");
			String isContainGV				= configFile.getProperty("isGV");
			String[] moduleIDs				= isContainGV.split(",");	
			
			/**
			 * added by Ankit to check if SVAR international version is of GV category
			 */
			String noGV_InternationalVersion = configFile.getProperty("noGV_InternationalVersion");
			String[] noGV_InternationalVersionArray = null;
			
			if(noGV_InternationalVersion != null)
				noGV_InternationalVersionArray = noGV_InternationalVersion.split(",");
				
			String[] moduleIDs_combineGV = null;
			String t				= configFile.getProperty("combineGV");
			if(t != null)
				moduleIDs_combineGV				= t.split(",");
			
			if(Arrays.asList(moduleIDs).contains(allowedModules+"") && (!(Arrays.asList(noGV_InternationalVersionArray).contains(svarVersion+"")) || svarVersion.trim().equals("-1")))
			{
				cefrPath				= filePath+configFile.getProperty("cefr_GVPath");
				isGV					=	1;
			}
			else if(Arrays.asList(moduleIDs_combineGV).contains(allowedModules+""))
			{
				cefrPath				= filePath+configFile.getProperty("cefr_combineGV");
				isGV					=	2;
			}
			else
			{
				cefrPath				= filePath+configFile.getProperty("cefrPath");
				isGV					=	0;
			}
			if(isGV == 0)
				totalScoreNormPath			= filePath+configFile.getProperty("totalScore_normsPath_noGV");
			else
				totalScoreNormPath			= filePath+configFile.getProperty("totalScore_normsPath_GV");
			jobSuitabilityPath			= filePath+configFile.getProperty("jobSuitabilityPath");
			configFile.clear();
			configFile.load(new FileInputStream(cefrPath));
			String[] levels = configFile.getProperty("levels").split(",");
			for(int i =0 ; i<levels.length; i++){
				cefr.put(levels[i], configFile.getProperty(levels[i]));
			}			
			configFile.clear();
		}
		catch(Exception e){
		System.out.println("Problem reading the cutoffs config file");
		}	
	}
	void updateNormsForTotalScore(String cefrLevel)
	{
		Properties configFile = new Properties();
		try{
			configFile.load(new FileInputStream(totalScoreNormPath));
			String[] mean_stdDev = configFile.getProperty("mean_stdDev_"+cefrLevel).split(",");
			mean = Double.parseDouble(mean_stdDev[0]);
			sigmma = Double.parseDouble(mean_stdDev[1]);
			ts_eval = configFile.getProperty("ts_scale_"+cefrLevel);
		}
		catch(Exception e){
			System.out.println("Problem reading the file"+totalScoreNormPath);
			}
	}
	
	int getJobSuitability(String cefrLevel)
	{
		Properties configFile = new Properties();
		int js = 0;
		try{
			configFile.load(new FileInputStream(jobSuitabilityPath));
			js = Integer.parseInt(configFile.getProperty(cefrLevel));
		}
		catch(Exception e){
			System.out.println("Problem reading the file"+totalScoreNormPath);
		}
		return js;
	}

}
	