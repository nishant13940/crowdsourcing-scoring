package svar;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class Property {
	 String cleaningScriptPath;
	 public String zippedFilesLocation;
	 public String zippedFilesSource;
	 public String zippedFilesSource1;	// Added by Vishal
	 public String unzippedFilesLocation;
	 public String mlfLocation;
	 public String dictLocation;
	 public String noiseReducedFilesLocation;
	 String compressionExtensionUsed;
	 public String MFCCLocation;
	 public String listMFCCLocation;
	 public String commonLocation;
	 public String RECLocation;
	 String wavConfigFileName;
	 String defaultTraits;
	 String additionalTraits;
	 String versionInfo;
	 int numberOfWAV;
	 int numberOfLogs;
	 int numberOfTIO;
	 int useNoiseReducedFiles;
	 public int numberAntiModels;
	 int minFreespeechID;
	 int maxFreespeechID;
	 int rcStartIndex;
	 int rcEndIndex;
	 int minFreeSpeechID;
	 int maxFreeSpeechID;
	 String[] vocabCategory;
	 String[] grammarCategory;
	 String[] comprehensionCategory;
	 String[] indianismCategory;	
	 String writeCSVPath;
	 String XMLFileName;
	 String reportPath;
	 String IRTPercentileLookup;
	 String mobileLookupPath;
	 String pollPath;
	 int usePolling;
	 String dataDumpPath;
	 String htmlImgPath;
	 String reportPropertiesPath;
	 String scoreDumpPath;
	 String noiseDetectionProperty;
	 public String modelFilesLocation;
	 ArrayList<String> listMobiles = new ArrayList<String>();
	 int useDB;
	 HashMap<String, String> IRTPercentile = new HashMap<String, String> ();
	 HashMap<String, String> rubrics = new HashMap<String, String> ();
	
	 String[] qParam0_s;
	 String[] qParam1_s;
	 String[] qParam2_s;
	 String[] correctAnswerQ_s;
	 String[] correctAnswerA_s;
	 String[] prior_s;
	
	 Double[] qParam0;
	 Double[] qParam1;
	 Double[] qParam2;
	 Double[][] correctanswer;
	 double[] prior;
	 String post2APIconfigsLocation;
	 String testformModelPath;
	 String[] totalScoreIgnoreTraits;
	 String allowedMFCCCategory[];
	 //new variable added for updated scoring 
	 String svmModel_P;
	 public String HTKConstantPath;
	 String IRTScoringTraits[];
	 String scoringVersion;
	 public int RSCategory;
	 public int LRCategory;
	 public int FSCategory;
	 String[] grammarCategory_VA;
	 
	 //! Added by ankit @ 03 Dec to rolled out fluency model 2.3
	 String svmModel_F_2_3;
	 
	 //serverConfig later added by ankit @ 26 Aug 13
	 int NETWORK_PORT;
	 String DB_URL;
	 String SVAR_TABLENAME;
	 String SVARDBName;
     String DB_USER;
     String DB_PASSWD;
	 String HOST_IP;
	 String HOST_NAME;
	 boolean printFeatures_flag;
	 String CORPMIS_URL;
	 
	 String[] CustomerHandlingSkillCategory;
	 public String scriptsLocation;
	 
	 String path = "/mnt/backupDrive/transdesk_scoring/data/";
	 public Property(){
		Properties configFile = new Properties();
		try{
			configFile.load(new FileInputStream(path+"SVAR2.properties"));
			zippedFilesLocation 			= configFile.getProperty("zippedFilesLocation");
			zippedFilesSource 				= configFile.getProperty("zippedFilesSource");
			zippedFilesSource1 				= configFile.getProperty("zippedFilesSource1");//Added by vishal for finding candidate data is it fails to copy to nfs candidate
			unzippedFilesLocation 			= configFile.getProperty("unzippedFilesLocation");
			noiseReducedFilesLocation 		= configFile.getProperty("noiseReducedFilesLocation");
			commonLocation					= configFile.getProperty("commonLocation");
			MFCCLocation					= configFile.getProperty("MFCCLocation");
			listMFCCLocation				= configFile.getProperty("listMFCCLocation");
			mlfLocation						= configFile.getProperty("mlfLocation");
			dictLocation					= configFile.getProperty("dictLocation");
			scriptsLocation					= configFile.getProperty("scriptsLocation");
			modelFilesLocation				= configFile.getProperty("modelFilesLocation");
			useNoiseReducedFiles			= Integer.parseInt(configFile.getProperty("useNoiseReducedFiles"));
			RECLocation						= configFile.getProperty("RECLocation");
			compressionExtensionUsed		= configFile.getProperty("compressionExtensionUsed");
			wavConfigFileName				= configFile.getProperty("wavConfigFileName");
			writeCSVPath 					= configFile.getProperty("rcStartIndex");
			XMLFileName 					= configFile.getProperty("regressionLocation");
			reportPath	 					= configFile.getProperty("reportPath");
			IRTPercentileLookup 			= configFile.getProperty("IRTPercentileLookup");
			mobileLookupPath 				= configFile.getProperty("mobileLookUpPath");
			pollPath 						= configFile.getProperty("pollPath");
			dataDumpPath					= configFile.getProperty("dataDumpPath");
			htmlImgPath						= configFile.getProperty("htmlImgPath");
			cleaningScriptPath				= configFile.getProperty("cleaningScriptPath");
			usePolling 						= Integer.parseInt(configFile.getProperty("usePolling"));
			reportPropertiesPath 					= configFile.getProperty("reportPropertiesPath");
			scoreDumpPath					= configFile.getProperty("scoreDumpPath");
			useDB							= Integer.parseInt(configFile.getProperty("useDB"));
			post2APIconfigsLocation			= configFile.getProperty("post2APIconfigsLocation");
			noiseDetectionProperty			= configFile.getProperty("noiseDetectionProperty");
			testformModelPath				= configFile.getProperty("testformModelPath");
			totalScoreIgnoreTraits			= configFile.getProperty("totalScoreIgnoreTraits").split(",");	
			svmModel_P					= configFile.getProperty("svmModel_P");
			HTKConstantPath					= configFile.getProperty("HTKConstantPath");
			svmModel_F_2_3					= configFile.getProperty("svmModel_F_2_3");
			
			BufferedReader br = new BufferedReader(new FileReader(mobileLookupPath));
			String read=null;
			while( (read = br.readLine()) != null){
				listMobiles.add(read);
			}
			configFile.clear();
			br.close();
			
			/*
			 // Uncomment to use single lookup for IRT Percentile
			  configFile.load(new FileInputStream(IRTPercentileLookup));
			for(String key : configFile.stringPropertyNames()){
				String value = configFile.getProperty(key);
				IRTPercentile.put(key, value);
			}*/
			
		}
		catch(Exception e){
		System.out.println("Problem reading the config file");
		}
	
		//update SERVER Config
		try{
			configFile.load(new FileInputStream(path+"serverConfig.properties"));
			NETWORK_PORT = Integer.parseInt(configFile.getProperty("serverNetworkPORT"));
			DB_URL = configFile.getProperty("dbUrl");
			SVAR_TABLENAME = configFile.getProperty("SVARTableName");
			SVARDBName = configFile.getProperty("SVARDBName");
		    DB_USER = configFile.getProperty("DBUserName");
		    DB_PASSWD = configFile.getProperty("DBPassword");
			HOST_IP = configFile.getProperty("hostIP");
			HOST_NAME = configFile.getProperty("hostName");
			printFeatures_flag = (configFile.getProperty("printFeatures").trim().equals("1"))?Boolean.TRUE:Boolean.FALSE;
			CORPMIS_URL = configFile.getProperty("corpmisURL");
		}
		catch(Exception e){
			e.printStackTrace();
			System.out.println("Problem reading the SAVR Config file");
		}
		
	}
	// Function to use different lookups for each TestForm
	void getPercentilePerTestform(String fileName, String version, String moduleID)
	{
		IRTPercentile.clear();
		Properties configFile = new Properties();
		try {
			configFile.load(new FileInputStream(commonLocation+"Module_"+moduleID+"/LookupTables_"+version+"/"+fileName+".properties"));
			for(String key : configFile.stringPropertyNames()){
				String value = configFile.getProperty(key);
				IRTPercentile.put(key, value);
			}
			configFile.clear();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	
	public void moduleVariables(String moduleID, String moduleVersion){
		Properties configFile = new Properties();
		String tempFileName = "";
		if(moduleVersion.equals("-1"))
			tempFileName = path+"Common/Module_"+moduleID+"/param_module"+moduleID+".properties";
		else
			tempFileName = path+"Common/Module_"+moduleID+"/"+moduleVersion+"/param_module"+moduleID+".properties";
		try{
			
			configFile.load(new FileInputStream(tempFileName));
			numberOfWAV						= Integer.parseInt(configFile.getProperty("numberOfWAV"));
			numberOfLogs					= Integer.parseInt(configFile.getProperty("numberOfLogs"));
			numberOfTIO						= Integer.parseInt(configFile.getProperty("numberOfTio"));
			numberAntiModels 				= Integer.parseInt(configFile.getProperty("numberAntiModels"));
			RSCategory						= Integer.parseInt(configFile.getProperty("RSCategory"));
			LRCategory						= Integer.parseInt(configFile.getProperty("LRCategory"));
			FSCategory						= Integer.parseInt(configFile.getProperty("FSCategory"));
			String t = configFile.getProperty("grammarCategory");
			if(t != null)
			grammarCategory					= t.split(",");
			t = configFile.getProperty("vocabCategory");
			if(t!= null)
			vocabCategory					= t.split(",");
			comprehensionCategory			= configFile.getProperty("comprehensionCategory").split(",");
			defaultTraits					= configFile.getProperty("defaultTraits");
			additionalTraits				= configFile.getProperty("additionalTraits");
			allowedMFCCCategory				= configFile.getProperty("allowedMFCCCategory").split(",");
			IRTScoringTraits				= configFile.getProperty("IRTScoringTraits").split(",");
			scoringVersion					= configFile.getProperty("scoringVersion");
			
			t						= configFile.getProperty("grammarCategory_VA");
			if(t!= null)
				grammarCategory_VA 			= 	t.split(",");			
			t
			=	 configFile.getProperty("indianismCategory");
			if(t!= null)
				indianismCategory = t.split(",");
			
			t						= configFile.getProperty("CustomerHandlingSkillCategory");
			
			if(t!= null)
				CustomerHandlingSkillCategory 			= 	t.split(",");			
			
			
			configFile.clear();
			
			configFile.load(new FileInputStream(path+"Common/Module_"+moduleID+"/versionConfig.properties"));
			versionInfo 			= configFile.getProperty("version");
			configFile.clear();
		}
		catch(Exception e){
			System.out.println("Error in reading moduleVariables()");
			e.printStackTrace();
		}
	}
	
	void setModuleScoringDetails(String moduleID, String version)
	{
		Properties configFile = new Properties();
		try{
			configFile.load(new FileInputStream(path+"Common/Module_"+moduleID+"/qParam_"+moduleID+"_"+version+".properties"));
			qParam0_s 						= configFile.getProperty("qParam0").split(",");
			qParam1_s 						= configFile.getProperty("qParam1").split(",");
			qParam2_s 						= configFile.getProperty("qParam2").split(",");
			correctAnswerQ_s				= configFile.getProperty("correctAnswerQ").split(",");
			correctAnswerA_s				= configFile.getProperty("correctAnswerA").split(",");
			prior_s							= configFile.getProperty("prior").split(",");
			qParam0 						= new Double[qParam0_s.length];
			qParam1 						= new Double[qParam1_s.length];
			qParam2 						= new Double[qParam2_s.length];
			correctanswer 					= new Double[2][correctAnswerA_s.length];
			prior 							= new double[prior_s.length];
			for(int i =0; i<qParam0_s.length;i++)
				qParam0[i] = Double.parseDouble(qParam0_s[i]);
			for(int i =0; i<qParam1_s.length;i++)
				qParam1[i] = Double.parseDouble(qParam1_s[i]);
			for(int i =0; i<qParam2_s.length;i++)
				qParam2[i] = Double.parseDouble(qParam2_s[i]);
			for(int i =0; i<correctAnswerA_s.length;i++){
				correctanswer[0][i] = Double.parseDouble(correctAnswerQ_s[i]);
				correctanswer[1][i] = Double.parseDouble(correctAnswerA_s[i]);
			}
			for(int i =0; i<prior_s.length;i++)
				prior[i] = (double)Double.parseDouble(prior_s[i]);
			configFile.clear();
			
		}
		catch(Exception e){
			System.out.println("Error in reading moduleVariables()");
			e.printStackTrace();
		}
	}
	
}
