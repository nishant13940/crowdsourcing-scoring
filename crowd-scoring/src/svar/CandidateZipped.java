/*
 * Requires 	: Zipped file containing candidate information
 * Produces 	: Validated unzipped file of the same 
 * Methods		:	1. CandidateDataZipped.unzip()
 * 					2. CandidateDataZipped.validateFiles()
 * 					3. CandidateDataZipped.isOkay()
 * 					
 */
package svar;
import java.io.*;

public class CandidateZipped {
	String candidateID;
	String zipFolderName;
	Property property = new Property();
	CandidateZipped(String sourcePath, String compressionExtensionUsed){
		int idx 			= sourcePath.lastIndexOf('/');
		zipFolderName		= (idx > 0)?sourcePath.substring(idx+1) : "";
		String temp_name	= zipFolderName;
		if(compressionExtensionUsed.equals(".tar.gz")){
			for(int i = 0; i<2; ++i){ // Because of .tar.gz | Notice the two dots.
				idx 				= temp_name.lastIndexOf('.');
				temp_name 			= (idx > 0) ? temp_name.substring(0,idx) : "";
			}
			candidateID 			= (idx > 0) ? temp_name.substring(0,idx) : "";
		}
		else{
			if(compressionExtensionUsed.equals("NONE"))
				candidateID = temp_name;
			else
				System.out.println(".zip has not been implemented");
		}
				
	}
	
	int isValid(){
		System.out.println("In candidateZipped.isValid()");
		try {
			if(unzip() == 1){
				try{
					if(countFiles() == 1){
						if(checkFileType() == 1){
							return 1;
						}
					}
				}
				catch(Exception e){
					System.out.println("Exception caught at countFiles().\n");
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			System.out.println("Exception caught at unzip(). "+ candidateID);
			e.printStackTrace();
		}		
		return 0;
	}
	
	int unzip() throws IOException, InterruptedException{
		Process process;
		if(property.compressionExtensionUsed.equals(".tar.gz")){
			process = Runtime.getRuntime().exec("tar -C "+property.unzippedFilesLocation+" -zxvf "+property.zippedFilesLocation+zipFolderName);
			process.waitFor();
		}
		else
			System.out.println(".zip has not been handled yet");
		return 1;
	}
	
	int countFiles() throws Exception{
		int numberOfQuestions 	= property.numberOfWAV+property.numberOfLogs+property.numberOfTIO;
		String temp				= property.unzippedFilesLocation+candidateID;
		int len 				= new File(temp).listFiles().length ;
		//System.out.println(leng);
		if(len == numberOfQuestions){
			return 1;
		}
		return 0;
	}
	
	int checkFileType(){
		return 1;
	}
}
	
