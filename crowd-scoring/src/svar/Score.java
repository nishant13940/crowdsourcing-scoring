package svar;

import java.io.*;
import java.nio.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;
import java.util.Map.Entry;
import java.lang.Math;
import java.lang.reflect.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import libsvm.svm;
import libsvm.svm_model;
import libsvm.svm_node;

import org.lorecraft.phparser.SerializedPhpParser;
import org.w3c.dom.*;

import svarUpdated.ParsePhoneFile;

public class Score {
	Candidate candidateObj;
	HashMap<String, FinalScore> finalScores;
	HashMap<Integer, ArrayList<Integer>> categoryVersusQuestionID;
	HashMap<Integer, Integer> categoryVersusUtteranceSkips;
	HashMap<String, Integer> sectionVsMinAttempts;
	HashMap<String, ArrayList<Integer>> sectionVersusQuestionID;
	HashMap<String, Integer> sectionVersusAttempted;
	HashMap<String, String> sectionVersusdiscardTraits;
	HashMap<String, Integer> sectionVersusTotalQuestion;

	/*
	 * discardTraits having the information of all traits whose scores will not
	 * be calculated
	 */
	ArrayList<String> discardTraits;

	ArrayList<Integer> countTricks;
	Property property = new Property();

	Score() {
	}

	Score(Candidate obj1) {
		finalScores = new HashMap<String, FinalScore>(); // hashmap having all
		// of the
		// information of
		// all traits;
		candidateObj = obj1;
		categoryVersusQuestionID = new HashMap<Integer, ArrayList<Integer>>();
		categoryVersusUtteranceSkips = new HashMap<Integer, Integer>();
		sectionVsMinAttempts = new HashMap<String, Integer>();
		sectionVersusQuestionID = new HashMap<String, ArrayList<Integer>>();
		sectionVersusAttempted = new HashMap<String, Integer>();
		discardTraits = new ArrayList<String>();
		sectionVersusdiscardTraits = new HashMap<String, String>();
		sectionVersusTotalQuestion = new HashMap<String, Integer>();

		/**
		 * code for assign dynamic traits
		 * 
		String additionalTraits="";
		String availTraits[];
		if(!property.additionalTraits.toUpperCase().equals("NONE"))
			additionalTraits = ","+property.additionalTraits;
		
		availTraits = (property.defaultTraits+additionalTraits).split(",");
		
		for(String trait : availTraits)
		{
			FinalScore traitObj = new FinalScore(trait);
			finalScores.put(trait, traitObj);
		}*/
		
		
		FinalScore sopkenEnglish = new FinalScore("Spoken English Understanding");
		FinalScore pronunciation = new FinalScore("Pronunciation");
		FinalScore fluency = new FinalScore("Fluency");
		FinalScore activeListening = new FinalScore("Active Listening");
		FinalScore grammar = new FinalScore("Grammar");
		FinalScore vocab = new FinalScore("Vocabulary");
		FinalScore indianism = new FinalScore("Indianism");
		FinalScore custHandlingSkill = new FinalScore("Customer Handling Skill");
		FinalScore totalScore = new FinalScore("totalScore");

		finalScores.put(sopkenEnglish.traitName, sopkenEnglish);
		finalScores.put(pronunciation.traitName, pronunciation);
		finalScores.put(fluency.traitName, fluency);
		finalScores.put(activeListening.traitName, activeListening);
		finalScores.put(grammar.traitName, grammar);
		finalScores.put(vocab.traitName, vocab);
		finalScores.put(indianism.traitName, indianism);
		finalScores.put(custHandlingSkill.traitName, custHandlingSkill);
		finalScores.put(totalScore.traitName, totalScore);
		
		property.moduleVariables(obj1.allowedModules, obj1.deliverModuleVersion);
	}

	void printFeature(HashMap<String, Feature> feature, String pathExt) {
		try {
			String pathName = property.dataDumpPath + pathExt + "/"+ candidateObj.candidateID + ".csv";
			FileWriter writer = new FileWriter(pathName);
			Iterator<Map.Entry<String, Feature>> it = feature.entrySet().iterator();
			int run = 1;

			while (it.hasNext()) {
				writer.append('\n');
				Map.Entry<String, Feature> entry = (Map.Entry<String, Feature>) it.next();
				Feature featObj = entry.getValue();
				if (run == 1) {
					writer.append("CandidateID,UtteranceID,");
					for (Field field : featObj.getClass().getDeclaredFields()) {
						writer.append(field.getName());
						writer.append(',');
					}
					run = 2;
					writer.append('\n');
				}

				writer.append(candidateObj.candidateID);
				writer.append(',');
				writer.append(entry.getKey());
				writer.append(',');
				for (Field field : featObj.getClass().getDeclaredFields()) {
					writer.append(((Double) featObj.getClass()
							.getDeclaredField(field.getName())
							.getDouble(featObj)).toString());
					writer.append(',');
				}
			}
			writer.flush();
			writer.close();
		} catch (Exception e) {
			System.out.println("There's an error in the reduction in noise in the files\n Error number : "+ candidateObj.candidateID);
			e.printStackTrace();
		}
	}
	
	void printNLPFeature(HashMap<String, Object> feature, String pathExt) {
		try {
			String pathName = property.dataDumpPath + pathExt + "/"+ candidateObj.candidateID + ".csv";
			FileWriter writer = new FileWriter(pathName);
			for (String key : feature.keySet() ) {
				writer.append(key+ ',');
			}
			writer.append("\n");
			for (String key : feature.keySet() ) {
				String value = feature.get(key).toString();
				writer.append(value+ ',');
			}
			writer.flush();
			writer.close();
		} catch (Exception e) {
			System.out.println("There's an error in the reduction in noise in the files\n Error number : "+ candidateObj.candidateID);
			e.printStackTrace();
		}
	}
	
	void printCrowdGradeFeatures(CrowdInput crowdInput, String pathExt) throws IOException {
		String pathName = property.dataDumpPath + pathExt + "/"+ candidateObj.candidateID + ".csv";
		FileWriter writer;
		writer = new FileWriter(pathName);
		writer.append("PCrowdScore,FCrowdScore,CCrowdScore,GCrowdScore");
		writer.append("\n");
		writer.append(crowdInput.PCrowdScore+","+crowdInput.FCrowdScore+","+crowdInput.CCrowdScore+","+crowdInput.GCrowdScore);
		writer.flush();
		writer.close();
	}

	void printFeature(ArrayList<ArrayList<String>> features,String pathExt) throws IOException
	{
		String pathName = property.dataDumpPath + pathExt + "/"+ candidateObj.candidateID + ".csv";
		PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(pathName)));
		for(int i=0;i<features.size();i++)
		{
			for(int j=0;j<features.get(i).size();j++)
				pw.print(features.get(i).get(j)+",");
			pw.println();
		}
		pw.close();	
	}


	public int calculateFinalScore(HashMap<String, Feature> feature, double P, double F, double AL, double totalScore, String scoringVersion, CrowdInput crowdInput) {

		System.out.println("In calculateFinalScore()");
		property.moduleVariables(candidateObj.currentModule, candidateObj.deliverModuleVersion);
		subCatVersusUtterance(feature, candidateObj.categoryVersusQuestionID); // edit by ankit to calculate the skips
		try {
			String xmlConfigName = property.commonLocation + "Module_" + candidateObj.allowedModules+"/"+candidateObj.deliverModuleVersion + "/sectionConfig.xml";
			if(!Files.exists(Paths.get(xmlConfigName)))
				xmlConfigName = property.commonLocation + "Module_" + candidateObj.allowedModules+ "/sectionConfig.xml";
			
			calculateAttemptedSectionWise(xmlConfigName, feature);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			System.out.println("Errro in parse section XML. file name : "+ property.commonLocation + "Module_"+ candidateObj.allowedModules + "/sectionConfig.xml");
			e1.printStackTrace();
		}

		// calculate the discard traits , means the traits whose score will not
		// be calculated
		checkAttemptsVsMinAttempts(sectionVsMinAttempts,sectionVersusAttempted, sectionVersusQuestionID);
		Trick trickObj = new Trick();
		try
		{	
			countTricks = trickObj.countTricks(categoryVersusQuestionID, feature,property);
			System.out.println("Number of tricks detected : " + countTricks);
		}catch(Exception e)
		{
			System.out.println("Exception in count tricks");
			countTricks = new ArrayList<Integer>();
			countTricks.add(0);
			countTricks.add(0);
		}

		String version = Report.getScoringVersion(candidateObj,property.versionInfo);
		property.setModuleScoringDetails(candidateObj.currentModule, version);


		//calculate IRT Score also check where it should not be a discarded trait
		double IRTScore = 0;
		double irtPercentile = 0;
		double IRTScore_regression = 0; //score between 0 to 1
		final String finalAdaptiveTraits[] = property.IRTScoringTraits;

		if (!discardTraits.contains("Spoken English Understanding")) //if it does not come under discarded traits
		{
			if(Arrays.asList(finalAdaptiveTraits).contains("Spoken English Understanding")) //if to do IRT Scoring else do RAW Scoring
			{
				IRTScore = calculateIRTScore(property.comprehensionCategory);
				irtPercentile = getCategoryPercentile(IRTScore, "SEU", version, candidateObj.testformNumber);
				IRTScore_regression = IRTScore;
				if (IRTScore < 0.1)
					IRTScore = 0.1;
				else if (IRTScore > 0.9)
					IRTScore = 0.9;
				IRTScore = (Math.floor(IRTScore * 1000) - 100) / 8;
			}
			else //calculate RAW score
			{
				IRTScore = calculateRawScore(property.comprehensionCategory);
				irtPercentile = (IRTScore*100)/sectionVersusTotalQuestion.get("SectionC");
				IRTScore_regression = IRTScore/sectionVersusTotalQuestion.get("SectionC");
			}
		}


		int[] countAttempted = new int[2];
		if(categoryVersusQuestionID.containsKey(property.RSCategory))
			countAttempted[0] = categoryVersusQuestionID.get(property.RSCategory).size();
		else
			countAttempted[0] = 0;
		if(categoryVersusQuestionID.containsKey(property.LRCategory))
			countAttempted[1] = categoryVersusQuestionID.get(property.LRCategory).size();
		else
			countAttempted[1] = 0;

		ArrayList<HashMap<String, Double>> finalScore = new ArrayList<HashMap<String, Double>>();
		ArrayList<HashMap<String, Double>> penalizedScores = new ArrayList<HashMap<String, Double>>();

		if(scoringVersion.equals("v1"))//if scoring version is v1 then calculate P, F, AL by using regression																		
		{
			if(!discardTraits.contains("Spoken English Understanding"))
			{
				try {
					if (irtPercentile < 50.0)
						finalScore = parseRegressionXML(property.XMLFileName+ ".Below50", feature, IRTScore_regression);
					else
						finalScore = parseRegressionXML(property.XMLFileName+ ".Above50", feature, IRTScore_regression);
				} catch (Exception e) {
					System.out.println("Error " + candidateObj.candidateID+ " in parseXML");
					e.printStackTrace();
				}
				//penalizedScores = trickObj.penalizeScore(finalScore, categoryVersusUtteranceSkips, countAttempted, property);
				penalizedScores = finalScore;
			}
		}
		else //for version V2
		{
			//create temp array List		
			HashMap<String, Double> temp = new HashMap<String, Double>();
			temp.put("Pronunciation", P);
			finalScore.add(temp);

			temp = new HashMap<String, Double>();
			temp.put("Listening", AL);
			finalScore.add(temp);

			temp = new HashMap<String, Double>();
			temp.put("Fluency", F);
			finalScore.add(temp);		

			temp = new HashMap<String, Double>();
			temp.put("totalScore", totalScore);
			finalScore.add(temp);		

			//penalizedScores = trickObj.penalizeScore(finalScore,categoryVersusUtteranceSkips, countAttempted, property);
			penalizedScores = finalScore;

		}

		FinalScore tempObj;
		String scoringVersionRegex = "^v2.*";
		if(scoringVersion.matches(scoringVersionRegex) || (scoringVersion.equals("v1") && (!discardTraits.contains("Spoken English Understanding")))) {
			Random rnd = new Random();
			for (int i = 0; i < penalizedScores.size(); ++i) {
				HashMap<String, Double> scores = new HashMap<String, Double>();
				scores = penalizedScores.get(i);
				Iterator<Map.Entry<String, Double>> it = scores.entrySet().iterator();
				while (it.hasNext()) {
					Map.Entry<String, Double> entry = (Map.Entry<String, Double>) it.next();
					String outputName = entry.getKey();

					if (outputName.equals("Fluency")) {
						double f = entry.getValue();
						/*
						if (f < 5)
							f = 5 + (rnd.nextDouble() * 5);
						else if (f >= 87 && f < 92)
							f = f - 5;
						else if (f >= 95 && f <= 100)
							f = 87 + (rnd.nextDouble() * 8);
						*/
						tempObj = finalScores.get("Fluency");
						tempObj.setScore("-3", String.format("%.2f", f));
					} else if (outputName.equals("Pronunciation")) {
						double p = entry.getValue();
						/*
						if (p < 5)
							p = 5 + (rnd.nextDouble() * 5);
						else if (p >= 90 && p < 95)
							p = p - 5;
						else if (p >= 95 && p <= 100)
							p = 90 + (rnd.nextDouble() * 5);
						*/
						tempObj = finalScores.get("Pronunciation");
						tempObj.setScore("-3", String.format("%.2f", p));
					} else if (outputName.equals("Listening")) {
						double al = entry.getValue();
						/*
						if (al < 5)
							al = 5 + (rnd.nextDouble() * 5);
						else if (al >= 90 && al < 95)
							al = al - 5;
						else if (al >= 95 && al <= 100)
							al = 90 + (rnd.nextDouble() * 5);
						*/
						tempObj = finalScores.get("Active Listening");
						tempObj.setScore("-3",String.format("%.2f", al));
					} else if (outputName.equals("totalScore")) {
						double totalscore = entry.getValue();
						if (totalscore < 5)
							totalscore = 5 + (rnd.nextDouble() * 5);
						else if (totalscore >= 90 && totalscore < 95)
							totalscore = totalscore - 5;
						else if (totalscore >= 95)
							totalscore = 90 + (rnd.nextDouble() * 5);
						tempObj = finalScores.get("totalScore");
						tempObj.setScore("-3",String.format("%.2f", totalscore));
					} 
					
				}
			}
			if(!discardTraits.contains("Spoken English Understanding")) //do recheck for V2
			{
				tempObj = finalScores.get("Spoken English Understanding");
				tempObj.setScore(String.format("%.2f", irtPercentile), String.format("%.1f", IRTScore));
			}

		}
		if (!property.additionalTraits.toUpperCase().equals("NONE")) {
			String additionalTraits[] = property.additionalTraits.split(",");
			for (int i = 0; i < additionalTraits.length; i++) {
				if (additionalTraits[i].equals("Grammar") && !discardTraits.contains("Grammar")) {
					double GrammarScore;
					double GrammarPercentile;
					if(Arrays.asList(finalAdaptiveTraits).contains("Grammar")) //if to do IRT Scoring else do RAW Scoring
					{	
						GrammarScore = calculateIRTScore(property.grammarCategory);
						GrammarScore = (Math.floor(GrammarScore * 1000) - 100) / 8;
						GrammarPercentile = getCategoryPercentile(Double.parseDouble(String.format("%.2f", GrammarScore)), "Grammar", version, "adaptive");
					}
					else
					{
						GrammarScore = calculateRawScore(property.grammarCategory);
						GrammarPercentile = (GrammarScore*100)/sectionVersusTotalQuestion.get("SectionE");
					}
					tempObj = finalScores.get("Grammar");
					if(!tempObj.score.equals("-3")) //if score in != -3
						GrammarPercentile = (Double.parseDouble(tempObj.percentile) + GrammarPercentile)/2;

					tempObj.setScore(String.format("%.2f", GrammarPercentile), String.format("%.2f", GrammarScore));
				} else if (additionalTraits[i].equals("Vocabulary") && !discardTraits.contains("Vocabulary")) {
					double VocabScore;
					double VocabPercentile;
					if(Arrays.asList(finalAdaptiveTraits).contains("Vocabulary")) //if to do IRT Scoring else do RAW Scoring
					{
						VocabScore = calculateIRTScore(property.vocabCategory);
						VocabScore = (Math.floor(VocabScore * 1000) - 100) / 8;
						VocabPercentile = getCategoryPercentile(Double.parseDouble(String.format("%.2f", VocabScore)), "Vocab",version, "adaptive");
					}
					else
					{
						VocabScore = calculateRawScore(property.vocabCategory);
						VocabPercentile = (VocabScore*100)/sectionVersusTotalQuestion.get("SectionD");
					}
					tempObj = finalScores.get("Vocabulary");
					tempObj.setScore(String.format("%.2f", VocabPercentile), String.format("%.2f", VocabScore));
				} else if (additionalTraits[i].equals("Indianism") && !discardTraits.contains("Indianism")) {
					double IndianismScore;
					double IndianismPercentile;
					if(Arrays.asList(finalAdaptiveTraits).contains("Indianism")) //if to do IRT Scoring else do RAW Scoring
					{
						IndianismScore = calculateIRTScore(property.indianismCategory);
						IndianismScore = (Math.floor(IndianismScore * 1000) - 100) / 8;
						IndianismPercentile = getCategoryPercentile(Double.parseDouble(String.format("%.2f", IndianismScore)), "Vocab",version, "adaptive");
					}
					else
					{
						IndianismScore = calculateRawScore(property.indianismCategory);
						IndianismPercentile = (IndianismScore * 100) /sectionVersusTotalQuestion.get("SectionE_2"); ;
					}
					tempObj = finalScores.get("Indianism");
					tempObj.setScore(String.format("%.2f", IndianismPercentile),String.format("%.2f", IndianismScore));
				}

				else if (additionalTraits[i].equals("grammarCategory_VA") && !discardTraits.contains("grammarCategory_VA")) {
					double grammarVAScore = calculateRawScore(property.grammarCategory_VA);
					double grammarVAScorePercentile = (grammarVAScore * 100) / 5;

					tempObj = finalScores.get("Grammar");
					if(!tempObj.score.equals("-3")) //if score is != -3
						grammarVAScorePercentile = (Double.parseDouble(tempObj.percentile) + grammarVAScorePercentile)/2;

					tempObj.setScore(String.format("%.2f", grammarVAScorePercentile), tempObj.score);
					if(discardTraits.contains("Grammar"))
						discardTraits.remove("Grammar");
				}else if (additionalTraits[i].equals("Customer Handling Skill") && !discardTraits.contains("Customer Handling Skill")) {
					double custHandlingScore;
					double custHandlingScorePercentile;
					if(Arrays.asList(finalAdaptiveTraits).contains("Customer Handling Skill")) //if to do IRT Scoring else do RAW Scoring
					{
						custHandlingScore = calculateIRTScore(property.CustomerHandlingSkillCategory);
						custHandlingScore = (Math.floor(custHandlingScore * 1000) - 100) / 8;
						custHandlingScorePercentile = getCategoryPercentile(Double.parseDouble(String.format("%.2f", custHandlingScore)), "customerHandling",version, "adaptive");
					}
					else
					{
						custHandlingScore = calculateRawScore(property.CustomerHandlingSkillCategory);
						custHandlingScorePercentile = (custHandlingScore*100)/sectionVersusTotalQuestion.get("SectionF");
					}
					tempObj = finalScores.get("Customer Handling Skill");
					tempObj.setScore(String.format("%.2f", custHandlingScorePercentile), String.format("%.2f", custHandlingScore));
				}
			}
		}
		
		Report reportObj = new Report(finalScores, candidateObj, discardTraits);

		if (property.useDB == 1) {
			DatabaseWrite dbObj = new DatabaseWrite(property);
			dbObj.writeDB(candidateObj, finalScores, reportObj, trickObj, crowdInput);
			PostResult pstRes = new PostResult();
			try {
				if (!candidateObj.currentModule.equals("93")) {
					pstRes.postResult2API(candidateObj, finalScores, reportObj, crowdInput);
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				//dbObj.postResultErrorHandler(candidateObj, "-1", "Exception when send data by post","");
			}
		}
		return 1;

	}

	double getCategoryPercentile(Double score, String cat, String version,
			String scoringType) {
		Property propObj = new Property();
		propObj.getPercentilePerTestform(cat + "Lookup_" + scoringType,
				version, candidateObj.allowedModules);
		if (propObj.IRTPercentile.get(score.toString()) != null) {
			return Double.parseDouble(propObj.IRTPercentile.get(score
					.toString()));
		} else {
			Set<String> keys = propObj.IRTPercentile.keySet();
			Object[] keysArrTemp = keys.toArray();
			Double[] keysArr = new Double[keysArrTemp.length];
			String temp;
			for (int j = 0; j < keysArrTemp.length; j++) {
				temp = (String) keysArrTemp[j];
				keysArr[j] = Double.parseDouble(temp);
			}
			Arrays.sort(keysArr);

			if (score < keysArr[0])
				return Double.parseDouble(propObj.IRTPercentile.get(keysArr[0]
				                                                            .toString()));
			if (score > keysArr[keysArr.length - 1])
				return Double.parseDouble(propObj.IRTPercentile
						.get(keysArr[keysArr.length - 1].toString()));
			int i = 0;
			for (i = 0; i < keysArr.length - 1; i++) {
				if ((score > keysArr[i]) && (score < keysArr[i + 1]))
					break;
			}
			double x1 = keysArr[i];
			double x2 = keysArr[i + 1];
			double y1 = Double.parseDouble(propObj.IRTPercentile.get(keysArr[i]
			                                                                 .toString()));
			double y2 = Double.parseDouble(propObj.IRTPercentile
					.get(keysArr[i + 1].toString()));
			double perc = y1 + ((y2 - y1) / (x2 - x1)) * (score - x1);
			return perc;
		}
	}

	void subCatVersusUtterance(
			HashMap<String, Feature> feature,
			HashMap<Integer, ArrayList<Integer>> categoryVersusQuestionIDCandidate) {
		Iterator<Entry<Integer, ArrayList<Integer>>> it = categoryVersusQuestionIDCandidate
		.entrySet().iterator();
		while (it.hasNext()) {
			Entry<Integer, ArrayList<Integer>> entry1 = it.next();
			Integer subCatID = entry1.getKey();
			ArrayList<Integer> questionAttempted = categoryVersusQuestionIDCandidate
			.get(subCatID);
			for (Integer qID : questionAttempted) {
				if ((qID < property.minFreeSpeechID || qID > property.maxFreeSpeechID)
						&& feature.get(((Integer) qID).toString()) != null) {
					if (categoryVersusQuestionID.containsKey(subCatID)) {
						ArrayList<Integer> tempList = (ArrayList<Integer>) categoryVersusQuestionID
						.get(subCatID);
						tempList.add(qID);
						categoryVersusQuestionID.put(subCatID, tempList);
					} else {
						ArrayList<Integer> tempList = new ArrayList<Integer>();
						tempList.add(qID);
						categoryVersusQuestionID.put(subCatID, tempList);
						if (!categoryVersusUtteranceSkips.containsKey(subCatID))
							categoryVersusUtteranceSkips.put(subCatID, 0);
					}
				} else {
					if (feature.get(((Integer) qID).toString()) == null
							&& (qID < property.minFreeSpeechID || qID > property.maxFreeSpeechID)
							&& !(qID >= property.rcStartIndex && qID <= property.rcEndIndex)) {
						if (categoryVersusUtteranceSkips.containsKey(subCatID)) {
							int temp = categoryVersusUtteranceSkips
							.get(subCatID);
							temp += 1;
							categoryVersusUtteranceSkips.put(subCatID, temp);
						} else {
							categoryVersusUtteranceSkips.put(subCatID, 1);
						}
					}
				}
			}
		}
	}

	void subCatVersusUtterance(HashMap<String, Feature> feature) {
		int size = ((Map) ((SerializedPhpParser.PhpObject) candidateObj.TIO).attributes
				.get("testResults")).size();
		for (int i = 0; i < size; ++i) {
			int qID = Integer
			.parseInt(((Map) ((Map) ((SerializedPhpParser.PhpObject) candidateObj.TIO).attributes
					.get("testResults")).get(i)).get("questionID")
					.toString());
			int subCatID = Integer
			.parseInt(((Map) ((Map) ((SerializedPhpParser.PhpObject) candidateObj.TIO).attributes
					.get("testResults")).get(i)).get("subcategoryID")
					.toString());
			if ((qID < property.minFreeSpeechID || qID > property.maxFreeSpeechID)
					&& feature.get(((Integer) qID).toString()) != null) {
				if (categoryVersusQuestionID.containsKey(subCatID)) {
					ArrayList<Integer> tempList = (ArrayList<Integer>) categoryVersusQuestionID
					.get(subCatID);
					tempList.add(qID);
					categoryVersusQuestionID.put(subCatID, tempList);
				} else {
					ArrayList<Integer> tempList = new ArrayList<Integer>();
					tempList.add(qID);
					categoryVersusQuestionID.put(subCatID, tempList);
					if (!categoryVersusUtteranceSkips.containsKey(subCatID))
						categoryVersusUtteranceSkips.put(subCatID, 0);
				}
			} else {
				if (feature.get(((Integer) qID).toString()) == null
						&& (qID < property.minFreeSpeechID || qID > property.maxFreeSpeechID)
						&& !(qID >= property.rcStartIndex && qID <= property.rcEndIndex)) {
					if (categoryVersusUtteranceSkips.containsKey(subCatID)) {
						int temp = categoryVersusUtteranceSkips.get(subCatID);
						temp += 1;
						categoryVersusUtteranceSkips.put(subCatID, temp);
					} else {
						categoryVersusUtteranceSkips.put(subCatID, 1);
					}
				}
			}
		}
	}

	double calculateIRTScore() {
		System.out.println("In calculateIRTScore()");
		double ability = 0;


		double[] theta = new double[100];
		for (int i = 0; i < 100; i++)
			theta[i] = ((double) i / 100 + (double) 1 / (2 * 100));
		int size = ((Map) ((SerializedPhpParser.PhpObject) candidateObj.TIO).attributes
				.get("testResults")).size();
		double[] abilityDist = new double[property.prior.length];
		for (int incr = 0; incr < property.prior.length; incr++) {
			abilityDist[incr] = property.prior[incr];
		}
		for (int i = 0; i < size; ++i) {
			int qID = Integer
			.parseInt(((Map) ((Map) ((SerializedPhpParser.PhpObject) candidateObj.TIO).attributes
					.get("testResults")).get(i)).get("questionID")
					.toString());

			int SubCatID = Integer
			.parseInt(((Map) ((Map) ((SerializedPhpParser.PhpObject) candidateObj.TIO).attributes
					.get("testResults")).get(i)).get("subcategoryID")
					.toString());
			SubCatID = (SubCatID / 100) * 100;
			int catCheck = 0;
			for (int k = 0; k < property.comprehensionCategory.length; k++) {
				if (SubCatID == Integer
						.parseInt(property.comprehensionCategory[k]))
					catCheck = 1;
			}
			if (catCheck == 1) {

				// int ind = qID - property.rcStartIndex;
				int answer;
				try {
					answer = Integer
					.parseInt(((Map) ((Map) ((SerializedPhpParser.PhpObject) candidateObj.TIO).attributes
							.get("testResults")).get(i)).get(
							"answerSelected").toString());
				} catch (Exception e) {
					answer = -1;
				}

				boolean response = false;
				int check = 0;
				int ind = 0;
				for (int j = 0; j < property.correctanswer[1].length; j++) {
					if (qID == property.correctanswer[0][j]) {
						response = (property.correctanswer[1][j] == answer);
						check = 1;
						ind = j;
						break;
					}
				}
				if (check == 0)
					System.out
					.println("qID :" + qID + " not found in Qparam!!");

				double[] probability = new double[100];

				for (int j = 0; j < 100; j++) {
					probability[j] = property.qParam2[ind]
					                                  + (1 - property.qParam2[ind])
					                                  * Math.exp(property.qParam1[ind]
					                                                              * (theta[j] - property.qParam0[ind]))
					                                                              / (1 + Math.exp(property.qParam1[ind]
					                                                                                               * (theta[j] - property.qParam0[ind])));
				}
				double sum = 0;
				if (response) {
					for (int j = 0; j < 100; j++) {
						abilityDist[j] = abilityDist[j] * (probability[j]);
						sum = sum + abilityDist[j];
					}
				} else {
					for (int j = 0; j < 100; j++) {
						abilityDist[j] = abilityDist[j] * (1 - probability[j]);
						sum = sum + abilityDist[j];
					}
				}
				if (sum != 0) {
					for (int j = 0; j < abilityDist.length; j++) {
						abilityDist[j] = abilityDist[j] / sum;
					}
				}
				double abMap = 0;
				for (int j = 0; j < abilityDist.length; j++) {
					abMap = abMap + theta[j] * abilityDist[j];
				}
				int index = maxIndex(abilityDist);
				ability = (index + 0.5) / (float) 100;
			}
		}
		// System.out.println("Value in IRT: "+ability);
		return ability;
	}

	double calculateIRTScore(String IRTcategory[]) {
		System.out.println("In calculateIRT score for category "+IRTcategory[0]);
		double ability = 0;


		double[] theta = new double[100];
		for (int i = 0; i < 100; i++)
			theta[i] = ((double) i / 100 + (double) 1 / (2 * 100));
		int size = ((Map) ((SerializedPhpParser.PhpObject) candidateObj.TIO).attributes.get("testResults")).size();
		double[] abilityDist = new double[property.prior.length];
		for (int incr = 0; incr < property.prior.length; incr++) {
			abilityDist[incr] = property.prior[incr];
		}
		for (int i = 0; i < size; ++i) {
			int qID = Integer.parseInt(((Map) ((Map) ((SerializedPhpParser.PhpObject) candidateObj.TIO).attributes.get("testResults")).get(i)).get("questionID").toString());
			int SubCatID = Integer.parseInt(((Map) ((Map) ((SerializedPhpParser.PhpObject) candidateObj.TIO).attributes.get("testResults")).get(i)).get("subcategoryID").toString());
			SubCatID = (SubCatID / 100) * 100;
			int catCheck = 0;
			for (int k = 0; k <IRTcategory.length; k++) {
				if (SubCatID == Integer.parseInt(IRTcategory[k]))
					catCheck = 1;
			}
			if (catCheck == 1) {
				// int ind = qID - property.rcStartIndex;
				int answer;
				try {
					answer = Integer.parseInt(((Map) ((Map) ((SerializedPhpParser.PhpObject) candidateObj.TIO).attributes.get("testResults")).get(i)).get("answerSelected").toString());
				} catch (Exception e) {
					answer = -1;
				}
				boolean response = false;
				int check = 0;
				int ind = 0;
				for (int j = 0; j < property.correctanswer[1].length; j++) {
					if (qID == property.correctanswer[0][j]) {
						response = (property.correctanswer[1][j] == answer);
						check = 1;
						ind = j;
						break;
					}
				}
				if (check == 0)
					System.out.println("qID :" + qID + " not found in Qparam!!");
				// boolean response = (property.correctanswer[1][ind]==answer);
				double[] probability = new double[100];

				for (int j = 0; j < 100; j++) {
					probability[j] = property.qParam2[ind]
					                                  + (1 - property.qParam2[ind])
					                                  * Math.exp(property.qParam1[ind]
					                                                              * (theta[j] - property.qParam0[ind]))
					                                                              / (1 + Math.exp(property.qParam1[ind]
					                                                                                               * (theta[j] - property.qParam0[ind])));
				}
				double sum = 0;
				if (response) {
					for (int j = 0; j < 100; j++) {
						abilityDist[j] = abilityDist[j] * (probability[j]);
						sum = sum + abilityDist[j];
					}
				} else {
					for (int j = 0; j < 100; j++) {
						abilityDist[j] = abilityDist[j] * (1 - probability[j]);
						sum = sum + abilityDist[j];
					}
				}
				if (sum != 0) {
					for (int j = 0; j < abilityDist.length; j++) {
						abilityDist[j] = abilityDist[j] / sum;
					}
				}
				double abMap = 0;
				for (int j = 0; j < abilityDist.length; j++) {
					abMap = abMap + theta[j] * abilityDist[j];
				}
				int index = maxIndex(abilityDist);
				ability = (index + 0.5) / (float) 100;

			}
		}
		// System.out.println("Value in IRT: "+ability);
		return ability;
	}

	@SuppressWarnings("rawtypes")
	double calculateGrammarScore(HashMap<String, Feature> feature) {
		System.out.println("In calculateGrammarScore()");
		double ability = 0;

		double[] theta = new double[100];
		for (int i = 0; i < 100; i++)
			theta[i] = ((double) i / 100 + (double) 1 / (2 * 100));
		int size = ((Map) ((SerializedPhpParser.PhpObject) candidateObj.TIO).attributes
				.get("testResults")).size();
		double[] abilityDist = new double[property.prior.length];
		for (int incr = 0; incr < property.prior.length; incr++) {
			abilityDist[incr] = property.prior[incr];
		}
		for (int i = 0; i < size; ++i) {
			int qID = Integer
			.parseInt(((Map) ((Map) ((SerializedPhpParser.PhpObject) candidateObj.TIO).attributes
					.get("testResults")).get(i)).get("questionID")
					.toString());
			int SubCatID = Integer
			.parseInt(((Map) ((Map) ((SerializedPhpParser.PhpObject) candidateObj.TIO).attributes
					.get("testResults")).get(i)).get("subcategoryID")
					.toString());
			SubCatID = (SubCatID / 100) * 100;
			int catCheck = 0;
			for (int k = 0; k < property.grammarCategory.length; k++) {
				if (SubCatID == Integer.parseInt(property.grammarCategory[k]))
					catCheck = 1;
			}
			if (catCheck == 1) {
				// int ind = qID - property.rcStartIndex;
				int answer;
				try {
					answer = Integer
					.parseInt(((Map) ((Map) ((SerializedPhpParser.PhpObject) candidateObj.TIO).attributes
							.get("testResults")).get(i)).get(
							"answerSelected").toString());
				} catch (Exception e) {
					answer = -1;
				}
				boolean response = false;
				int check = 0;
				int ind = 0;
				for (int j = 0; j < property.correctanswer[1].length; j++) {
					if (qID == property.correctanswer[0][j]) {
						response = (property.correctanswer[1][j] == answer);
						check = 1;
						ind = j;
						break;
					}
				}
				if (check == 0)
					System.out
					.println("qID :" + qID + " not found in Qparam!!");
				// boolean response = (property.correctanswer[1][ind]==answer);
				double[] probability = new double[100];

				for (int j = 0; j < 100; j++) {
					probability[j] = property.qParam2[ind]
					                                  + (1 - property.qParam2[ind])
					                                  * Math.exp(property.qParam1[ind]
					                                                              * (theta[j] - property.qParam0[ind]))
					                                                              / (1 + Math.exp(property.qParam1[ind]
					                                                                                               * (theta[j] - property.qParam0[ind])));
				}
				double sum = 0;
				if (response) {
					for (int j = 0; j < 100; j++) {
						abilityDist[j] = abilityDist[j] * (probability[j]);
						sum = sum + abilityDist[j];
					}
				} else {
					for (int j = 0; j < 100; j++) {
						abilityDist[j] = abilityDist[j] * (1 - probability[j]);
						sum = sum + abilityDist[j];
					}
				}
				if (sum != 0) {
					for (int j = 0; j < abilityDist.length; j++) {
						abilityDist[j] = abilityDist[j] / sum;
					}
				}
				double abMap = 0;
				for (int j = 0; j < abilityDist.length; j++) {
					abMap = abMap + theta[j] * abilityDist[j];
				}
				int index = maxIndex(abilityDist);
				ability = (index + 0.5) / (float) 100;

			}
		}
		// System.out.println("Value in IRT: "+ability);
		return ability;
	}

	int maxIndex(double[] abilityDist) {
		double max = -1.0;
		int position = 0;
		for (int i = 0; i < abilityDist.length; i++)
			if (abilityDist[i] > max) {
				max = abilityDist[i];
				position = i;
			}
		return position;
	}

	ArrayList<HashMap<String, Double>> parseRegressionXML(String XMLFileName,
			HashMap<String, Feature> feature, double IRTScore) throws Exception {
		double SVARScore = -1;
		ArrayList<HashMap<String, Double>> scores = new ArrayList<HashMap<String, Double>>();
		DocumentBuilderFactory docBuildFactory = DocumentBuilderFactory
		.newInstance();
		DocumentBuilder docBuilder = docBuildFactory.newDocumentBuilder();
		Document doc = docBuilder.parse(new File(XMLFileName));
		String AMCAT;
		if (candidateObj.isAMCAT)
			AMCAT = "AMCAT";
		else
			AMCAT = "nonAMCAT";
		doc.getDocumentElement().normalize();
		NodeList outputList = doc.getElementsByTagName("output");
		for (int j = 0; j < outputList.getLength(); ++j) {
			if (outputList.item(j).getParentNode().getNodeName().equals(AMCAT)) {
				HashMap<String, Double> scorePair = new HashMap<String, Double>();
				Element eleOutput = (Element) outputList.item(j);
				String outputName = getElementValue(eleOutput, "name").trim();
				NodeList termsList = doc.getElementsByTagName("term");
				double sectionScore = 0;
				for (int i = 0; i < termsList.getLength(); i++) {
					String temp = termsList.item(i).getParentNode()
					.getNodeName();
					temp = termsList.item(i).getParentNode().getParentNode()
					.getNodeName();
					if (termsList.item(i).getParentNode().getNodeName()
							.equals(outputName)
							&& termsList.item(i).getParentNode()
							.getParentNode().getNodeName()
							.equals(AMCAT)) {
						Element eleObj = (Element) termsList.item(i);
						if (getElementValue(eleObj, "subcat").trim().equals(
						"IRT")) {
							double tempScore = IRTScore
							* Double.parseDouble(getElementValue(
									eleObj, "coeff"));
							sectionScore += tempScore;
						} else if (getElementValue(eleObj, "subcat").trim()
								.equals("CONST"))
							sectionScore += Double.parseDouble(getElementValue(
									eleObj, "coeff"));
						else {
							Double tempScore = getValueAcrossSubcat(
									Integer.parseInt(getElementValue(eleObj,
									"subcat").trim()),
									Integer.parseInt(getElementValue(eleObj,
									"operation").trim()), feature,
									getElementValue(eleObj, "feature").trim());
							tempScore *= Double.parseDouble(getElementValue(
									eleObj, "coeff").trim());
							sectionScore += tempScore;
						}
					}
				}
				if (sectionScore > 100)
					sectionScore = 100;
				scorePair.put(outputName, sectionScore);
				scores.add(scorePair);
				SVARScore += sectionScore;
			}
		}

		return scores;
	}

	double calcMean(int subCat, ArrayList<Integer> listQuestions, HashMap<String, Feature> feature, String featureName) {
		return (calcSum(subCat, listQuestions, feature, featureName) / listQuestions.size());
	}

	double stdDev(int subCat, ArrayList<Integer> listQuestions,
			HashMap<String, Feature> feature, String featureName) {
		double stdDev = 0.0;
		double mean = calcMean(subCat, listQuestions, feature, featureName);
		for (int i = 0; i < listQuestions.size(); i++)
			if (feature.containsKey(listQuestions.get(i).toString())) {
				Feature featObj = feature.get(listQuestions.get(i).toString());
				try {
					stdDev += Math.pow(
							(featObj.getClass().getDeclaredField(featureName)
									.getDouble(featObj) - mean), 2.0);
				} catch (Exception e) {
					System.out
					.println("There's an error in the std dev calculation of feature "
							+ featureName
							+ "\n Error number : "
							+ candidateObj.candidateID);
					e.printStackTrace();
				}
			}
		return Math.pow(stdDev / (listQuestions.size() - 1), 0.5);
	}

	double calcExactStdDev(int subCat, ArrayList<Integer> listQuestions,HashMap<String, Feature> feature, String featureName) {
		double stdDev = 0.0;
		int count  =0;
		double mean = calcExactMean(subCat, listQuestions, feature, featureName);
		for (int i = 0; i < listQuestions.size(); i++)
			if (feature.containsKey(listQuestions.get(i).toString())) {
				Feature featObj = feature.get(listQuestions.get(i).toString());
				try {
					stdDev += Math.pow(
							(featObj.getClass().getDeclaredField(featureName)
									.getDouble(featObj) - mean), 2.0);
					count++;
				} catch (Exception e) {
					System.out
					.println("There's an error in the std dev calculation of feature "
							+ featureName
							+ "\n Error number : "
							+ candidateObj.candidateID);
					e.printStackTrace();
				}
			}
		return Math.pow(stdDev / (count - 1), 0.5);
	}
	double calcSum(int subCat, ArrayList<Integer> listQuestions, HashMap<String, Feature> feature, String featureName) {
		double sum = 0;
		for (int j = 0; j < listQuestions.size(); ++j) {
			if (feature.containsKey(listQuestions.get(j).toString())) {
				Feature featObj = feature.get(listQuestions.get(j).toString());
				try {
					sum += featObj.getClass().getDeclaredField(featureName)
					.getDouble(featObj);
				} catch (Exception e) {
					System.out
					.println("There's an error in the sum calculation of feature "
							+ featureName
							+ "\n Error number : "
							+ candidateObj.candidateID);
					e.printStackTrace();
				}
			} else
				System.out
				.println("There's an error in the sum calculation of feature "
						+ featureName
						+ "\n Error number : "
						+ candidateObj.candidateID);
		}
		return sum;
	}

	double calcExactMean(int subCat, ArrayList<Integer> listQuestions, HashMap<String, Feature> feature, String featureName) {
		double sum = 0;
		int count = 0;
		for (int j = 0; j < listQuestions.size(); ++j) {
			if (feature.containsKey(listQuestions.get(j).toString())) {
				Feature featObj = feature.get(listQuestions.get(j).toString());
				try {
					sum += featObj.getClass().getDeclaredField(featureName)
					.getDouble(featObj);
					count++;
				} catch (Exception e) {
					System.out
					.println("There's an error in the sum calculation of feature "
							+ featureName
							+ "\n Error number : "
							+ candidateObj.candidateID);
					e.printStackTrace();
				}
			} else
				System.out
				.println("There's an error in the sum calculation of feature "
						+ featureName
						+ "\n Error number : "
						+ candidateObj.candidateID);
		}
		return sum/count;
	}

	String getElementValue(Element e, String elementName) {
		NodeList nl = e.getElementsByTagName(elementName);
		if (nl != null && nl.getLength() > 0)
			return ((Element) nl.item(0)).getFirstChild().getNodeValue();
		else
			return "ERROR";
	}

	Double getValueAcrossSubcat(int subCat, int operationType,
			HashMap<String, Feature> feature, String featureName) {
		ArrayList<Integer> arr = new ArrayList<Integer>();
		arr = categoryVersusQuestionID.get(subCat);
		if (arr.size() > 0) {
			if (operationType == 1)
				return calcMean(subCat, arr, feature, featureName);
			else
				return stdDev(subCat, arr, feature, featureName);
		} else {
			System.out.println("UNKNOWN SUBCATEGORY FOUND");
			return 0.0;
		}

	}

	/*
	 * double getIRTPercentile(Double score, String cat, String version, String
	 * scoringType) { String temp_percentile = "0.0";
	 * property.moduleVariables(candidateObj.currentModule); try { // To use
	 * different files for percentile lookup Property propObj = new Property();
	 * propObj.getPercentilePerTestform(cat+"Lookup_"+scoringType, version,
	 * candidateObj.allowedModules); temp_percentile =
	 * propObj.IRTPercentile.get(((Double) score).toString()); } catch
	 * (Exception e) { e.printStackTrace(); } return
	 * Double.parseDouble(temp_percentile); }
	 */

	void calculateAttemptedSectionWise(String XMLFileName,
			HashMap<String, Feature> feature) throws Exception {
		HashMap<Integer, String> subcatVsSection = new HashMap<Integer, String>();
		HashMap<String, String> sectionVsSource = new HashMap<String, String>();
		HashMap<String, Integer> sectionVsIsCheckSkip = new HashMap<String, Integer>();

		DocumentBuilderFactory docBuildFactory = DocumentBuilderFactory
		.newInstance();
		DocumentBuilder docBuilder = docBuildFactory.newDocumentBuilder();
		Document doc = docBuilder.parse(new File(XMLFileName));
		doc.getDocumentElement().normalize();
		NodeList sectionList = doc.getElementsByTagName("section");
		for (int i = 0; i < sectionList.getLength(); i++) {
			Element eleOutput = (Element) sectionList.item(i);
			String[] subCat = getElementValue(eleOutput, "subCat").trim()
			.split(",");
			String sectionName = getElementValue(eleOutput, "name").trim();

			for (int incr = 0; incr < subCat.length; incr++) {
				subcatVsSection.put(Integer.parseInt(subCat[incr]), sectionName);
			}

			sectionVsSource.put(sectionName,getElementValue(eleOutput, "source").trim());
			sectionVsIsCheckSkip.put(sectionName,Integer.parseInt(getElementValue(eleOutput, "isCheckSkips").trim()));
			if (sectionVsIsCheckSkip.get(sectionName) == 1)
				sectionVsMinAttempts.put(sectionName,Integer.parseInt(getElementValue(eleOutput,"minAttempted").trim()));
			sectionVersusdiscardTraits.put(sectionName,getElementValue(eleOutput, "notScored").trim());
			sectionVersusTotalQuestion.put(sectionName,Integer.parseInt(getElementValue(eleOutput, "totalQuestion").trim()));	
		}

		int size = ((Map) ((SerializedPhpParser.PhpObject) candidateObj.TIO).attributes
				.get("testResults")).size();
		for (int i = 0; i < size; ++i) {
			int qID = Integer
			.parseInt(((Map) ((Map) ((SerializedPhpParser.PhpObject) candidateObj.TIO).attributes
					.get("testResults")).get(i)).get("questionID")
					.toString());
			int subCatID = Integer
			.parseInt(((Map) ((Map) ((SerializedPhpParser.PhpObject) candidateObj.TIO).attributes
					.get("testResults")).get(i)).get("subcategoryID")
					.toString());
			subCatID = ((int) (subCatID / 100)) * 100;
			if (subcatVsSection.containsKey(subCatID)) {
				String sectionName = subcatVsSection.get(subCatID);

				if (sectionVersusQuestionID.containsKey(sectionName)) {
					ArrayList<Integer> tempList = (ArrayList<Integer>) sectionVersusQuestionID
					.get(sectionName);
					tempList.add(qID);
					sectionVersusQuestionID.put(sectionName, tempList);
				} else {
					ArrayList<Integer> tempList = new ArrayList<Integer>();
					tempList.add(qID);
					sectionVersusQuestionID.put(sectionName, tempList);
					if (!sectionVersusAttempted.containsKey(sectionName))
						sectionVersusAttempted.put(sectionName, 0);
				}
				if (sectionVsIsCheckSkip.get(sectionName) == 1) { // check is to
					// calculate
					// the
					// attempts
					String source = sectionVsSource.get(sectionName);
					if (source.toUpperCase().equals("FEATURES")) {
						if (feature.get(((Integer) qID).toString()) != null) {
							if (sectionVersusAttempted.containsKey(sectionName)) {
								int temp = sectionVersusAttempted
								.get(sectionName);
								temp += 1;
								sectionVersusAttempted.put(sectionName, temp);
							} else {
								sectionVersusAttempted.put(sectionName, 1);
							}
						}
					} else if (source.toUpperCase().equals("TIO")) {
						String answerSelected = "";
						try {
							answerSelected = ((Map) ((Map) ((SerializedPhpParser.PhpObject) candidateObj.TIO).attributes
									.get("testResults")).get(i)).get(
									"answerSelected").toString();
						} catch (Exception e) {
							answerSelected = "";
						}
						if (!answerSelected.equals("")) {
							if (sectionVersusAttempted.containsKey(sectionName)) {
								int temp = sectionVersusAttempted
								.get(sectionName);
								temp += 1;
								sectionVersusAttempted.put(sectionName, temp);
							} else {
								sectionVersusAttempted.put(sectionName, 1);
							}
						}
					}
				}
			}
		}

	}

	void checkAttemptsVsMinAttempts(
			HashMap<String, Integer> sectionVsMinAttempts,
			HashMap<String, Integer> sectionVersusAttempted,
			HashMap<String, ArrayList<Integer>> sectionVersusQuestionID) {
		for (Entry<String, Integer> entry : sectionVsMinAttempts.entrySet()) {
			String sectionName = entry.getKey();
			if (sectionVersusAttempted.containsKey(sectionName)) {
				if (entry.getValue() > sectionVersusAttempted.get(sectionName)) {
					String[] tempNotScore = sectionVersusdiscardTraits.get(
							sectionName).split(",");
					for (int i = 0; i < tempNotScore.length; i++) {
						if (!discardTraits.contains(tempNotScore[i]))
							;
						discardTraits.add(tempNotScore[i]);
					}
				}
			} else {
				String[] tempNotScore = sectionVersusdiscardTraits.get(
						sectionName).split(",");
				for (int i = 0; i < tempNotScore.length; i++) {
					if (!discardTraits.contains(tempNotScore[i])
							&& !tempNotScore[i].equals("None"))
						;
					discardTraits.add(tempNotScore[i]);
				}
			}
		}
	}

	public double getPScores(HashMap<String, Feature> featureOrg,
			HashMap<String, Feature> feature100,
			HashMap<String, Feature> featureSent,
			HashMap<String, Feature> featureOrgWarped,
			ArrayList<ArrayList<String>> featureState,
			ArrayList<ArrayList<String>> featureStateWarped,
			HashMap<Integer, ArrayList<Integer>> categoryVersusQuestionID)
	throws IOException {
		double PScore = 0;

		// The Features is LR-Diff Mean
		ArrayList<Double> fState = new ArrayList<Double>();
		ArrayList<Double> fStateWarped = new ArrayList<Double>();
		ArrayList<Double> llTNorm = new ArrayList<Double>();
		ArrayList<Double> rsDiffOrg = new ArrayList<Double>();

		double pState = 0;
		double pStateWarped = 0;
		double pLLNorm = 0;
		double prsDiffOrg = 0;

		ArrayList<Integer> rs = new ArrayList<Integer>(
				categoryVersusQuestionID.get(property.RSCategory));
		ArrayList<Integer> lr = new ArrayList<Integer>(
				categoryVersusQuestionID.get(property.LRCategory));

		for (int i = 0; i < featureState.size(); i++) {
			Integer num = Integer.parseInt(featureState.get(i).get(0));
			if (lr.contains(num))
				fState.add(Double.parseDouble(featureState.get(i).get(7)));
		}

		pState = mean(fState);

		for (int i = 0; i < featureStateWarped.size(); i++) {

			Integer num = Integer.parseInt(featureStateWarped.get(i).get(0));
			if (lr.contains(num)) {
				Double num2 = Double.parseDouble(featureStateWarped.get(i).get(7));
				fStateWarped.add(num2);
			}
		}
		pStateWarped = mean(fStateWarped);

		for (int lrutterance : lr) {
			String temp = String.valueOf(lrutterance);
			if (featureOrg.containsKey(temp) && featureSent.containsKey(temp))
				llTNorm.add(featureOrg.get(temp).llTotal
						- featureSent.get(temp).llTotal);
		}

		pLLNorm = mean(llTNorm);

		for (int rsutterance : rs) {

			String temp = String.valueOf(rsutterance);
			if (featureOrg.containsKey(temp))
				rsDiffOrg.add(featureOrg.get(temp).diffTotal);
		}
		prsDiffOrg = mean(rsDiffOrg);

		// Norm Constants
		double meanScale[] = { 103.345404540441, -693.893937822769,
				163.466751912435, 112.264759745693 };
		double stdScale[] = { 54.7510891753830, 515.231451417811,
				94.5220331462901, 48.1658728351936 };

		svm_model LoadModel = svm.svm_load_model(property.svmModel_P);

		double[] dec_values = new double[1];
		svm_node[] x = new svm_node[] {
				new_svm_node(1, (pState - meanScale[0]) / stdScale[0]),
				new_svm_node(2, (pLLNorm - meanScale[1]) / stdScale[1]),
				new_svm_node(3, (prsDiffOrg - meanScale[2]) / stdScale[2]),
				new_svm_node(4, (pStateWarped - meanScale[3]) / stdScale[3]) };

		PScore = svm.svm_predict_values(LoadModel, x, dec_values);

		PScore = (PScore - 38.85)*21.29/20.77 + 55.43;
		//PScore = PScore - 51.09509178 + 61.78916905;

		return PScore;
	}

	public double getPScores_updated(HashMap<String, Feature> featureOrg,HashMap<String, Feature> feature100,HashMap<String, Feature> featureSent,HashMap<String, Feature> featureOrgWarped,ArrayList<ArrayList<String>> featureState,ArrayList<ArrayList<String>> featureStateWarped,HashMap<Integer, ArrayList<Integer>> categoryVersusQuestionID) throws IOException {
		double PScore = 0;

		ArrayList<Integer> rs = categoryVersusQuestionID.get(property.RSCategory);
		ArrayList<Integer> lr = categoryVersusQuestionID.get(property.LRCategory);

		ArrayList<Double> result = new ArrayList<Double>();

		result.add(calcExactMean(property.LRCategory,lr, featureStateWarped,15));
		result.add(calcExactMean(property.RSCategory,rs, featureState,5));
		result.add(calcExactMean(property.LRCategory,lr,feature100,"timeStart"));
		result.add(calcExactMean(property.RSCategory,rs, featureStateWarped,46));
		result.add(calcExactMean(property.RSCategory,rs, featureSent,"stdLlWords"));
		result.add(calcExactStdDev(property.RSCategory,rs, featureOrgWarped,"freqLongPause"));		
		result.add(calcExactStdDev(property.RSCategory,rs, featureOrgWarped,"llEnd"));
		result.add(calcExactStdDev(property.RSCategory,rs, featureSent,"stdLlSil"));
		result.add(calcExactStdDev(property.RSCategory,rs, featureStateWarped,52));
		result.add(calcExactMean(property.LRCategory,lr,featureOrg,"llSilence1"));

		PScore = 213.710422649436;
		double model[] = {0.209435524960577,-25.9719798422284,-1.77294023219599e-06,0.0157953175795000,0.0658444568937341,-14.4614865004560,-2.72604874215156,1.72511313499229,-781.196427778982,0.0359038930827947};

		for(int i=0;i<result.size();i++)
			PScore+=result.get(i)*model[i];

		PScore = (PScore-35.66)/24.40*21.29+55.436;

		return PScore;
	}

	public double getFScores(HashMap<String, Feature> featureOrg,
			HashMap<String, Feature> feature100,
			HashMap<String, Feature> featureSent,
			HashMap<String, Feature> featureOrgWarped,
			ArrayList<ArrayList<String>> featureState,
			ArrayList<ArrayList<String>> featureStateWarped,
			HashMap<Integer, ArrayList<Integer>> categoryVersusQuestionID) {
		double FScore = 0;

		// ArrayList<ArrayList<Double>> features = new
		// ArrayList<ArrayList<Double>>();

		ArrayList<Integer> rs = new ArrayList<Integer>(
				categoryVersusQuestionID.get(property.RSCategory));
		ArrayList<Integer> lr = new ArrayList<Integer>(
				categoryVersusQuestionID.get(property.LRCategory));

		ArrayList<ArrayList<Double>> featuresRS = new ArrayList<ArrayList<Double>>();
		ArrayList<ArrayList<Double>> featuresLR = new ArrayList<ArrayList<Double>>();
		ArrayList<Double> tempfeatures = new ArrayList<Double>();

		for (int i = 0; i < rs.size(); i++) {
			if (featureOrg.containsKey(String.valueOf(rs.get(i))) && featureOrgWarped.containsKey(String.valueOf(rs.get(i))) && feature100.containsKey(String.valueOf(rs.get(i))) && featureSent.containsKey(String.valueOf(rs.get(i)))) {
				String temp = String.valueOf(rs.get(i));
				tempfeatures.add(featureOrgWarped.get(temp).rateOfSpeech); // M
				tempfeatures.add(featureOrg.get(temp).numberOfSil); // M
				tempfeatures.add(featureOrg.get(temp).stdLongPause); // S
				tempfeatures.add(featureOrgWarped.get(temp).timeEnd); // S
				tempfeatures.add(featureOrg.get(temp).rateOfSpeech); // M
				tempfeatures.add(feature100.get(temp).timeStart); // M
				tempfeatures.add(featureSent.get(temp).numWords); // M
				tempfeatures.add(featureSent.get(temp).timeEnd); // S
				tempfeatures.add(featureSent.get(temp).timeWord); // M
				tempfeatures.add(feature100.get(temp).meanPhSilPauseGreaterPointTwo); // S
				tempfeatures.add(featureSent.get(temp).meanDevSilPause); // S
				tempfeatures.add(feature100.get(temp).timeTotal);   // M
				tempfeatures.add(feature100.get(temp).articulationRate); // S

				featuresRS.add(new ArrayList<Double>(tempfeatures));
				tempfeatures.clear();

			}
		}

		for (int i = 0; i < lr.size(); i++) {
			if (featureOrgWarped.containsKey(String.valueOf(lr.get(i))) && featureSent.containsKey(String.valueOf(lr.get(i)))) {
				String temp = String.valueOf(lr.get(i));
				tempfeatures.add(featureOrg.get(temp).rateOfSpeech); // S
				tempfeatures.add(featureOrg.get(temp).timeSilN); // S
				tempfeatures.add(featureOrgWarped.get(temp).timeWord); // M
				tempfeatures.add(featureOrgWarped.get(temp).rateOfSpeech); // M
				tempfeatures.add(featureOrgWarped.get(temp).numSilToTimetotal); // S
				tempfeatures.add(featureSent.get(temp).numPh); // M
				tempfeatures.add(featureSent.get(temp).timeWordsToNumPh); // M

				featuresLR.add(new ArrayList<Double>(tempfeatures));
				tempfeatures.clear();

			}
		}

		double resRS[] = new double[13];
		double resLR[] = new double[7];
		double res[] = new double[20];
		double resn[] = new double[20];

		// features[:][:]

		// For RS
		ArrayList<Integer> meanFeaturesIndex = new ArrayList<Integer>(
				Arrays.asList(0, 1, 4, 5, 6, 8, 11));
		ArrayList<Integer> stdFeaturesIndex = new ArrayList<Integer>(
				Arrays.asList(2, 3, 7, 9, 10, 12));

		ArrayList<Double> temp = new ArrayList<Double>();

		for (int j = 0; j < featuresRS.get(0).size(); j++) {
			for (int i = 0; i < featuresRS.size(); i++) // Array is of length K
			{
				temp.add(featuresRS.get(i).get(j));
			}
			if (meanFeaturesIndex.contains(j))
				resRS[j] = mean(temp);
			else if (stdFeaturesIndex.contains(j))
				resRS[j] = getStd(temp);
			temp.clear();
		}

		meanFeaturesIndex = new ArrayList<Integer>(Arrays.asList(2, 3, 5, 6));
		stdFeaturesIndex = new ArrayList<Integer>(Arrays.asList(0, 1, 4));

		temp = new ArrayList<Double>();
		for (int j = 0; j < featuresLR.get(0).size(); j++) {
			for (int i = 0; i < featuresLR.size(); i++) // Array is of length K
			{
				temp.add(featuresLR.get(i).get(j));
			}
			if (meanFeaturesIndex.contains(j))
				resLR[j] = mean(temp);
			else if (stdFeaturesIndex.contains(j))
				resLR[j] = getStd(temp);
			temp.clear();
		}

		// To get original mapping
		res[0] = resRS[0];
		res[1] = resLR[0];
		res[2] = resRS[1];
		res[3] = resRS[2];
		res[4] = resRS[3];
		res[5] = resLR[1];
		res[6] = resLR[2];
		res[7] = resRS[4];
		res[8] = resLR[3];
		res[9] = resLR[4];
		res[10] = resRS[5];
		res[11] = resLR[5];
		res[12] = resRS[6];
		res[13] = resRS[7];
		res[14] = resRS[8];
		res[15] = resRS[9];
		res[16] = resLR[6];
		res[17] = resRS[10];
		res[18] = resRS[11];
		res[19] = resRS[12];

		// res = mean(features,1);

		// double meanScale[] = {};
		// double stdScale[] =
		// {54.7510891753830,515.231451417811,94.5220331462901,48.1658728351936};

		// prsDiffOrg = mean(rsDiffOrg);

		double meanScale[] = { 1.06311002259454e-06, 2.60687434818818e-07,
				2.51050000000000, 1159442.93847291, 8625048.72247073,
				2702526.37839036, 28242324.1685924, 1.05505918335868e-06,
				1.20549801823249e-06, 3.34717819911796e-08, 9173500.00000000,
				30.1651136363636, 15.1325000000000, 4958923.57241471,
				38246500.0008334, 3.77651494483853, 945166.244384527,
				1794849.36168797, 48045583.3333333, 1.06465349732909e-07 };

		double stdScale[] = { 1.79034675757893e-07, 1.46861876659661e-07,
				1.04564635067724, 2284186.43231653, 8003478.11212061,
				2574284.73386853, 3666971.76751676, 1.85489551295025e-07,
				1.95173434518191e-07, 9.63941851262480e-09, 2658650.72042167,
				4.50323524581691, 1.93203453267942, 4635088.59174905,
				6423947.85337002, 4.61636441183954, 119448.358950765,
				1374570.12447437, 13436983.4071869, 3.07602149490561e-08 };

		for (int i = 0; i < 20; i++) {
			resn[i] = (res[i] - meanScale[i]) / stdScale[i];
		}

		double model[] = { 50.8190811356900, 4.06633096631618,
				-3.84454200553322, -3.73443284426078, 5.79838567233988,
				4.28598407905239, -7.23822668548467, 0.727876383758772,
				5.62904809055778, -3.98707036969358, -2.64010460376706,
				-4.79533634531518, 5.04287030485552, -7.91458762213819,
				-5.87023075872400, 4.27358782722246, -4.25213990924804,
				-7.97888267481969, -3.40766157628760, 5.06655927771469,
				1.27143041222621 };

		FScore = model[0];
		for (int i = 0; i < 20; i++)
			FScore += model[i + 1] * resn[i];

		FScore = (FScore - 43.41)*19.789/23.459 + 55.05;
		//FScore = FScore - 52.46937515 + 59.81040115;

		return FScore;
	}

	public double getFScores_updated(HashMap<String, Feature> featureOrg,HashMap<String, Feature> feature100,HashMap<String, Feature> featureSent,HashMap<String, Feature> featureOrgWarped,ArrayList<ArrayList<String>> featureState,ArrayList<ArrayList<String>> featureStateWarped,HashMap<Integer, ArrayList<Integer>> categoryVersusQuestionID)
	{
		double FScore = 0;

		ArrayList<Integer> rs = categoryVersusQuestionID.get(property.RSCategory);
		ArrayList<Integer> lr = categoryVersusQuestionID.get(property.LRCategory);

		ArrayList<Double> result = new ArrayList<Double>();

		result.add(calcExactMean(property.LRCategory,lr, featureOrgWarped,"diff"));
		result.add(calcExactMean(property.RSCategory,rs, featureOrgWarped,"phonationTotime"));
		result.add(calcExactMean(property.RSCategory,rs, featureOrg,"numPh"));
		result.add(calcExactMean(property.LRCategory,lr, featureSent,"ll"));
		result.add(calcExactMean(property.LRCategory,lr, featureOrg,"timeSilN"));

		result.add(calcExactStdDev(property.RSCategory,rs, featureOrgWarped,"phonationTotime"));
		result.add(calcExactStdDev(property.RSCategory,rs, feature100,"llSpN"));
		result.add(calcExactStdDev(property.RSCategory,rs, feature100,"timeSilN"));

		//	feature100.get(0).meanDiffWordWise
		result.add(calcExactMean(property.RSCategory,rs, feature100,"stdLongPause"));
		result.add(calcExactMean(property.LRCategory,lr, featureOrgWarped,"timeWord"));
		result.add(calcExactMean(property.RSCategory,rs, featureSent,"meanPhSilPauseGreaterPointTwo"));
		result.add(calcExactMean(property.RSCategory,rs, featureSent,"stdLlWords"));

		result.add(calcExactStdDev(property.LRCategory,lr,featureOrgWarped,"diff"));

		result.add(calcExactMean(property.LRCategory,lr,feature100,"timeStart"));

		result.add(calcExactStdDev(property.RSCategory,rs, featureOrg,"freqLongPause"));
		result.add(calcExactStdDev(property.LRCategory,lr, featureOrg,"timeWordsToNumPh"));
		result.add(calcExactStdDev(property.LRCategory,lr, featureOrgWarped, "timeWordsToNumPh"));

		result.add(calcExactMean(property.RSCategory,rs, featureOrgWarped,"meanDiffWordWise"));
		result.add(calcExactMean(property.RSCategory,rs, featureOrgWarped,"llSilence4"));
		result.add(calcExactMean(property.LRCategory,lr, feature100,"llTotal"));

		FScore = 14.53019211;		// Constant
		double model[] = {0.228427870000000,74.6663116700000,-2.30351390500000,-0.0114896940000000,-2.53000000000000e-06,214.491622500000,0.315818252000000,-1.85000000000000e-06,4.24000000000000e-06,-2.89000000000000e-06,-0.977822788000000,0.0668117700000000,0.233465872000000,-7.61000000000000e-07,-13.7687473400000,-6.67000000000000e-05,6.55000000000000e-05,-0.562774223000000,0.0646397030000000,-0.0118540240000000};


		for(int i=0;i<result.size();i++)
			FScore+=result.get(i)*model[i];

		FScore = (FScore-40.83)/25.56*19.789+55.07;

		return FScore;

	}

	public double getFScoresV_2_3(
			HashMap<String, Feature> featureOrg,
			HashMap<String, Feature> featureSent,
			HashMap<String, Feature> featureOrgWarped,
			ArrayList<ArrayList<String>> featureState,
			HashMap<Integer, ArrayList<Integer>> categoryVersusQuestionID)
	throws IOException {
		double FScore = 0;

		ArrayList<Integer> rs = new ArrayList<Integer>(categoryVersusQuestionID.get(property.FSCategory));
		ArrayList<Integer> lr = new ArrayList<Integer>(categoryVersusQuestionID.get(property.FSCategory));

		// The Features is LR-Diff Mean
		ArrayList<Double> fState = new ArrayList<Double>();
		double state_diff_lr = 0;

		//! State diff feature val name Diff-ALL-7 
		for (int i = 0; i < featureState.size(); i++) {
			Integer num = Integer.parseInt(featureState.get(i).get(0));
			if (lr.contains(num))
				fState.add(Double.parseDouble(featureState.get(i).get(15)));
		}
		state_diff_lr = mean(fState);

		svm_model LoadModel = svm.svm_load_model(property.svmModel_F_2_3);

		svm_node[] x = new svm_node[] {
				new_svm_node(1, state_diff_lr),
				new_svm_node(2, calcExactMean(property.RSCategory,rs, featureOrg,"numberOfSil")),				
				new_svm_node(3, calcExactMean(property.RSCategory,rs, featureSent,"stdLlWords")),
				new_svm_node(4, calcExactMean(property.RSCategory,rs, featureOrgWarped,"diff")),
				new_svm_node(5, calcExactMean(property.RSCategory,rs, featureOrg,"rateOfSpeech")),
				new_svm_node(6, calcExactMean(property.RSCategory,rs, featureOrg,"numSilToNumWords")),
				new_svm_node(7, calcExactMean(property.RSCategory,rs, featureOrgWarped,"stdLlSil")),
				new_svm_node(8, calcExactMean(property.RSCategory,rs, featureOrgWarped,"stdLlWords")),
				new_svm_node(9, calcExactMean(property.LRCategory,lr, featureSent,"llSpNumPh")),
				new_svm_node(10, calcExactMean(property.LRCategory,lr, featureOrg,"llSpN"))

		};

		FScore = svm.svm_predict_values(LoadModel, x, new double[1]);
		FScore = (((FScore - 47.88)/19.93)*17.63) + 61.29;
		return FScore;
	}

	public double getTotalScores(HashMap<String, Feature> featureOrg,
			HashMap<String, Feature> featureOrgWarped,
			HashMap<String, Feature> featureSent,
			ArrayList<ArrayList<String>> featureState,
			ArrayList<ArrayList<String>> featureStateWarped,
			CrowdInput crowdInput,
			HashMap<String, Object> nlpFeature,
			HashMap<Integer, ArrayList<Integer>> categoryVersusQuestionID) {
		double totalScore = 0;

		double diffTAll7State = 0;
		double stdLlSilOrgWarped = 0;
		double llSpNumPhSent = 0;
		double diffOrgWarped = 0;
		double rosOrg = 0;
		double meanPhSilPauseOrgWarped = 0;
		double turker_p = 0;
		double turker_f = 0;
		double turker_c = 0;
		double turker_g = 0;
		double numWords = 0;
		double nlp_morethan5Words = 0;
		double nlp_morethan8Words = 0;
		double nlp_lessthan3WordsNorm = 0;

		ArrayList<Integer> fs = new ArrayList<Integer>(
				categoryVersusQuestionID.get(property.FSCategory));

		for (int i = 0; i < featureOrg.size(); i++) {
			rosOrg = featureOrg.get(fs.get(0).toString()).rateOfSpeech;
			numWords = featureOrg.get(fs.get(0).toString()).numWords;
		}
		
		for (int i = 0; i < featureOrgWarped.size(); i++) {
			stdLlSilOrgWarped = featureOrgWarped.get(fs.get(0).toString()).stdLlSil;
			diffOrgWarped = featureOrgWarped.get(fs.get(0).toString()).diff;
			meanPhSilPauseOrgWarped= featureOrgWarped.get(fs.get(0).toString()).meanPhSilPause;
		}
		
		for (int i = 0; i < featureSent.size(); i++) {
			stdLlSilOrgWarped = featureSent.get(fs.get(0).toString()).llSpNumPh;
		}
		
		for (int i = 0; i < featureState.size(); i++) {
			diffTAll7State = Double.parseDouble(featureStateWarped.get(i).get(7));
		}
		
		turker_p = Double.parseDouble(crowdInput.PCrowdScore);
		turker_f = Double.parseDouble(crowdInput.FCrowdScore);
		turker_c = Double.parseDouble(crowdInput.CCrowdScore);
		turker_g = Double.parseDouble(crowdInput.GCrowdScore);
		
		nlp_morethan5Words = Double.parseDouble(nlpFeature.get("nlp_numberOfWordsMoreThan_5").toString());
		nlp_morethan8Words = Double.parseDouble(nlpFeature.get("nlp_numberOfWordsMoreThan_8").toString());
		nlp_lessthan3WordsNorm = Double.parseDouble(nlpFeature.get("nlp_numberOfWordsLessThan_3_Norm").toString());
	/*
	double meanScale[] = { 103.345404540441, 193.979552884981 };
	double stdScale[] = { 54.7510891753830, 88.194725735912780 };

	double alStateN = (alState - meanScale[0]) / stdScale[0];
	double alorgWarpedN = (alorgWarped - meanScale[1]) / stdScale[1];
	*/

	double p_model[] = { 0.0267127005, 10.863643, 0.596563273, 2.25155894, -0.00754445209, 0.656106674297 };
	double f_model[] = { 16.9998798, 0.0000368334467, 0.0340117205, -0.224388420, -0.457247198178 };
	double c_model[] = { 16.04075138, -34.46783602, 0.59729443, -0.66328334, 1.28635950416 };
	double g_model[] = { 13.53710077, 0.54275108, -13.4302957442 };
	double pfcg_model[]= { 0.31676376, 0.03514615, 0.66773175, 0.01338654, 1.63727925177 };

	double pscore = diffTAll7State * p_model[0] + turker_p * p_model[1] + stdLlSilOrgWarped * p_model[2] + llSpNumPhSent * p_model[3] + diffOrgWarped * p_model[4] + p_model[5];
	double fscore = turker_f * f_model[0] + rosOrg * f_model[1] + numWords * f_model[2] + meanPhSilPauseOrgWarped * f_model[3] + f_model[4];
	double cscore = turker_c * c_model[0] + nlp_lessthan3WordsNorm * c_model[1] + nlp_morethan5Words * c_model[2] + nlp_morethan8Words * c_model[3]+ c_model[4];
	double gscore = turker_g * g_model[0] + nlp_morethan5Words * g_model[1] + g_model[2];
	
	totalScore = pscore * pfcg_model[0] + fscore * pfcg_model[1] + cscore * pfcg_model[2] + gscore * pfcg_model[3] + pfcg_model[4] ;

	//scaledTotalScore
	totalScore = (((totalScore - 49.6715)*22.6683)/16.0630)+50.0002;
	return totalScore;
	}

	private svm_node new_svm_node(int i, double v) {
		svm_node x = new svm_node();
		x.index = i;
		x.value = v;

		return x;
	}

	public double mean(ArrayList<Double> arr) {
		double sum = 0;

		for (Double num : arr) {
			sum += num;
		}

		return sum / arr.size();
	}

	public double mean(double[] arr) {
		double sum = 0;
		for (Double num : arr) {
			sum += num;
		}

		return sum / arr.length;
	}

	private double getStd(ArrayList<Double> number) {
		final int n = number.size();
		if (n < 2) {
			return Double.NaN;
		}
		double avg = number.get(0);
		double sum = 0;
		for (int i = 1; i < number.size(); i++) {
			double newavg = avg + (number.get(i) - avg) / (i + 1);
			sum += (number.get(i) - avg) * (number.get(i) - newavg);
			avg = newavg;
		}
		// Change to ( n - 1 ) to n if you have complete data instead of a
		// sample.
		return Math.sqrt(sum / (n - 1));
	}

	double calculateRawScore(String[] scoringCategory) {
		System.out.println("In calculateRaw Score for Category : "
				+ scoringCategory[0]);
		int score = 0;
		int size = ((Map) ((SerializedPhpParser.PhpObject) candidateObj.TIO).attributes
				.get("testResults")).size();
		for (int i = 0; i < size; ++i) {
			int qID = Integer
			.parseInt(((Map) ((Map) ((SerializedPhpParser.PhpObject) candidateObj.TIO).attributes
					.get("testResults")).get(i)).get("questionID")
					.toString());
			int SubCatID = Integer
			.parseInt(((Map) ((Map) ((SerializedPhpParser.PhpObject) candidateObj.TIO).attributes
					.get("testResults")).get(i)).get("subcategoryID")
					.toString());
			SubCatID = (SubCatID / 100) * 100;
			int catCheck = 0;
			for (int k = 0; k < scoringCategory.length; k++) {
				if (SubCatID == Integer.parseInt(scoringCategory[k]))
					catCheck = 1;
			}
			if (catCheck == 1) {
				// int ind = qID - property.rcStartIndex;
				int answer;
				try {
					answer = Integer
					.parseInt(((Map) ((Map) ((SerializedPhpParser.PhpObject) candidateObj.TIO).attributes
							.get("testResults")).get(i)).get(
							"answerSelected").toString());
				} catch (Exception e) {
					answer = -1;
				}
				boolean response = false;
				int check = 0;
				for (int j = 0; j < property.correctanswer[1].length; j++) {
					if (qID == property.correctanswer[0][j]) {
						response = (property.correctanswer[1][j] == answer);
						check = 1;
						break;
					}
				}
				if (check == 0)
					System.out
					.println("qID :" + qID + " not found in Qparam!!");
				if (response)
					score += 1;
			}
		}
		return score;
	}

	double calcExactMean(int subCat, ArrayList<Integer> listQuestions,
			ArrayList<ArrayList<String>> feature, int id) {

		ArrayList<Double> fState = new ArrayList<Double>();

		for(int i=0;i<feature.size();i++)
		{			
			Integer num = Integer.parseInt(feature.get(i).get(0));
			if(listQuestions.contains(num))
			{
				Double num2 = Double.parseDouble(feature.get(i).get(id));
				fState.add(num2);
			}			
		}

		return getSum(fState)/fState.size();
	}

	double calcExactStdDev(int subCat, ArrayList<Integer> listQuestions,ArrayList<ArrayList<String>> feature, int id) {

		ArrayList<Double> fState = new ArrayList<Double>();

		for(int i=0;i<feature.size();i++)
		{			
			Integer num = Integer.parseInt(feature.get(i).get(0));
			if(listQuestions.contains(num))
			{
				Double num2 = Double.parseDouble(feature.get(i).get(id));
				fState.add(num2);
			}			
		}
		return getStd(fState);
	}

	private double getSum(ArrayList<Double> fState) {

		double sum=0;
		for(double num:fState)
			sum+=num;
		return sum;
	}
}
