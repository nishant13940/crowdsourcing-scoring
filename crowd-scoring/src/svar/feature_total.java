package svar;
import java.util.ArrayList;

public class feature_total
{
	String user_id;
	String utterance_id;
	String model_id;
	ArrayList<Float> time_word;
	ArrayList<Float> likelihood_word;
	ArrayList<Float>time_sp;
	ArrayList<Float>ll_sp;
	float TimeTotal;
	int numph;
	int numword;
	int sp;
	int validsp;
	ArrayList<Float> ros_3_words;
	ArrayList<Integer> numph_silpause;
	ArrayList<Integer> numph_word;

	public feature_total() 
	{
		user_id=new String();
		utterance_id=new String();
		time_word=new ArrayList<Float>();
		likelihood_word=new ArrayList<Float>();
		ll_sp=new ArrayList<Float>();
		time_sp=new ArrayList<Float>();
		numph_silpause=new ArrayList<Integer>();
		numph_word=new ArrayList<Integer>();
		TimeTotal=0;
		numph=0;
		numword=0;
		sp=0;
		validsp=0;
		// TODO Auto-generated constructor stub
	}
	void clear(){
		time_word.clear();
		likelihood_word.clear();
		time_sp.clear();
		ll_sp.clear();
		numph_silpause.clear();
		numph_word.clear();
		TimeTotal = 0;
		numph = 0;
		numword = 0;
		sp = 0;
		validsp = 0;
		utterance_id = "";
		user_id = "";
	}
};
