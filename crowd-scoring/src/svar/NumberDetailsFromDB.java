package svar;

import java.sql.*;

public class NumberDetailsFromDB {
	Connection con = null;
	PreparedStatement pst = null;
	ResultSet rs = null;

	String url = "jdbc:mysql://localhost/db_VS";
	String user = "root";
	String password = "12345678";


	int[] getDetailFromDB(String number)
	{
		int[] phoneNumberDetails  = new int[4];
		try {

			con = DriverManager.getConnection(url, user, password);
			String query = "SELECT * FROM mobileLookup WHERE `phoneNumber` = "+number;
			pst = con.prepareStatement(query);
			rs = pst.executeQuery();
			if(rs!=null)
			{
				while (rs.next()) {
					phoneNumberDetails[0] = Integer.parseInt(rs.getString(1));
					phoneNumberDetails[1] = Integer.parseInt(rs.getString(2));
					phoneNumberDetails[2] = Integer.parseInt(rs.getString(3));
					phoneNumberDetails[3] = Integer.parseInt(rs.getString(4));
				}
			}
		}catch (SQLException ex) {
			System.out.println("error in parsing the phone number from db for : "+number);
		} finally {

			try {
				if (rs != null) {
					rs.close();
				}
				if (pst != null) {
					pst.close();
				}
				if (con != null) {
					con.close();
				}

			} catch (SQLException ex) {
				System.out.println("error in closing the connection of DB ");
			}
		}
		return phoneNumberDetails;
	}
	
	int getPhoneNumberDetail(String number)
	{
		int[] phoneNumberDetails  = new int[4];
		
		if(number.length() < 5)
			return -1;
		String checkDigits = number.substring(0, 5); //get first 5 digits
		phoneNumberDetails = getDetailFromDB(checkDigits);
		if(phoneNumberDetails[0] == 0) //if not found at first time
		{
			checkDigits = number.substring(0, 4); //get first 4 digits
			phoneNumberDetails = getDetailFromDB(checkDigits);
		}
		if(phoneNumberDetails[0] == 0)
			return -2; //if landline
		else
			return phoneNumberDetails[3]; //send either 0 or 1 in case of mobile
	}

	
	String getFinalNumber(String number){
		try {
			boolean errFlag = false;
			if(number.length() != 10){
				if(number.length() == 11){
					if(number.charAt(0)=='0')
						number = number.substring(1);
					else{
						System.out.println("ERROR IN PHONE NUMBER : 11 digit incorrect detected\n");
						errFlag = true;
					}
				}
				else
					if(number.length() == 12){
						if(number.substring(0, 2).equals("91"))
							number = number.substring(2);
						else{
							System.out.println("ERROR IN PHONE NUMBER : 12 digit incorrect detected");
							errFlag = true;
						}
					}
				if(!errFlag)
					return number;
				else
				{
					System.out.println("Critical Error : Phone Number error");
					return number;
				}
			}				
		}catch(Exception e){
			System.out.println("Error in parsing phone number\n");
			e.printStackTrace();
		}
		return number;
	}
}


