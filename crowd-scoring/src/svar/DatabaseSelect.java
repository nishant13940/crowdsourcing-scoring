package svar;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.io.FileWriter;

public class DatabaseSelect {
	private static final String String = null;
	String dbtime;
	Property property = new Property();
	
	public DatabaseSelect() {} //default constructor
	
	DatabaseSelect(Property property1)
	{
		property = property1;
	}
	
	String dbUrl = property.DB_URL;
	String dbClass = "com.mysql.jdbc.Driver";
	String tableName = property.SVAR_TABLENAME;
	String dbName = property.SVARDBName;
	String dbUser = property.DB_USER;
	String dbPass = property.DB_PASSWD;
	String hostIP = property.HOST_IP;
	String hostName = property.HOST_NAME;
	String dbSelectQuery = "";
	
	public String selectDb(String word,int itr) throws ClassNotFoundException, SQLException{
		Class.forName("com.mysql.jdbc.Driver");
		Connection con = DriverManager.getConnection (dbUrl+dbName, dbUser, dbPass);

		// Check of existing entry
		Statement stmtCheck = con.createStatement() ;
		ResultSet rs = stmtCheck.executeQuery( "SELECT * FROM dictionary WHERE word ='"+word+"'");
		
		if(!rs.next()){
			try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmtCheck != null) stmtCheck.close(); } catch (Exception e) {};
		    try { if (con != null) con.close(); } catch (Exception e) {};
			return "";
		}
		else{
			String phoneme=rs.getString(itr+2);
			try { if (rs != null) rs.close(); } catch (Exception e) {};
		    try { if (stmtCheck != null) stmtCheck.close(); } catch (Exception e) {};
		    try { if (con != null) con.close(); } catch (Exception e) {};
			return phoneme;
		}
	}
	
	public boolean insertDB(String[] rowEntry) throws ClassNotFoundException, SQLException{
		Class.forName("com.mysql.jdbc.Driver");
		Connection con = DriverManager.getConnection (dbUrl+dbName, dbUser, dbPass);
		
		PreparedStatement stmtCheck = con.prepareStatement("INSERT INTO dictionary SET word=?, dict0=?, dict1=?, dict2=?, dict3=?, dict4=?, dict5=?");
		stmtCheck.setString(1, rowEntry[0]);
		stmtCheck.setString(2, rowEntry[1]);
		stmtCheck.setString(3, rowEntry[2]);
		stmtCheck.setString(4, rowEntry[3]);
		stmtCheck.setString(5, rowEntry[4]);
		stmtCheck.setString(6, rowEntry[5]);
		stmtCheck.setString(7, rowEntry[6]);
		
		return stmtCheck.execute();
		
	}
	public int askDBForNumberOfCommonWords(String text, String number) throws ClassNotFoundException, SQLException{
		Class.forName("com.mysql.jdbc.Driver");
		Connection con = DriverManager.getConnection (dbUrl+dbName, dbUser, dbPass);

		// Check of existing entry
		String query = "SELECT COUNT(*) FROM tbl_Common"+number+" WHERE word in ("+text+")";
		System.out.println(query);
		Statement stmtCheck = con.createStatement() ;
		ResultSet rs = stmtCheck.executeQuery( query);
		
		if(rs.next()){
			return Integer.parseInt(rs.getString(1));
		}
		else{
			return -1;
		}
	}
}
