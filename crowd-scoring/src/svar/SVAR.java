/*
 * Requires 	: Main file, which calls all other class instances and methods
 * 
 * Produces 	: 
 * 
 * Methods		:	
 * 					
 */
package svar;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

import svarUpdated.ParsePhoneFile;
import svarUpdated.SentenceWise;
import svarUpdated.WarpMFCC;
import noisedetection.*;
import lib.*;

public class SVAR implements Runnable{
	String PATH_OF_ZIP;
	String candidateID;
	int copySuccess;
	boolean mlfSuccess;
	boolean dictSuccess;
	Property property = new Property();
	CrowdInput crowdInput = new CrowdInput();
	
	SVAR(CrowdInput crowdInputForCand){
	  	//Insert Copy Code
		crowdInput = crowdInputForCand;
		String fileName = crowdInput.candId;
		
		PATH_OF_ZIP = property.zippedFilesLocation +fileName;
		DirCopy dirCopyObj = new DirCopy();
		copySuccess = dirCopyObj.copyDir(fileName);
		
		
		if(copySuccess==0)
		{
			DatabaseWrite dbObject = new DatabaseWrite();	
			dbObject.changeScoringStatus(6,fileName);	//Added by vishal to implement scheduler. Status 6 means the scoring has crashed. No TIO Found
		}
		//copySuccess = 1;
	}
	
	public void run(){
		if(copySuccess == 1){
		DatabaseWrite dbObject = new DatabaseWrite();	
		CandidateZipped candidateZipObj		= new CandidateZipped(PATH_OF_ZIP, property.compressionExtensionUsed);// dest add (pathofzip)
		dbObject.changeScoringStatus(2,candidateZipObj.candidateID);	//Added by vishal to implement scheduler. Status 2 means the scoring has been started
		int retIsValid						= property.compressionExtensionUsed.equals("NONE")?1:candidateZipObj.isValid();
		if(retIsValid == 1){
			candidateID = candidateZipObj.candidateID;
			NoiseReduce reduceObj = new NoiseReduce(candidateZipObj.candidateID);
			try {
				final long startTime = System.currentTimeMillis();			

				// Noise Detection Algorithm yet to be finalized. ReduceNoise currently does nothing
				int retReduceNoise = reduceObj.reduceNoise(); 
				if(retReduceNoise == 1){
					Candidate candidateObj = new Candidate(candidateZipObj.candidateID);
					dbObject.postResultResetStatus(candidateObj);
					int retParseTIO = candidateObj.parseTIO(); 
					if(retParseTIO == 1){
						
						DictOperations dictObj = new DictOperations();
						dictSuccess = dictObj.createDict(candidateObj, crowdInput);
						
						MLFOperations mlfObj = new MLFOperations();
						mlfSuccess = mlfObj.createMLF(candidateObj, crowdInput);
						
						
						
						if(mlfSuccess & dictSuccess == true){
							property.moduleVariables(candidateObj.currentModule, candidateObj.deliverModuleVersion);
							NoiseDetectionResult noiseOb = new NoiseDetectionResult(property.noiseDetectionProperty, property);
							candidateObj.noisyStatus = noiseOb.getResult(property.noiseReducedFilesLocation+candidateObj.candidateID, candidateObj.categoryVersusQuestionID,candidateObj.currentModule);
								
							int retCreateREC=1;
							int retCreateMFCC = 1;
							HTK htkObj = new HTK(candidateObj);
							retCreateMFCC = htkObj.createMFCC(true,candidateObj.categoryVersusQuestionID2, candidateObj.categoryVersusQuestionID);
							
							String output100Words = "100Word";
							String outputSentWords = "SentWord";
							String output100 = "100";
							String outputSent = "Sent";
							String org = "org";
							String state = "state";
							String orgWarped = "orgWarped";
							String stateWarped = "stateWarped";
							String nlp = "nlp";
							String crowdGrades = "crowdGrades";
							
							String createFolders[] = {output100Words,outputSentWords,output100,outputSent,org,state,orgWarped,stateWarped,nlp,crowdGrades};
							
							for(String str:createFolders)
							{
								Path p = Paths.get(property.RECLocation+str); 
								if(!Files.exists(p))
									Files.createDirectory(p);
								p = Paths.get(property.RECLocation+str+"/"+candidateID);
								if(!Files.exists(p))
									Files.createDirectory(p);
							}
							if(retCreateMFCC == 1){
							    int retCreateRECOrg = htkObj.createREC(org);
							    
							    String scoringVersionRegex = "^v2.*";
							    if(property.scoringVersion.matches(scoringVersionRegex)){
								    int retCreateRECState = htkObj.createREC(state);	
								    String modelFilePath = property.modelFilesLocation;
									int retCreateRECWords100Word = SentenceWise.runMFCC(property, candidateObj.categoryVersusQuestionID, candidateID,modelFilePath+"100Words/"+candidateID,candidateObj.currentModule, output100Words);
									int retCreateRECWordsSentWord = SentenceWise.runMFCC(property, candidateObj.categoryVersusQuestionID, candidateID,modelFilePath+"sentWords/"+candidateID,candidateObj.currentModule, outputSentWords);
									property.numberAntiModels=0;
									int retCreateRECWords100 = SentenceWise.runMFCCForceAlign(property, candidateObj.categoryVersusQuestionID, candidateObj,property.RECLocation+output100Words+"/",candidateObj.currentModule,output100);
									int retCreateRECWordsSent = SentenceWise.runMFCCForceAlign(property, candidateObj.categoryVersusQuestionID, candidateObj,property.RECLocation+outputSentWords+"/",candidateObj.currentModule,outputSent);
							    }
								property.moduleVariables(candidateObj.currentModule, candidateObj.deliverModuleVersion);
					
								Score score = new Score(candidateObj);
								if(retCreateREC == 1){
									double PScore = -3.0;
									double FScore = -3.0;
									double ALScore = -3.0;
									double totalScore = -3.0;
									
									HashMap<String, Feature> featureOrg = htkObj.calcFeatures(htkObj.parseFeatures(org,property.numberAntiModels));
									if(property.printFeatures_flag)
										score.printFeature(featureOrg,org);								
									if(property.scoringVersion.matches(scoringVersionRegex)){
										
										NLPFeatures nlpObj = new NLPFeatures();
										HashMap<String, Object> nlpFeature = nlpObj.calcNLPFeatures(crowdInput.Transcription);
										if(property.printFeatures_flag)
											score.printNLPFeature(nlpFeature,nlp);
										
										if(property.printFeatures_flag)
											score.printCrowdGradeFeatures(crowdInput,crowdGrades);
										
										HashMap<String, Feature> feature100 = htkObj.calcFeatures(htkObj.parseFeatures(output100,0));
										if(property.printFeatures_flag)
											score.printFeature(feature100,output100);
										
										HashMap<String, Feature> featureSent = htkObj.calcFeatures(htkObj.parseFeatures(outputSent,0));
										if(property.printFeatures_flag)
											score.printFeature(featureSent,outputSent);
										
										// For  State Features
										ParsePhoneFile parsePhoneFile = new ParsePhoneFile();
										ArrayList<ArrayList<String>> featureState = parsePhoneFile.getStateFeatures(property.RECLocation+state+"/",property.dataDumpPath+state+"/",candidateID,property);
										if(property.printFeatures_flag)
											score.printFeature(featureState,state);
									
										// Now For Warping MFCC
										ParsePhoneFile.getVowelFile(property.RECLocation+org+"/",candidateID);								
										WarpMFCC.warpMFCC(property.MFCCLocation,property.RECLocation+org+"/", candidateObj.candidateID,candidateObj.categoryVersusQuestionID,candidateObj.currentModule, candidateObj.deliverModuleVersion);
										 
										property.moduleVariables(candidateObj.currentModule, candidateObj.deliverModuleVersion);
											 
										retCreateRECOrg = htkObj.createREC(orgWarped,property);
		  							    int retCreateRECState = htkObj.createREC(stateWarped,property);						    
		  							  
		  							    HashMap<String, Feature> featureOrgWarped = htkObj.calcFeatures(htkObj.parseFeatures(orgWarped,property.numberAntiModels));
		  							    if(property.printFeatures_flag)
		  								  score.printFeature(featureOrgWarped,orgWarped);
										
										property.moduleVariables(candidateObj.currentModule, candidateObj.deliverModuleVersion);
										
										//ParsePhoneFile parsePhoneFile = new ParsePhoneFile();
										ArrayList<ArrayList<String>> featureStateWarped = parsePhoneFile.getStateFeatures(property.RECLocation+stateWarped+"/",property.dataDumpPath+stateWarped+"/",candidateID,property);
										if(property.printFeatures_flag)
											score.printFeature(featureStateWarped,stateWarped);
										
										final long endTime = System.currentTimeMillis();
										
										try
										{
											/*
											if(property.scoringVersion.equals("v2.1") || property.scoringVersion.equals("v2.2"))
												PScore = score.getPScores(featureOrg,feature100,featureSent,featureOrgWarped,featureState,featureStateWarped,candidateObj.categoryVersusQuestionID);
											else											
												PScore = score.getPScores(featureOrg,feature100,featureSent,featureOrgWarped,featureState,featureStateWarped,candidateObj.categoryVersusQuestionID);
											
											if(PScore > 100) PScore = 100;
											*/
											PScore= -3.0;
										}
										catch(Exception e)
										{
											System.out.println("Exception in generate P Scores");
											PScore = -3.0; //set PScore -3
										}
										
										try
										{
											/*
											if(property.scoringVersion.equals("v2.3"))
												FScore = score.getFScoresV_2_3(featureOrg,featureSent,featureOrgWarped,featureState,candidateObj.categoryVersusQuestionID);
												//FScore = score.getFScores_updated(featureOrg,feature100,featureSent,featureOrgWarped,featureState,featureStateWarped,candidateObj.categoryVersusQuestionID);
											else if(property.scoringVersion.equals("v2.1") || property.scoringVersion.equals("v2.2"))
												FScore = score.getFScores_updated(featureOrg,feature100,featureSent,featureOrgWarped,featureState,featureStateWarped,candidateObj.categoryVersusQuestionID);
											else											
												FScore = score.getFScores(featureOrg,feature100,featureSent,featureOrgWarped,featureState,featureStateWarped,candidateObj.categoryVersusQuestionID);
											
											if(FScore > 100) FScore = 100;
											*/
											FScore=-3;
										}
										catch(Exception e)
										{
											System.out.println("Exception in generate F Scores");
											FScore = -3.0; //set FScore -3
										}
										
										try
										{
											/*
											if(property.scoringVersion.equals("v2.1") || property.scoringVersion.equals("v2.2"))
												PScore = score.getPScores(featureOrg,feature100,featureSent,featureOrgWarped,featureState,featureStateWarped,candidateObj.categoryVersusQuestionID);
											else											
												PScore = score.getPScores(featureOrg,feature100,featureSent,featureOrgWarped,featureState,featureStateWarped,candidateObj.categoryVersusQuestionID);
											
											if(PScore > 100) PScore = 100;
											*/
											ALScore= -3.0;
										}
										catch(Exception e)
										{
											System.out.println("Exception in generate P Scores");
											ALScore = -3.0; //set PScore -3
										}
										
										try
										{
											totalScore = score.getTotalScores(featureOrg,featureOrgWarped,featureSent,featureState,featureStateWarped,crowdInput,nlpFeature,candidateObj.categoryVersusQuestionID);
										}
										catch(Exception e)
										{
											e.printStackTrace();
											totalScore = -3.0; //set ALScore -3
										}
										System.out.println(endTime-startTime);
									}
									
									int retCalcScore = score.calculateFinalScore(featureOrg, PScore, FScore, ALScore, totalScore, property.scoringVersion, crowdInput);
									// Call Your P,F,AL score functions here
									
									if(retCalcScore == 1)
									{
										dbObject.changeScoringStatus(5,candidateZipObj.candidateID);	//Added by vishal to implement scheduler. Status 5 means that scoring has been completed successfully
										System.out.println("\nScore Calculated. So far so good.");
									}
									else
									{
										String reason = "";
										if(retCalcScore == -1)
											reason = "Candidate has attempt less number of question than required";
										else
											reason = "Score not calculated by calculateFinalScore()";
										dbObject.writeDBScoringError(candidateID, reason);
									}
								}
								else{
									System.out.println("Corrupted REC or Test form number .. "+candidateObj.candidateID);
									dbObject.writeDBScoringError(candidateID, "Corrupted REC or Test form number");
									/*
									 * send post result as -1 if there is any error in the HVITE command
									 * */
									PostResult pr = new PostResult();
									pr.postResult2API(candidateObj, -1, "v1");
								}
							}
							else{
								System.out.println("Incorrect number of files in ReducedNoise Sample.. "+candidateObj.candidateID);
								dbObject.writeDBScoringError(candidateID, "Incorrect number of files in ReducedNoise Sample");
								
								Property property = new Property();
								/*
								 * get the module version
								 * If can not parse the tio then module version will be v1
								 * */
								property.moduleVariables(candidateObj.allowedModules, candidateObj.deliverModuleVersion);
								String moduleVersion  =  Report.getScoringVersion(candidateObj, property.versionInfo);
								
								DatabaseWrite dbObj = new DatabaseWrite();
								dbObj.writeDB(candidateObj, -1, moduleVersion);
								
								PostResult pr = new PostResult();
								pr.postResult2API(candidateObj, -1, moduleVersion);
							}
						}
						else{
							System.out.println("There's an error in creation of Dictionary or MLF.\n CandidateID : "+candidateObj.candidateID);
							dbObject.writeDBScoringError(candidateID, "error in the creating dictionary or MLF");
						}
					}
					else
					{
						
						System.out.println("There's an error in the parsing of serialized TIOs.\n Error number : "+candidateObj.candidateID);
						dbObject.writeDBScoringError(candidateID, "error in the parsing of serialized TIOs");
						
					}					
				}
				else
				{
					System.out.println("There's an error in the reduction in noise in the files\n Error number : "+candidateZipObj.candidateID);
					dbObject.writeDBScoringError(candidateID, "error in the reduction in noise in the files");
				}
			} catch (Exception e) {
				System.out.println("There's an error in the reduction in noise in the files\n Error number : "+candidateZipObj.candidateID);
				dbObject.writeDBScoringError(candidateID, "Exception in either HTK or noise reduction");
				e.printStackTrace();
			}
		}
		else{
			System.out.println("There's an error in the zipping of files\n Error number : "+retIsValid);		
		}
	
		if(!property.printFeatures_flag)
		{
		Process process;
		try{
			if((candidateID.length() == 14)&&(!candidateID.equals("              "))){
				process = Runtime.getRuntime().exec(property.cleaningScriptPath+" "+candidateID);
				process.waitFor();	
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		}
	}
}


