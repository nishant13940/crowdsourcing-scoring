package svar;
import java.io.*;
import org.lorecraft.phparser.SerializedPhpParser;

public class UnserializeTIO {
	String sourcePath;
	String fileName;
	String tioContent;
	UnserializeTIO(String inputSourcePath, String inputFileName){
		sourcePath 	= inputSourcePath;
		fileName	= inputFileName;
	}
	private String readFile(String file) throws IOException {
	    BufferedReader reader = new BufferedReader( new FileReader(file));
	    String line  = null;
	    StringBuilder stringBuilder = new StringBuilder();
	    String ls = System.getProperty("line.separator");
	    while( ( line = reader.readLine() ) != null ) {
	    	tioContent = line;
	        stringBuilder.append( line );
	        stringBuilder.append( ls );
	    }
	    reader.close();
	    return stringBuilder.toString();
	 }
	Object retrieveObject() throws Exception{
		String content = readFile(sourcePath+fileName+"/"+fileName+".tio");
		SerializedPhpParser serializedPhpParser = new SerializedPhpParser(content);
		Object result = serializedPhpParser.parse();
		return result;
	}
}
