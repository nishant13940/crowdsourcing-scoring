package svar;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import umontreal.iro.lecuyer.probdist.NormalDist;

import org.lorecraft.phparser.SerializedPhpParser;

public class Report {
	Candidate candObj;
	String[] availTraits;
	HashMap<String, FinalScore> finalScores;
	HashMap<String, Integer> grades;
	String CEFR;	
	double totalScore;
	String totalScoreString;
	int jobSuitability;
	Property property = new Property();
	ReportProperty reportPropObj;
	String moduleVersion= "";
	ArrayList<String> discardTraits;
	
	Report(HashMap<String, FinalScore> scores, Candidate obj, ArrayList<String> notScored)
	{
		finalScores = scores;		
		candObj = obj;
		property.moduleVariables(candObj.allowedModules, candObj.deliverModuleVersion);
		moduleVersion = getScoringVersion(candObj, property.versionInfo);
		reportPropObj = new ReportProperty(candObj.allowedModules, property.reportPropertiesPath+moduleVersion+"/", candObj.deliverModuleVersion);
		String additionalTraits="";
		if(!property.additionalTraits.toUpperCase().equals("NONE"))
			additionalTraits = ","+property.additionalTraits;
		availTraits = (property.defaultTraits+additionalTraits).split(",");
		discardTraits = notScored;
		//remove traits 
		for(int i = 0; i<property.totalScoreIgnoreTraits.length; i++)
		{
			List<String> list = new ArrayList<String>(Arrays.asList(availTraits));
			list.remove(property.totalScoreIgnoreTraits[i]);
			availTraits = list.toArray(new String[0]);
			
			//also remove from discard trait
			if(discardTraits.contains(property.totalScoreIgnoreTraits[i]))
				discardTraits.remove(discardTraits.indexOf(property.totalScoreIgnoreTraits[i]));
			
		}
		
		grades = new HashMap<String, Integer>();
		CEFR = "-3";
		jobSuitability = -3;
		for(Entry<String, FinalScore> entry : scores.entrySet())
			grades.put(entry.getValue().traitName, new Integer(-3));
		
		
		/*
		 * It will be -1 for SVAR report generation i.e. will show NA else the score
		 * */
		
		totalScoreString = "-3";
		calculateScores();		
	}
	
	void calculateScores()
	{
		String moduleID=candObj.allowedModules;
		if(discardTraits.size() == 0 || moduleID.equals("229")) // if there is any traits in discardTraits then total scores and grades will not be calculated
		{
			totalScore = calculateTotalScore(finalScores, moduleVersion, candObj.allowedModules);
			if(moduleVersion.equals("v2.0"))
			{
				grades = calculateGrades(finalScores, grades);
				CEFR = getCEFRLevel(grades);
				jobSuitability =  reportPropObj.getJobSuitability(CEFR);
				totalScore = getNewTotalScore(CEFR, totalScore);
			}
			
			if(moduleVersion.equals("v3.0"))
			{
				grades = calculateGrades(finalScores, grades);
				CEFR = getCEFRLevel(grades);
				jobSuitability =  reportPropObj.getJobSuitability(CEFR);
			}
			totalScoreString = String.format("%.2f", totalScore);
		}
		else
			grades = calculateGrades(finalScores, grades);
	}	
	
	double getNewTotalScore(String Cefr_level , double totalScore)
	{
		double finalTotalScore = 0;
		ScriptEngineManager manager = new ScriptEngineManager();
		ScriptEngine engine = manager.getEngineByName("js");		
		String level_floor = Cefr_level.substring(0,2);
		reportPropObj.updateNormsForTotalScore(level_floor);
		double mean = reportPropObj.mean;
		double sigmma = reportPropObj.sigmma;		
		double totalScore_percentile  = getPercentile(totalScore,mean,sigmma);
		String ts_to_calculate = reportPropObj.ts_eval.replaceAll("percentile", totalScore_percentile+"");
		try {
			finalTotalScore = Double.parseDouble(engine.eval(ts_to_calculate).toString());
		} catch (ScriptException e) {
			System.out.println("Exception in String calculation for total Score "+ts_to_calculate);
			e.printStackTrace();
		}
		return finalTotalScore;
	}
	
	String getCEFRLevel(HashMap<String, Integer> grades)
	{
		String CEFR;
		ScriptEngineManager manager = new ScriptEngineManager();
		ScriptEngine engine = manager.getEngineByName("js");
		LinkedHashMap<String, String> map =  reportPropObj.cefr;
		
		for(Object key: map.keySet())
		{
			CEFR = key.toString();
			String eval_string = map.get(key).toString();			
			for(int i=0; i< availTraits.length; i++)
			{
				eval_string = eval_string.replaceAll(availTraits[i], grades.get(availTraits[i]).toString());
				eval_string = eval_string.replaceAll("totalScore", String.valueOf(totalScore));
			}			
			Object result = null;
			try {
				result = engine.eval(eval_string);
			} catch (ScriptException e) {
				System.out.println("Exception in String calculation for CEFR "+eval_string);
				e.printStackTrace();
			}
			if(Boolean.parseBoolean(result.toString()))
				return CEFR;				
		}
		return "a1";
	}
	
	HashMap<String, Integer> calculateGrades(HashMap<String, FinalScore> finalScores, HashMap<String, Integer>grades)
	{
		for(Entry<String, FinalScore> traitDetails : finalScores.entrySet())
		{
			String traitName =  traitDetails.getValue().traitName;
			if(Arrays.asList(availTraits).contains(traitName))
			{	
				/*
				 * check if SEU in the discard list then don't calculate the 4 scores 
				 * */
				String scoringVersionRegex = "^v2.*";
				if(property.scoringVersion.matches(scoringVersionRegex) || (property.scoringVersion.equals("v1") && (!discardTraits.contains("Spoken English Understanding")))) {
					if(traitName.equals("Spoken English Understanding") && !discardTraits.contains("Spoken English Understanding"))
					{
						String threshold = reportPropObj.spokenEnglishThresholds;
						int grade = getGrade(threshold, Double.parseDouble(traitDetails.getValue().percentile));
						grades.put(traitName, new Integer(grade));
					}
					if(traitName.equals("Pronunciation") && !discardTraits.contains("Pronunciation"))
					{
						String threshold = reportPropObj.pronunciationThresholds;
						int grade = getGrade(threshold, Double.parseDouble(traitDetails.getValue().score));
						grades.put(traitName, new Integer(grade));
					}
					if(traitName.equals("Fluency") && !discardTraits.contains("Fluency"))
					{
						String threshold = reportPropObj.fluencyThresholds;
						int grade = getGrade(threshold, Double.parseDouble(traitDetails.getValue().score));
						grades.put(traitName, new Integer(grade));
					}					
					if(traitName.equals("Active Listening") && !discardTraits.contains("Active Listening"))
					{
						String threshold = reportPropObj.activeListeningThresholds;
						int grade = getGrade(threshold, Double.parseDouble(traitDetails.getValue().score));
						grades.put(traitName, new Integer(grade));
					}
				}
				if(traitName.equals("Grammar") && !discardTraits.contains("Grammar"))
				{
					String threshold = reportPropObj.grammarThresholds;
					int grade = getGrade(threshold, Double.parseDouble(traitDetails.getValue().percentile));
					grades.put(traitName, new Integer(grade));
				}
				else if(traitName.equals("Vocabulary") && !discardTraits.contains("Vocabulary"))
				{
					String threshold = reportPropObj.vocabularyThresholds;
					int grade = getGrade(threshold, Double.parseDouble(traitDetails.getValue().percentile));
					grades.put(traitName, new Integer(grade));
				}
			}
		}
		return grades;
	}
		
	int getGrade(String threshold, double score)
	{
		String []thresholds = threshold.split(",");
		int grade = 1;
		for(int i=0; i<thresholds.length; i++)
		{
			double gScore = Double.parseDouble(thresholds[thresholds.length-i-1]);
			if(score > gScore)
			{
				grade = thresholds.length-i;
				break;
			}
		}
		return grade;
	}
	
	double calculateTotalScore(HashMap<String, FinalScore> finalScores, String scoringVersion, String moduleID)
	{
		double totalScore = 0;
		if(scoringVersion.equals("v2.0"))
		{
			double ts = 0;
			int count=0;
			for(Entry<String, FinalScore> traitDetails : finalScores.entrySet())
			{
				String traitName =  traitDetails.getValue().traitName;
				if(Arrays.asList(availTraits).contains(traitName) && !Arrays.asList(property.totalScoreIgnoreTraits).contains(traitName))
				{
					if(traitName.equals("Grammar") || traitName.equals("Vocabulary") || traitName.equals("Spoken English Understanding"))
						ts+= Double.parseDouble(traitDetails.getValue().percentile);
					else
						ts+= Double.parseDouble(traitDetails.getValue().score);
					count++;
				}
			}
			totalScore = ts/count;
		}
		else if(scoringVersion.equals("v3.0"))
		{
			double P_coeff = 0.25;
			double F_coeff = 0.25;
			double L_coeff = 0.25;
			double R_coeff = 0.25;
			double G_coeff = 0;
			double V_coeff = 0;
			
			if(reportPropObj.isGV == 1){
				P_coeff = 0.2222222222;
				F_coeff = 0.2222222222;
				L_coeff = 0.2222222222;
				R_coeff = 0.1111111111;
				G_coeff = 0.1111111111;
				V_coeff = 0.1111111111;
				
				//if wipro module then assign change the weight age
				if(candObj.candidateID.substring(0, 4).equals("1443"))
				{
					P_coeff = (2.0/7.86);
					F_coeff = (2.0/7.86);
					L_coeff = (2.0/7.86);
					R_coeff = (0.78/7.86);
					G_coeff = (0.55/7.86);
					V_coeff = (0.53/7.86);
				}
			}
			
			else if(reportPropObj.isGV == 2){
					P_coeff = 0.25;
					F_coeff = 0.25;
					L_coeff = 0.25;
					R_coeff = 0.125;
					G_coeff = 0.125;
			}
			
			if(moduleID.equals("229")){
				P_coeff = 0.3333333;
				F_coeff = 0.3333333;
				L_coeff = 0.3333333;
				R_coeff = 0;
				G_coeff = 0;
				V_coeff = 0;
			}
			
			if(reportPropObj.isGV == 0)
				totalScore = F_coeff*Double.parseDouble(finalScores.get("Fluency").score) + P_coeff*Double.parseDouble(finalScores.get("Pronunciation").score) + L_coeff*Double.parseDouble(finalScores.get("Active Listening").score) + R_coeff*Double.parseDouble(finalScores.get("Spoken English Understanding").percentile);
			else if(reportPropObj.isGV == 1)
				totalScore = F_coeff*Double.parseDouble(finalScores.get("Fluency").score) + P_coeff*Double.parseDouble(finalScores.get("Pronunciation").score) + L_coeff*Double.parseDouble(finalScores.get("Active Listening").score) + R_coeff*Double.parseDouble(finalScores.get("Spoken English Understanding").percentile)+ G_coeff*Double.parseDouble(finalScores.get("Grammar").percentile) + V_coeff*Double.parseDouble(finalScores.get("Vocabulary").percentile);
			else //if grammar+vocab is combine then score will b in grammar trait
				totalScore = F_coeff*Double.parseDouble(finalScores.get("Fluency").score) + P_coeff*Double.parseDouble(finalScores.get("Pronunciation").score) + L_coeff*Double.parseDouble(finalScores.get("Active Listening").score) + R_coeff*Double.parseDouble(finalScores.get("Spoken English Understanding").percentile)+ G_coeff*Double.parseDouble(finalScores.get("Grammar").percentile);
		}
		else
		{
			double P_coeff = 0.3333333333;
			double F_coeff = 0.3333333333;
			double L_coeff = 0.1666666666;
			double R_coeff = 0.1666666666;
			double G_coeff = 0;
			double V_coeff = 0;
			
			if((moduleID.equals("93"))||(moduleID.equals("96"))||(moduleID.equals("94"))){
				P_coeff = 0.25;
				F_coeff = 0.25;
				L_coeff = 0.125;
				R_coeff = 0.125;
				G_coeff = 0.125;
				V_coeff = 0.125;
			}
			if(moduleID.equals("229")){
				P_coeff = 0.3333333;
				F_coeff = 0.3333333;
				L_coeff = 0.3333333;
				R_coeff = 0;
				G_coeff = 0;
				V_coeff = 0;
			}
			if(reportPropObj.isGV == 0)
				totalScore = F_coeff*Double.parseDouble(finalScores.get("Fluency").score) + P_coeff*Double.parseDouble(finalScores.get("Pronunciation").score) + L_coeff*Double.parseDouble(finalScores.get("Active Listening").score) + R_coeff*Double.parseDouble(finalScores.get("Spoken English Understanding").percentile);
			else
				totalScore = F_coeff*Double.parseDouble(finalScores.get("Fluency").score) + P_coeff*Double.parseDouble(finalScores.get("Pronunciation").score) + L_coeff*Double.parseDouble(finalScores.get("Active Listening").score) + R_coeff*Double.parseDouble(finalScores.get("Spoken English Understanding").percentile)+ G_coeff*Double.parseDouble(finalScores.get("Grammar").percentile) + V_coeff*Double.parseDouble(finalScores.get("Vocabulary").percentile);
		}		
		return totalScore;
	}

	static String getScoringVersion(Candidate candidateObj, String versionInfo)
	{
		String testDate = ((String)((SerializedPhpParser.PhpObject)candidateObj.TIO).attributes.get("current_date")).split(" ")[0];
		String version= "v1";
		String[] versions =  versionInfo.split(";");
		try{
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        	Date date1 = sdf.parse(testDate);
        	for(int i =0; i< versions.length; i++)
        	{
        		String[] vInfo = versions[i].split(":");
        		version = vInfo[0];
        		Date date2 = sdf.parse(vInfo[1]);
        		if(date1.compareTo(date2)>=0){
        			//System.out.println("Date1 is after Date2");
        			return version;
        		}
        	}
        }catch(ParseException ex){
        	System.out.println("Error in parsing date");
    		ex.printStackTrace();
    	}
		return version;
	}

	static double getPercentile(double score, double mean, double sigma)
	{
		NormalDist normObj = new NormalDist(mean, sigma);
		return normObj.cdf(score);		
	}
}
