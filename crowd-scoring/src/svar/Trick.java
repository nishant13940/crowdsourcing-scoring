package svar;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import libsvm.svm;
import libsvm.svm_model;
import libsvm.svm_node;

public class Trick {
	ArrayList<Integer> countTricks = new ArrayList<Integer>();
	int totalDeduction = 0;
	int totalCountSkips = 0;
	HashMap<Integer, Integer> categoryVersusUtteranceSkips;
	
	
	/** Updated countTricks (By Nitesh) function to include classification of testforms in it.  The features used are articulationRate-std, 
			probSpeakCorrect-Mean, diffN-std, numPh-M. Have used SVM to classify testforms
	 * 
	 */
	ArrayList<Integer> countTricks(HashMap<Integer, ArrayList<Integer>> categoryVersusQuestionID,HashMap<String, Feature> feature, Property property){
		Iterator<Map.Entry<String, Feature>> it = feature.entrySet().iterator();
		
		double countROSTricks = 0;
		int countTestFormTricks = 0;
		ArrayList<Double> articulationRateCand = new ArrayList<Double>();
		ArrayList<Double> probSpeakCorrectCand = new ArrayList<Double>();
		ArrayList<Double> diffNCand = new ArrayList<Double>();
		ArrayList<Double>numPhCand = new ArrayList<Double>();

		
		while(it.hasNext()){
			Map.Entry<String, Feature> entry1 = it.next();
			String temp_uid = entry1.getKey();
			
			
			if(categoryVersusQuestionID.get(property.RSCategory).contains(Integer.parseInt(temp_uid))){
				
				articulationRateCand.add(entry1.getValue().articulationRate);
			    probSpeakCorrectCand.add(entry1.getValue().probSpeakCorrect);
				diffNCand.add(entry1.getValue().diffN);
				numPhCand.add(entry1.getValue().numPh);
				
				if(entry1.getValue().rateOfSpeech > 1.6E-6)
					countROSTricks ++;
			}
		}
		
		countTestFormTricks = classifySVM(getStd(articulationRateCand), getMean(probSpeakCorrectCand), getStd(diffNCand), getMean(numPhCand),property);
		countTricks.add(countTestFormTricks);
	
		boolean temp = (countROSTricks/articulationRateCand.size())>=0.5;
		if(temp==false)
			countTricks.add(0);
		else
			countTricks.add(1);
		
		
		return countTricks;
	}
	
	
	
	private double getMean(ArrayList<Double> number)
	{
		double sum=0;
		for(int i=0;i<number.size();i++)
			sum+= number.get(i);
		
		return sum/number.size();		
	}
	
	
	private  double getStd ( ArrayList<Double> number )
	{
		final int n = number.size();
		if ( n < 2 )
		{
			return Double.NaN;
		}
		double avg = number.get(0);
		double sum = 0;
		for ( int i = 1; i < number.size(); i++ )
		{
			double newavg = avg + ( number.get(i) - avg ) / ( i + 1 );
			sum += ( number.get(i) - avg ) * ( number.get(i) -newavg ) ;
			avg = newavg;
		}
	// Change to ( n - 1 ) to n if you have complete data instead of a sample.
		return Math.sqrt( sum / ( n-1 ) );
	}
    
	
	private int classifySVM(double feature1,double feature2,double feature3,double feature4, Property property)
	{
	
		try{
		svm_model LoadModel = svm.svm_load_model(property.testformModelPath);
		double scalingFactor[] = {6.30112116930903e-07,0.742662335700790,5.7441,48.9166666666667};

		double[] dec_values = new double[1];		
		svm_node[] x = new svm_node[]{new_svm_node(1,feature1/scalingFactor[0]),new_svm_node(2,feature2/scalingFactor[1]),new_svm_node(3, feature3/scalingFactor[2]),new_svm_node(4, feature4/scalingFactor[3])};
		
		int predictedLabel = (int) svm.svm_predict_values(LoadModel,x, dec_values);

		if(predictedLabel==0 && !(dec_values[0]>-1.24))
			return 2;
		else if(predictedLabel==0 || !(dec_values[0]>-1.24))
			return 1;
		}catch (IOException e) {
			System.out.println("Model File not Found");
		}
		return 0;
				
	}
	
	private svm_node new_svm_node(int i,double v)
	{
		svm_node x= new svm_node();
		x.index = i;
		x.value = v;

		return x;
	}

	
	
	
	ArrayList<HashMap<String, Double>> penalizeScore(ArrayList<HashMap<String, Double>> scores, HashMap<Integer, Integer> utteranceSkips, int[] countAttempted, Property property){
		ArrayList<HashMap<String, Double>> penalized = new ArrayList<HashMap<String, Double>>();
		categoryVersusUtteranceSkips = utteranceSkips;
		penalized = penalizeTrick(scores,countAttempted);
		penalized = penalizeUtteranceSkips(penalized,countAttempted, property);
		return penalized;
	}
		ArrayList<HashMap<String, Double>> penalizeTrick(ArrayList<HashMap<String, Double>> scores, int[] countAttempted){
			for(int i = 0; i<scores.size(); ++i){
				HashMap<String, Double> tempScore = new HashMap<String, Double>();
				
				//double trickPerc = (countTricks*1.0)/countAttempted[0];
				tempScore = scores.get(i);
			  Iterator<Map.Entry<String, Double>> it = tempScore.entrySet().iterator();
				while(it.hasNext()){
					Map.Entry<String, Double> entry = (Map.Entry<String, Double>)it.next();
					double percentile = entry.getValue();
					/*if(countTricks > 0){
						if(countTricks >= 9)
							percentile -= 60;
						else
							if(countTricks >= 6)
								percentile -= 40;
							else
								if(countTricks >= 3)
									percentile -= 20;
					}*/
					
					if(countTricks.size() >= 2)
					{
						if(countTricks.get(0)==2)
							percentile-=30;
						else if(countTricks.get(0)==1)
							percentile-=20;
						
						
						if(countTricks.get(1)==1)
							percentile-=40;
					}
					/*
					if(trickPerc > 0.25)
						percentile -= (((trickPerc*100 -25)*0.8)+20); 
					*/
					
					if(percentile < 0)
						percentile = 0.0;
					
					
					tempScore.put(entry.getKey(), percentile);
				}
				scores.remove(i);
				scores.add(i,tempScore);
			}
			return scores;
		}
		
		ArrayList<HashMap<String, Double>> penalizeUtteranceSkips(ArrayList<HashMap<String, Double>> scores, int[] countAttempted, Property property){
			Iterator<Map.Entry<Integer, Integer>> skips = categoryVersusUtteranceSkips.entrySet().iterator();
			while(skips.hasNext()){
				Map.Entry<Integer, Integer> entry = (Map.Entry<Integer, Integer>)skips.next();
				int countSkips = entry.getValue();
				int subCat	   = entry.getKey();
				if(subCat == property.RSCategory || subCat == property.LRCategory){
					totalCountSkips += countSkips;
				}
			}
			double skipPerc = (totalCountSkips*1.0)/(countAttempted[0]+countAttempted[1]+totalCountSkips);
			/*if(totalCountSkips < 8)
				totalDeduction = totalCountSkips*(totalCountSkips+1)/2;
			else
				totalDeduction = 100;*/
			if(skipPerc > 0.25){
				if(skipPerc < 0.9167)
					totalDeduction = (int) ((skipPerc*100 - 25)*.8)+20;
				else
					totalDeduction = 80;
			}

		System.out.println("Number of skips : "+totalCountSkips);
		for(int i = 0; i<scores.size(); ++i){
			HashMap<String, Double> tempScore = new HashMap<String, Double>();
			tempScore = scores.get(i);
			Iterator<Map.Entry<String, Double>> it = tempScore.entrySet().iterator();
			while(it.hasNext()){
				Map.Entry<String, Double> entry = (Map.Entry<String, Double>)it.next();
				double tempVal = entry.getValue();
				tempVal = tempVal -totalDeduction;
				if(tempVal < 0)
					tempVal = 0;
				tempScore.put(entry.getKey(),tempVal);
			}
					
			scores.remove(i);
			scores.add(i,tempScore);
		}
		return scores;			
	}
}