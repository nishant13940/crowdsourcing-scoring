package svar;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.io.FileWriter;

import org.lorecraft.phparser.SerializedPhpParser;

public class DatabaseWrite {
	private static final String String = null;
	String dbtime;
	Property property = new Property();
	
	DatabaseWrite() {} //default constructor
	
	DatabaseWrite(Property property1)
	{
		property = property1;
	}
	
	String dbUrl = property.DB_URL;
	String dbClass = "com.mysql.jdbc.Driver";
	String tableName = property.SVAR_TABLENAME;
	String dbName = property.SVARDBName;
	String dbUser = property.DB_USER;
	String dbPass = property.DB_PASSWD;
	String hostIP = property.HOST_IP;
	String hostName = property.HOST_NAME;
	String dbWriteQuery = "";
	//String dbName = "SVAR";
	//String dbUser = "root";
	//String dbPass = "12345678";
	void writeDB(Candidate candObj, HashMap<String, FinalScore> finalScores, Report reportObj, Trick trickObj, CrowdInput crowdInput){
		try {
			int update = 1;
			Class.forName("com.mysql.jdbc.Driver");
			String query = new String();
			Connection con = DriverManager.getConnection (dbUrl+dbName, dbUser, dbPass);

			// Check of existing entry
			Statement stmtCheck = con.createStatement() ;
			ResultSet rs = stmtCheck.executeQuery( "SELECT totalScore FROM "+tableName+" WHERE SVARID ="+candObj.candidateID ) ;

			if(!rs.next())
				query = "INSERT INTO "+tableName+" (SVARID , AMCATID , testDate , phoneNumber , testformNumber , moduleID , moduleVersion , pronunciationScore , pronunciationPercentile , pronunciationGrade , fluencyScore , fluencyPercentile , fluencyGrade , activeListeningScore , activeListeningPercentile , activeListeningGrade , spokenEnglishUnderstandingScore , spokenEnglishUnderstandingPercentile , spokenEnglishUnderstandingGrade , grammarScore , grammarPercentile , grammarGrade , vocabularyScore , vocabularyPercentile , vocabularyGrade , totalScore , CEFRLevel , jobSuitability , numberTrick , spokenOnTopic, noisyStatus, miscellaneous, scoringVersion, updateDateTime) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			else{
				/*
				if ((rs.getString(1).equals("-1"))||(rs.getString(1).equals("-2"))|| (rs.getString(1).equals("-3")))
					query = "UPDATE " +tableName+ " SET SVARID = ? , AMCATID = ? , testDate = ? , phoneNumber = ? , testformNumber = ? , moduleID = ? , moduleVersion = ? , pronunciationScore = ? , pronunciationPercentile = ? , pronunciationGrade = ? , fluencyScore = ? , fluencyPercentile = ? , fluencyGrade = ? , activeListeningScore = ? , activeListeningPercentile = ? , activeListeningGrade = ? , spokenEnglishUnderstandingScore = ? , spokenEnglishUnderstandingPercentile = ? , spokenEnglishUnderstandingGrade = ? , grammarScore = ? , grammarPercentile = ? , grammarGrade = ? , vocabularyScore = ? , vocabularyPercentile = ? , vocabularyGrade = ? , totalScore = ? , CEFRLevel = ? , jobSuitability = ? , numberTrick = ? , numberSkip = ?, noisyStatus = ?, miscellaneous = ?, scoringVersion = ?, updateDateTime = ? WHERE svarID = "+candObj.candidateID;
				else
				{
					query = "INSERT INTO "+tableName+" (SVARID , AMCATID , testDate , phoneNumber , testformNumber , moduleID , moduleVersion , pronunciationScore , pronunciationPercentile , pronunciationGrade , fluencyScore , fluencyPercentile , fluencyGrade , activeListeningScore , activeListeningPercentile , activeListeningGrade , spokenEnglishUnderstandingScore , spokenEnglishUnderstandingPercentile , spokenEnglishUnderstandingGrade , grammarScore , grammarPercentile , grammarGrade , vocabularyScore , vocabularyPercentile , vocabularyGrade , totalScore , CEFRLevel , jobSuitability , numberTrick , numberSkip, noisyStatus, miscellaneous, scoringVersion, updateDateTime) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
					update = 0;
				}*/
				
				//! Now update the score in each case, now DM may hold the value but we need to update it.
				query = "UPDATE " +tableName+ " SET SVARID = ? , AMCATID = ? , testDate = ? , phoneNumber = ? , testformNumber = ? , moduleID = ? , moduleVersion = ? , pronunciationScore = ? , pronunciationPercentile = ? , pronunciationGrade = ? , fluencyScore = ? , fluencyPercentile = ? , fluencyGrade = ? , activeListeningScore = ? , activeListeningPercentile = ? , activeListeningGrade = ? , spokenEnglishUnderstandingScore = ? , spokenEnglishUnderstandingPercentile = ? , spokenEnglishUnderstandingGrade = ? , grammarScore = ? , grammarPercentile = ? , grammarGrade = ? , vocabularyScore = ? , vocabularyPercentile = ? , vocabularyGrade = ? , totalScore = ? , CEFRLevel = ? , jobSuitability = ? , numberTrick = ? , spokenOnTopic = ?, noisyStatus = ?, miscellaneous = ?, scoringVersion = ?, updateDateTime = ? WHERE svarID = "+candObj.candidateID;
				
			}
			//System.out.println(query);
			///////////////////////////////////////////////////////////////
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setString(1,candObj.candidateID);      //SVARID
			stmt.setString(2,candObj.amcatID);      //AMCATID
			stmt.setString(3,((String)((SerializedPhpParser.PhpObject)candObj.TIO).attributes.get("current_date")).split(" ")[0]);      //testDate
			stmt.setString(4,candObj.phoneNumber);      //phoneNumber
			stmt.setString(5,candObj.testformNumber);      //testformNumber
			stmt.setString(6,candObj.allowedModules);      //moduleID
			stmt.setString(7,reportObj.moduleVersion);      //moduleVersion
			stmt.setString(8,finalScores.get("Pronunciation").score);      //pronunciationScore
			stmt.setString(9,finalScores.get("Pronunciation").percentile);      //pronunciationPercentile
			stmt.setString(10,reportObj.grades.get(finalScores.get("Pronunciation").traitName).toString());      //pronunciationGrade
			stmt.setString(11,finalScores.get("Fluency").score);      //fluencyScore
			stmt.setString(12,finalScores.get("Fluency").percentile);      //fluencyPercentile
			stmt.setString(13,reportObj.grades.get(finalScores.get("Fluency").traitName).toString());      //fluencyGrade
			stmt.setString(14,finalScores.get("Active Listening").score);     //activeListeningScore
			stmt.setString(15,finalScores.get("Active Listening").percentile);      //activeListeningPercentile
			stmt.setString(16,reportObj.grades.get(finalScores.get("Active Listening").traitName).toString());      //activeListeningGrade
			stmt.setString(17,finalScores.get("Spoken English Understanding").score);      //spokenEnglishUnderstandingScore
			stmt.setString(18,finalScores.get("Spoken English Understanding").percentile);      //spokenEnglishUnderstandingPercentile
			stmt.setString(19,reportObj.grades.get(finalScores.get("Spoken English Understanding").traitName).toString());      //spokenEnglishUnderstandingGrade
			stmt.setString(20,finalScores.get("Grammar").score);      //grammarScore
			stmt.setString(21,finalScores.get("Grammar").percentile);      //grammarPercentile
			stmt.setString(22,reportObj.grades.get(finalScores.get("Grammar").traitName).toString());      //grammarGrade
			stmt.setString(23,finalScores.get("Vocabulary").score);      //vocabularyScore
			stmt.setString(24,finalScores.get("Vocabulary").percentile);      //vocabularyPercentile
			stmt.setString(25,reportObj.grades.get(finalScores.get("Vocabulary").traitName).toString());      //vocabularyGrade
			stmt.setString(26,finalScores.get("totalScore").score.toString());      //totalScore
			stmt.setString(27,reportObj.CEFR.toUpperCase());      //CEFRLevel
			stmt.setString(28,reportObj.jobSuitability+"");      //jobSuitability
			// Changed because structure of count tricks has changed
			//changes by ankit done @ 21 march 13
			//author - Nitesh and Vinay
			
			String testformClassification = "";
			if(trickObj.countTricks.size()>0 && trickObj.countTricks.get(0) == 0)
				testformClassification = "Correct Testform";
			else if(trickObj.countTricks.size()>0 && trickObj.countTricks.get(0) == 1)
				testformClassification = "Maybe Incorrect Testform";
			else
				testformClassification = "Incorrect Testform";
			
			if(trickObj.countTricks.size()>1 && trickObj.countTricks.get(1) == 0)
				testformClassification += " (No ROS bias)";
			else
				testformClassification += " (With ROS bias)";
			
			stmt.setString(29,testformClassification);      //numberTrick
			
			
			stmt.setString(30,crowdInput.spokenOnTopic);      //numberSkip				
			stmt.setString(31,crowdInput.isNoisy); //also write the sample noisy status into DB
			
			//to store indianism score in DB; nw we make a new field misc. in which we store all new value
			Map<String, String> miscellaneousVal = new HashMap<String, String>();
			
			miscellaneousVal.put("IndianismScore", finalScores.get("Indianism").score);
			miscellaneousVal.put("IndianismPercentile", finalScores.get("Indianism").percentile);
			
			Iterator<Entry<String, String>> it = candObj.candidateTag.entrySet().iterator();
			while (it.hasNext()) {
				Entry<String, String> entry = it.next();
				miscellaneousVal.put("tag_"+entry.getKey(), entry.getValue());
			}
			
			miscellaneousVal.put("customerHandlingScore", finalScores.get("Customer Handling Skill").percentile);
			
			
			String serializeString = PHPSerialization.serialize(miscellaneousVal);
			stmt.setString(32,serializeString);      //Indianism Score and candidate Tag values
			
			stmt.setString(33,property.scoringVersion);      //Scoring version
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date date = new Date();
			stmt.setString(34,(dateFormat.format(date)).toString());//current date time
			
			int i =0;
			String[] DBQry = stmt.toString().split(":");
			for(String tempD : DBQry)
			{
				if(i++ > 0)
					dbWriteQuery += tempD+":";
			}
			dbWriteQuery = dbWriteQuery.substring(0, dbWriteQuery.length()-1);
			System.out.println(dbWriteQuery);

			if (update == 1)
				stmt.executeUpdate();
			else
			{
				System.out.println("Repeated Entry : Already in DB with a valid values"); //add by ankit
				writeDbErrorHandler(candObj, dbWriteQuery, 2);
			}		
			rs.close() ;
			stmtCheck.close() ;
		}
		catch(Exception e){
			System.out.println("Issue in writing DB | with valid scores");
			writeDbErrorHandler(candObj, dbWriteQuery,1);
			e.printStackTrace();
		}
	}

	//function added by ankit to make entry in DB if any scoring is not done 
	void writeDBScoringError(String candidateID, String issue)
	{
		String tbl_notScoring = "tbl_notScored";
		System.out.println(tbl_notScoring);
		try
		{
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection (dbUrl+dbName, dbUser, dbPass);
			String query = new String();
			Statement stmtCheck = con.createStatement() ;
			ResultSet rs = stmtCheck.executeQuery( "SELECT numberOfTimesCalled FROM "+tbl_notScoring+" WHERE svarID ="+candidateID ) ;
			int timesScoringCalled = 1;
			if(!rs.next())

				query = "INSERT INTO "+tbl_notScoring+" (svarID, scoringDate, scoringIssue, status, serverIP, serverName, numberOfTimesCalled) VALUES (?,?,?,?,?,?,?)";
			else
			{
				timesScoringCalled = rs.getInt(1)+1;
				query = "UPDATE " +tbl_notScoring+ " SET svarID = ? , scoringDate = ? , scoringIssue = ? , status = ? , serverIP = ? , serverName = ?, numberOfTimesCalled = ? WHERE svarID = "+candidateID;
			}

			System.out.println(query);
			PreparedStatement stmt = con.prepareStatement(query);
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date date = new Date();

			stmt.setString(1,candidateID);
			stmt.setString(2,(dateFormat.format(date)).toString());
			stmt.setString(3,issue);
			stmt.setInt(4,0);
			stmt.setString(5,hostIP);
			stmt.setString(6,hostName);
			stmt.setInt(7,timesScoringCalled);
			stmt.executeUpdate();		
		}
		catch(Exception e){
			System.out.println("Issue in writing DB when update in not Scored table");
			e.printStackTrace();
		}		
	}

	void writeDbErrorHandler(Candidate candidateObj, String query, int typeError)
	{		
		String tableName_errorDB = new String();
		int i =2;
		switch(typeError)
		{
		case 1: tableName_errorDB = "writeDbError";
		break;
		case 2: tableName_errorDB = "repeatedEntry";
		break;
		}
		System.out.println(tableName_errorDB);
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection (dbUrl+dbName, dbUser, dbPass);			
			// New Query to write to WriteDbError
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date date = new Date();
			String query_errorDB = "INSERT INTO "+tableName_errorDB+" (svarID, Date, SQL_Query, serverIP, serverName) VALUES (?,?,?,?,?)";
			PreparedStatement stmt_errorDB = con.prepareStatement(query_errorDB);
			stmt_errorDB.setString(1,candidateObj.candidateID);
			stmt_errorDB.setString(2,(dateFormat.format(date)).toString());
			stmt_errorDB.setString(3,query);
			stmt_errorDB.setString(4,hostIP);
			stmt_errorDB.setString(5,hostName);
			stmt_errorDB.executeUpdate();		
		}
		catch(Exception e){
			System.out.println("Issue in writing DB | in DB Handler");
			e.printStackTrace();
		}
	}

	void writeDB(Candidate candObj, int error, String moduleVersion){
		
		String query = "INSERT INTO "+tableName+" (SVARID , AMCATID , testDate , phoneNumber , testformNumber , moduleID , moduleVersion , pronunciationScore , pronunciationPercentile , pronunciationGrade , fluencyScore , fluencyPercentile , fluencyGrade , activeListeningScore , activeListeningPercentile , activeListeningGrade , spokenEnglishUnderstandingScore , spokenEnglishUnderstandingPercentile , spokenEnglishUnderstandingGrade , grammarScore , grammarPercentile , grammarGrade , vocabularyScore , vocabularyPercentile , vocabularyGrade , totalScore , CEFRLevel , jobSuitability , numberTrick , numberSkip, noisyStatus, miscellaneous) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		String writeError = error+"";
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection (dbUrl+dbName, dbUser, dbPass);
			
			Statement stmtCheck = con.createStatement() ;
			ResultSet rs = stmtCheck.executeQuery( "SELECT totalScore FROM "+tableName+" WHERE SVARID ="+candObj.candidateID ) ;
			
			if(!rs.next())
			{			
				PreparedStatement stmt = con.prepareStatement(query);
				stmt.setString(1,candObj.candidateID);
				if(error != -2)
				{
					stmt.setString(2,candObj.amcatID);      //AMCATID
					stmt.setString(3,((String)((SerializedPhpParser.PhpObject)candObj.TIO).attributes.get("current_date")).split(" ")[0]);      //testDate
					stmt.setString(4,candObj.phoneNumber);      //phoneNumber
					if(candObj.testformNumber != "NULL")
						stmt.setString(5,candObj.testformNumber);      //testformNumber
					else
						stmt.setString(5,"-3");      //testformNumber
					stmt.setString(6,candObj.allowedModules);      //moduleID
					stmt.setString(31,candObj.noisyStatus);      //noisy status
					
					Map<String, String> miscellaneousVal = new HashMap<String, String>();
					Iterator<Entry<String, String>> it = candObj.candidateTag.entrySet().iterator();
					while (it.hasNext()) {
						Entry<String, String> entry = it.next();
						miscellaneousVal.put("tag_"+entry.getKey(), entry.getValue());
					}
					String serializeString = PHPSerialization.serialize(miscellaneousVal);
					stmt.setString(32,serializeString);      //Candidate Tag
					
				}
				else
				{
					stmt.setString(2,writeError);      //AMCATID
					stmt.setString(3,"0000-00-00");      //testDate
					stmt.setString(4,writeError);      //phoneNumber
					stmt.setString(5,writeError);      //testformNumber
					stmt.setString(6,writeError);      //moduleID
					stmt.setString(31,writeError);      //noisy status
					stmt.setString(32,writeError);      //candidate Tag
				}	
				
				stmt.setString(7,moduleVersion);      //moduleVersion
				stmt.setString(8,writeError);      //pronunciationScore
				stmt.setString(9,writeError);      //pronunciationPercentile
				stmt.setString(10,writeError);      //pronunciationGrade
				stmt.setString(11,writeError);      //fluencyScore
				stmt.setString(12,writeError);      //fluencyPercentile
				stmt.setString(13,writeError);      //fluencyGrade
				stmt.setString(14,writeError);     //activeListeningScore
				stmt.setString(15,writeError);      //activeListeningPercentile
				stmt.setString(16,writeError);      //activeListeningGrade
				stmt.setString(17,writeError);      //spokenEnglishUnderstandingScore
				stmt.setString(18,writeError);      //spokenEnglishUnderstandingPercentile
				stmt.setString(19,writeError);      //spokenEnglishUnderstandingGrade
				stmt.setString(20,writeError);      //grammarScore
				stmt.setString(21,writeError);      //grammarPercentile
				stmt.setString(22,writeError);      //grammarGrade
				stmt.setString(23,writeError);      //vocabularyScore
				stmt.setString(24,writeError);      //vocabularyPercentile
				stmt.setString(25,writeError);      //vocabularyGrade
				stmt.setString(26,writeError);      //totalScore
				stmt.setString(27,writeError);      //CEFRLevel
				stmt.setString(28,writeError);      //jobSuitability
				stmt.setString(29,writeError);      //numberTrick
				stmt.setString(30,writeError);      //numberSkip
				stmt.executeUpdate();
			}
		}
		catch(Exception e){
			System.out.println("Issue in writing DB : try to set score "+writeError);
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @param obj
	 * @param errorStatus
	 * @param errorMessage
	 * @param data
	 * If the System is unable to send data  on corpMIS then make a corresponding entry in post2APIerror table
	 * now it also save the string to be send on corpMIS 
	 */
	void postResultErrorHandler(Candidate obj, String errorStatus, String errorMessage, String data){
		
		String tableName_errorDB = "post2ApiError";
		try {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		String query_errorDB = "INSERT INTO "+tableName_errorDB+" (svarID, errorStatus,errorMessage, Date, serverIP, serverName, postString) VALUES (?,?,?,?,?,?,?)";
		Connection con = DriverManager.getConnection (dbUrl+dbName, dbUser, dbPass);
		PreparedStatement stmt_error = con.prepareStatement(query_errorDB);
		
		stmt_error.setString(1,obj.candidateID);
		stmt_error.setString(2,errorStatus);
		stmt_error.setString(3,errorMessage);
		stmt_error.setString(4,(dateFormat.format(date)).toString());
		stmt_error.setString(5,hostIP);
		stmt_error.setString(6,hostName);
		stmt_error.setString(7,data);
		
		stmt_error.executeUpdate();
		}
		catch(Exception e){
			System.out.println("Issue in writing DB | Error in POST result");
			e.printStackTrace();
		}
	}
	
	/** 
	 * @param obj
	 * If a candidate is already present in DB then set its status 1	 * 
	 */
	void postResultResetStatus(Candidate obj){
		
		String tableName_errorDB = "post2ApiError";
		try {
			String query_errorDB = "UPDATE "+tableName_errorDB+" SET status = 1 where svarID = '"+obj.candidateID+"'";
			Connection con = DriverManager.getConnection (dbUrl+dbName, dbUser, dbPass);
			PreparedStatement stmt_error = con.prepareStatement(query_errorDB);
			stmt_error.executeUpdate();
		}
		catch(Exception e){
			System.out.println("Issue in Upadte post result API DB");
			e.printStackTrace();
		}
	}
	
	String getTestFormNumber(String testFormID, String moduleID, String moduleVersion) throws SQLException, ClassNotFoundException
	{
		String testFormNumber;
		String tName = "tbl_mapForms"+moduleID;
		if(!moduleVersion.equals("-1"))
			tName = "tbl_mapForms"+moduleID+"_"+moduleVersion;
		Class.forName("com.mysql.jdbc.Driver");
		Connection con = DriverManager.getConnection (dbUrl+"db_amsat", dbUser, dbPass);
		Statement stmtCheck = con.createStatement() ;
		ResultSet rs;
		rs = stmtCheck.executeQuery( "SELECT mapID FROM "+tName+" WHERE testID ="+testFormID);
		if(rs.next())
			testFormNumber = rs.getString(1);
		else
			testFormNumber = "";
		return testFormNumber;
	}
	
	void changeScoringStatus(int status,String candidateId)
	{
		String tSchedulerName = "tbl_scoringScheduled";
		try
		{
			Class.forName("com.mysql.jdbc.Driver");
			String query_DB = "UPDATE "+tSchedulerName+" SET status = "+status+", completedAt = NOW() where svarID = '"+candidateId+"'";
			Connection con = DriverManager.getConnection (dbUrl+dbName, dbUser, dbPass);
			PreparedStatement stmt = con.prepareStatement(query_DB);
			stmt.executeUpdate();
		}
		catch(Exception e){
			System.out.println("Issue while updating scoring status. Check or scoring will crash due to load.");
			e.printStackTrace();
		}
	}
}