package svar;

public class NoiseReduce {
	String candidateID;
	Property property = new Property();
	NoiseReduce (String input_candidateID){
		candidateID = input_candidateID;
	}
	
	int reduceNoise() throws Exception{
		System.out.println("In NoiseReduce.reduceNoise()");
		Process process = Runtime.getRuntime().exec("cp -r "+property.unzippedFilesLocation+candidateID+" "+property.noiseReducedFilesLocation);
		process.waitFor();
		return 1;
	}
}

