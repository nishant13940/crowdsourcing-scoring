package svar;

import java.util.Map;

public class CrowdInput {
		public String candId;
		public String PCrowdScore;
		public String FCrowdScore;
		public String CCrowdScore;
		public String GCrowdScore;
		public String isNoisy;
		public String spokenOnTopic;
		public String Transcription;
		
		CrowdInput(){}
		
		
		CrowdInput(Map<String, String> crowdMap){
			this.candId			=(String) crowdMap.get("candId");
			this.PCrowdScore	=crowdMap.get("PScore");
			this.FCrowdScore	=crowdMap.get("FScore");
			this.CCrowdScore	=crowdMap.get("CScore");
			this.GCrowdScore	=crowdMap.get("GScore");
			this.isNoisy		=crowdMap.get("isNoisy");
			this.spokenOnTopic	=crowdMap.get("spokenOnTopic");
			this.Transcription	=(crowdMap.get("Transcription")).replaceAll("\\s+", " ").toLowerCase();
		}
}
