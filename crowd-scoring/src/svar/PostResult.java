package svar;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map.Entry;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.X509TrustManager;
import javax.net.ssl.TrustManager;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.lorecraft.phparser.SerializedPhpParser;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import java.util.regex.*;
import java.util.*;

public class PostResult {
	Property property = new Property();

	public void postResult2API(Candidate candidateObj, HashMap<String, FinalScore> finalScores, Report reportObj, CrowdInput crowdInput) throws SAXException, IOException, ParserConfigurationException, KeyManagementException, NoSuchAlgorithmException, SQLException {
		// TODO Auto-generated method stub
		// Create a trust manager that does not validate certificate chains

		TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			public void checkClientTrusted(X509Certificate[] arg0,
					String arg1) throws CertificateException {
				// TODO Auto-generated method stub
			}

			public void checkServerTrusted(X509Certificate[] arg0,
					String arg1) throws CertificateException {
				// TODO Auto-generated method stub
			}
		}
		};

		// Install the all-trusting trust manager	
		SSLContext sc = SSLContext.getInstance("SSL");	
		sc.init(null, trustAllCerts, new java.security.SecureRandom());	
		HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());	 

		// Create all-trusting host name verifier	
		HostnameVerifier allHostsValid = new HostnameVerifier() {

			public boolean verify(String hostname, SSLSession session) {
				// TODO Auto-generated method stub
				return true;
			}
		};

		// Install the all-trusting host verifier

		HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

		// Using Module ID to read config
		String moduleID = candidateObj.allowedModules;

		//Making ModuleID Compatible
		if (moduleID.equals("1")){
			moduleID = "90";
		}

		if (moduleID.equals("2")){
			moduleID = "91";
		}

		//Read Configuration

		DocumentBuilderFactory config_dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder config_db = config_dbf.newDocumentBuilder();
		Document configDoc = config_db.parse(new File(property.post2APIconfigsLocation));

		
		Node configStatusNode = null;
		
		if(configDoc.getElementsByTagName("module_"+moduleID+"_"+candidateObj.deliverModuleVersion).item(0) != null)
			configStatusNode = configDoc.getElementsByTagName("module_"+moduleID+"_"+candidateObj.deliverModuleVersion).item(0);
		else
			configStatusNode = configDoc.getElementsByTagName("module_"+moduleID).item(0);
	
		
		Element configStatuses = (Element)configStatusNode;

		NodeList noOfSubSectionList = configStatuses.getElementsByTagName("noOfSubSection");
		Element noOfSubSectionElement = (Element)noOfSubSectionList.item(0);

		NodeList textNoOfSubSection = noOfSubSectionElement.getChildNodes();
		String noOfSubSectionString = ((Node)textNoOfSubSection.item(0)).getNodeValue().trim();
		//System.out.println("noOfSubSection: " + noOfSubSectionString);


		NodeList scoreSequenceList = configStatuses.getElementsByTagName("scoreSequence");
		Element scoreSequenceElement = (Element)scoreSequenceList.item(0);

		NodeList textScoreSequence = scoreSequenceElement.getChildNodes();
		String scoreSequenceString = ((Node)textScoreSequence.item(0)).getNodeValue().trim();
		//System.out.println("scoreSequence: " + scoreSequenceString);

		NodeList scorePercentileCheckList = configStatuses.getElementsByTagName("scorePercentileCheck");
		Element scorePercentileCheckElement = (Element)scorePercentileCheckList.item(0);

		NodeList textScorePercentileCheck = scorePercentileCheckElement.getChildNodes();
		String scorePercentileCheckString = ((Node)textScorePercentileCheck.item(0)).getNodeValue().trim();
		//System.out.println("scorePercentileCheck: " + scorePercentileCheckString);

		NodeList subSectionSequenceList = configStatuses.getElementsByTagName("subSectionSequence");
		Element subSectionSequenceElement = (Element)subSectionSequenceList.item(0);

		NodeList textSubSectionSequence = subSectionSequenceElement.getChildNodes();
		String subSectionSequenceString = ((Node)textSubSectionSequence.item(0)).getNodeValue().trim();
		//System.out.println("subSectionSequence: " +	subSectionSequenceString);

		//}


		//Create Data
		String testDate = ((String)((SerializedPhpParser.PhpObject)candidateObj.TIO).attributes.get("current_date")).split(" ")[0];
		/*int[] scoreSequence = {1,0,2,3,5,6,8,4};
	        int[] scorePercentileCheck = {2,2,2,2,1,1,0,0};        //0: score, 1: Percentile, 2: Both
	        String[] subSectionSequence = {"Pronunciation","Fluency","Active Listening","Spoken English Understanding","Overall Spoken English Skills","Overall English Listening and Comprehension Skills","English Language Proficiency Score","Job Suitability"};
		 */

		int noOfSubSection = Integer.parseInt(noOfSubSectionString);
		String[] scoreSequenceSplit = scoreSequenceString.split(",");
		String[] scorePercentileCheckSplit = scorePercentileCheckString.split(",");
		String[] subSectionSequence = subSectionSequenceString.split(","); 
		int[] scoreSequence = new int[noOfSubSection];
		int[] scorePercentileCheck = new int[noOfSubSection];
		for (int i=0;i<noOfSubSection;i++)
		{
			scoreSequence[i] = Integer.parseInt(scoreSequenceSplit[i]);
			scorePercentileCheck[i] = Integer.parseInt(scorePercentileCheckSplit[i]);

		}
		//get the test form number
		//Added by ankit @ 31-Dec-2012
		//author - sumit, required by IGT
		String testNumber = "";
		try {
			testNumber = new DatabaseWrite().getTestFormNumber(candidateObj.testformNumber, moduleID, candidateObj.deliverModuleVersion);
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			System.out.println("Error in get the test form number for the test ID :"+candidateObj.testformNumber);
			e1.printStackTrace();
		}
		
		//end
		//0: score, 1: Percentile, 2: Both 3: Percentile as Score

		String data = "updatePwd=" + "updateScore" + "&svarId=" + candidateObj.candidateID + "&amcatId=" + candidateObj.amcatID + "&phone=" + candidateObj.phoneNumber + "&testDate=" + testDate + "&moduleID=" + moduleID + "&noOfSubSection=" +((Integer)(scoreSequence.length)).toString();
		String subSectionQuery = new String();

		String reportParam = new String();
		
		for(int i=1;i<=subSectionSequence.length;i++)
		{
			switch (scorePercentileCheck[i-1])
			{			
			case 0:	subSectionQuery = subSectionQuery + "&scores[" + subSectionSequence[i-1] + "][score]=" + finalScores.get(subSectionSequence[i-1]).score + "&scores["+ subSectionSequence[i-1] + "][percentile]=-3";
			break;
			case 1: subSectionQuery = subSectionQuery + "&scores[" + subSectionSequence[i-1] + "][score]=-3" + "&scores[" + subSectionSequence[i-1] + "][percentile]=" + finalScores.get(subSectionSequence[i-1]).percentile;
			break;
			case 2: subSectionQuery = subSectionQuery + "&scores[" + subSectionSequence[i-1] + "][score]=" + finalScores.get(subSectionSequence[i-1]).score + "&scores["+ subSectionSequence[i-1] + "][percentile]=" + finalScores.get(subSectionSequence[i-1]).percentile;
			break;
			case 3: subSectionQuery = subSectionQuery + "&scores[" + subSectionSequence[i-1] + "][score]=" + finalScores.get(subSectionSequence[i-1]).percentile + "&scores["+ subSectionSequence[i-1] + "][percentile]=-3";
			break;
			}
			reportParam = reportParam + "&reportParam[grades]["+subSectionSequence[i-1]+"]="+reportObj.grades.get(subSectionSequence[i-1])+"scores[totalScore][score]="+finalScores.get("totalScore").score+"&spokenOnTopic="+crowdInput.spokenOnTopic+"&isNoisy="+crowdInput.isNoisy;
		}

		String mScoreQuery = new String();
		mScoreQuery = mScoreQuery + "&mScore=" + reportObj.totalScoreString + "&mPercentile=-3";
		
		reportParam = reportParam + "&reportParam[moduleVersion]=" + reportObj.moduleVersion + "&reportParam[CEFRLevel]="+reportObj.CEFR.toUpperCase().substring(0,2)+"&reportParam[jobSuitability]="+reportObj.jobSuitability;
		reportParam = reportParam + "&reportParam[modDeliveryVer]=" + candidateObj.deliverModuleVersion;
		
		String candidateTag = new String();		
		Iterator<Entry<String, String>> it = candidateObj.candidateTag.entrySet().iterator();
		while (it.hasNext()) {
			Entry<String, String> entry = it.next();
			candidateTag += "&candidateTag["+entry.getKey()+"]=" + entry.getValue();
		}
		
		data = data + subSectionQuery + mScoreQuery+reportParam + "&passkey=" + candidateObj.passKey + "&testNumber=" + testNumber + candidateTag;
		System.out.println(data);
		
		//Connect to API
		URL url = new URL(property.CORPMIS_URL);
		URLConnection conn = url.openConnection();
		conn.setDoOutput(true);
		OutputStreamWriter writer = null;
		try{
			writer= new OutputStreamWriter(conn.getOutputStream());
			//write parameters
			writer.write(data);
			writer.flush();
		
		
	
		// Get the response
		
            StringBuffer answer = new StringBuffer();
            BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                answer.append(line);
            }
            writer.close();
            reader.close();
            //Output the response
            String errorStatus,errorMessage;
            System.out.println(answer.toString());
            
            Pattern p1=Pattern.compile("<errorStatus>(.*)</errorStatus>");
    		Matcher m1=p1.matcher(answer.toString());
    		if(m1.find())
    		{
    			errorStatus=m1.group(1);
    			if(!errorStatus.trim().equals("0"))
    			{
    				Pattern p2=Pattern.compile("<errorMessage>(.*)</errorMessage>");
    	    		Matcher m2=p2.matcher(answer.toString());
    	    		if(m2.find())
    	    		{
    	    			errorMessage=m2.group(1);
    	    			DatabaseWrite dbObj = new DatabaseWrite();
    	            	dbObj.postResultErrorHandler(candidateObj,errorStatus,errorMessage, data);
    	    		}
    	    		else
    	    		{
    	    			DatabaseWrite dbObj = new DatabaseWrite();
    	            	dbObj.postResultErrorHandler(candidateObj,errorStatus,"No Error Message received", data);
    	    		}


    			}
    		}
    		else
    		{
    			DatabaseWrite dbObj = new DatabaseWrite();
            	dbObj.postResultErrorHandler(candidateObj,"-1","No Message received from Server", data);
    		}
		}
		catch(Exception e)
		{
			DatabaseWrite dbObj = new DatabaseWrite();
			e.printStackTrace();
        	dbObj.postResultErrorHandler(candidateObj,"-3","Exception in sending data", data);
			
		}
    		
    		
    		
	}
	
	
	public void postResult2API(Candidate obj, int errorNumber, String moduleVersion) throws Exception {

		// Create a trust manager that does not validate certificate chains

		TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {

			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			public void checkClientTrusted(X509Certificate[] arg0,
					String arg1) throws CertificateException {
				// TODO Auto-generated method stub
			}

			public void checkServerTrusted(X509Certificate[] arg0,
					String arg1) throws CertificateException {
				// TODO Auto-generated method stub
			}
		}
		};



		// Install the all-trusting trust manager

		SSLContext sc = SSLContext.getInstance("SSL");

		sc.init(null, trustAllCerts, new java.security.SecureRandom());

		HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());



		// Create all-trusting host name verifier

		HostnameVerifier allHostsValid = new HostnameVerifier() {

			public boolean verify(String hostname, SSLSession session) {
				// TODO Auto-generated method stub
				return true;
			}

		};

		// Install the all-trusting host verifier

		HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

		// Using Module ID to read config
		String moduleID = obj.allowedModules;

		//Making ModuleID Compatible
		if (moduleID.equals("1")){
			moduleID = "90";
		}

		if (moduleID.equals("2")){
			moduleID = "91";
		}

		//get the test form number
		//Added by ankit @ 31-Dec-2012
		//author - sumit, required by IGT
		String testNumber = "";
		try {
			testNumber = new DatabaseWrite().getTestFormNumber(obj.testformNumber, moduleID, obj.deliverModuleVersion);
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			System.out.println("Error in get the test form number for the test ID :"+obj.testformNumber);
			e1.printStackTrace();
		}
		
		//end
		
		
		
		//Read Configuration

		DocumentBuilderFactory config_dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder config_db = config_dbf.newDocumentBuilder();
		Document configDoc = config_db.parse(new File(property.post2APIconfigsLocation));

		NodeList configOutputlist = configDoc.getElementsByTagName("module_"+moduleID);
		//for(int j = 0; j<configOutputlist.getLength(); ++j){
		//Node configStatusNode = configOutputlist.item(j);

		Node configStatusNode = configOutputlist.item(0);
		Element configStatuses = (Element)configStatusNode;

		NodeList noOfSubSectionList = configStatuses.getElementsByTagName("noOfSubSection");
		Element noOfSubSectionElement = (Element)noOfSubSectionList.item(0);

		NodeList textNoOfSubSection = noOfSubSectionElement.getChildNodes();
		String noOfSubSectionString = ((Node)textNoOfSubSection.item(0)).getNodeValue().trim();
		//System.out.println("noOfSubSection: " + noOfSubSectionString);


		NodeList scoreSequenceList = configStatuses.getElementsByTagName("scoreSequence");
		Element scoreSequenceElement = (Element)scoreSequenceList.item(0);

		NodeList textScoreSequence = scoreSequenceElement.getChildNodes();
		String scoreSequenceString = ((Node)textScoreSequence.item(0)).getNodeValue().trim();
		//System.out.println("scoreSequence: " + scoreSequenceString);

		NodeList scorePercentileCheckList = configStatuses.getElementsByTagName("scorePercentileCheck");
		Element scorePercentileCheckElement = (Element)scorePercentileCheckList.item(0);

		NodeList textScorePercentileCheck = scorePercentileCheckElement.getChildNodes();
		String scorePercentileCheckString = ((Node)textScorePercentileCheck.item(0)).getNodeValue().trim();
		//System.out.println("scorePercentileCheck: " + scorePercentileCheckString);

		NodeList subSectionSequenceList = configStatuses.getElementsByTagName("subSectionSequence");
		Element subSectionSequenceElement = (Element)subSectionSequenceList.item(0);

		NodeList textSubSectionSequence = subSectionSequenceElement.getChildNodes();
		String subSectionSequenceString = ((Node)textSubSectionSequence.item(0)).getNodeValue().trim();
		//System.out.println("subSectionSequence: " +	subSectionSequenceString);

		//}


		//Create Data
		String testDate = ((String)((SerializedPhpParser.PhpObject)obj.TIO).attributes.get("current_date")).split(" ")[0];
		/*int[] scoreSequence = {1,0,2,3,5,6,8,4};
	        int[] scorePercentileCheck = {2,2,2,2,1,1,0,0};        //0: score, 1: Percentile, 2: Both
	        String[] subSectionSequence = {"Pronunciation","Fluency","Active Listening","Spoken English Understanding","Overall Spoken English Skills","Overall English Listening and Comprehension Skills","English Language Proficiency Score","Job Suitability"};
		 */

		int noOfSubSection = Integer.parseInt(noOfSubSectionString);
		String[] scoreSequenceSplit = scoreSequenceString.split(",");
		String[] scorePercentileCheckSplit = scorePercentileCheckString.split(",");
		String[] subSectionSequence = subSectionSequenceString.split(","); 
		int[] scoreSequence = new int[noOfSubSection];
		int[] scorePercentileCheck = new int[noOfSubSection];
		for (int i=0;i<noOfSubSection;i++)
		{
			scoreSequence[i] = Integer.parseInt(scoreSequenceSplit[i]);
			scorePercentileCheck[i] = Integer.parseInt(scorePercentileCheckSplit[i]);

		}
		//0: score, 1: Percentile, 2: Both

		String data = "updatePwd=" + "updateScore" + "&svarId=" + obj.candidateID + "&amcatId=" + obj.amcatID + "&phone=" + obj.phoneNumber + "&testDate=" + testDate + "&moduleID=" + moduleID + "&noOfSubSection=" +((Integer)(scoreSequence.length)).toString();
		String subSectionQuery = new String();
		String reportParam = new String();
		
		for(int i=1;i<=subSectionSequence.length;i++)
		{
			switch (scorePercentileCheck[i-1])
			{			
			case 0: subSectionQuery = subSectionQuery + "&scores[" + subSectionSequence[i-1] + "][score]=" + errorNumber + "&scores["+ subSectionSequence[i-1] + "][percentile]=-3"+"&scores["+ subSectionSequence[i-1] + "][grade]="+errorNumber;
			break;
			case 1: subSectionQuery = subSectionQuery + "&scores[" + subSectionSequence[i-1] + "][score]=-3" + "&scores[" + subSectionSequence[i-1] + "][percentile]=" + errorNumber;
			break;
			case 2: subSectionQuery = subSectionQuery + "&scores[" + subSectionSequence[i-1] + "][score]=" + errorNumber + "&scores["+ subSectionSequence[i-1] + "][percentile]=" + errorNumber;
			break;
			case 3: subSectionQuery = subSectionQuery + "&scores[" + subSectionSequence[i-1] + "][score]=" + errorNumber + "&scores["+ subSectionSequence[i-1] + "][percentile]=-3";
			break;
			}
			reportParam = reportParam + "&reportParam[grades]["+subSectionSequence[i-1]+"]="+errorNumber;
		}

		String mScoreQuery = new String();
		mScoreQuery = mScoreQuery + "&mScore=" + errorNumber + "&mPercentile=-3";  
       
		reportParam = reportParam + "&reportParam[moduleVersion]=" + moduleVersion + "&reportParam[CEFRLevel]="+errorNumber+"&reportParam[jobSuitability]="+errorNumber;
		reportParam = reportParam + "&reportParam[modDeliveryVer]=" + obj.deliverModuleVersion;
		
		String candidateTag = new String();		
		Iterator<Entry<String, String>> it = obj.candidateTag.entrySet().iterator();
		while (it.hasNext()) {
			Entry<String, String> entry = it.next();
			candidateTag += "&candidateTag["+entry.getKey()+"]=" + entry.getValue();
		}
		
		data = data + subSectionQuery + mScoreQuery+reportParam + "&passkey=" + obj.passKey+ "&testNumber=" + testNumber + candidateTag;
		System.out.println(data);
		
		URL url = new URL(property.CORPMIS_URL);
		URLConnection conn = url.openConnection();

		conn.setDoOutput(true);

		OutputStreamWriter writer = null;
		try{
			writer= new OutputStreamWriter(conn.getOutputStream());

			//write parameters
			writer.write(data);
			writer.flush();
		}
		catch(Exception e)
		{
			//DatabaseWrite dbObj = new DatabaseWrite();
        	//dbObj.postResultErrorHandler(obj,"-3","Exception in sending data");
			
		}

		// Get the response
		
        StringBuffer answer = new StringBuffer();
        BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line;
        while ((line = reader.readLine()) != null) {
            answer.append(line);
        }
        writer.close();
        reader.close();
        //Output the response
        String errorStatus,errorMessage;
        System.out.println(answer.toString());
        
        Pattern p1=Pattern.compile("<errorStatus>(.*)</errorStatus>");
		Matcher m1=p1.matcher(answer.toString());
		if(m1.find())
		{
			errorStatus=m1.group(1);
			if(!errorStatus.trim().equals("0"))
			{
				Pattern p2=Pattern.compile("<errorMessage>(.*)</errorMessage>");
	    		Matcher m2=p2.matcher(answer.toString());
	    		if(m2.find())
	    		{
	    			errorMessage=m2.group(1);
	    			DatabaseWrite dbObj = new DatabaseWrite();
	            	dbObj.postResultErrorHandler(obj,errorStatus,errorMessage, data);
	    		}
	    		else
	    		{
	    			DatabaseWrite dbObj = new DatabaseWrite();
	            	dbObj.postResultErrorHandler(obj,errorStatus,"No Error Message received", data);
	    		}

			}
             
         }
		else
		{
			DatabaseWrite dbObj = new DatabaseWrite();
        	dbObj.postResultErrorHandler(obj,"-1","No Message from server", data);
		}
		 

		// Get the response
		/*
		StringBuffer answer = new StringBuffer();
		BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		String line;
		while ((line = reader.readLine()) != null) {
			answer.append(line);
		}
		writer.close();
		reader.close();

		//Output the response
		System.out.println(answer.toString());*/
		

	}


		/*public static void main(String[] args) throws Exception {

    	//new PostResult().postResult2API();
    }*/

	
}