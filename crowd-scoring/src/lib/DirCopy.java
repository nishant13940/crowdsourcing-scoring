package lib;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import svar.Property;


public class DirCopy {
	Property property = new Property();
//public static int copyDir (String CandidateID)
public int copyDir (String CandidateID)
	{
	int index;
	
	String sourceAdd = property.zippedFilesSource;
	String sourceAdd1 = property.zippedFilesSource1;
	String destAdd = property.noiseReducedFilesLocation;
	File path = new File(sourceAdd + CandidateID);
	File path1 = new File(sourceAdd1 + CandidateID);
	File dest = new File(destAdd + CandidateID);
	//System.out.println("dest Add = " + dest);
	File[] files;
	String fileAdd = new String();
	String fileName = new String();
	files = path.listFiles();
	if(files== null)
	{
		files = path1.listFiles();
	}
	dest.mkdir();
	if(files!= null){
	for (int i = 0, n = files.length; i < n; i++)
		{fileAdd = files[i].toString();
		index = fileAdd.lastIndexOf("/");
		fileName = fileAdd.substring(index+1);
	//System.out.println("filename "+fileAdd + " " + index);
	//System.out.println(dest + "/" + fileName);
		try {
		      FileCopy.copy(fileAdd, dest + "/" + fileName);
		    } catch (IOException e) {
		      System.err.println(e.getMessage());
		    }
		}
	return 1;
	}
	else{
		System.out.println("Directory to be copied not found");
		return 0;
		}
	}

/*public static void main(String[] args)
{	
	//String Candidate = "10010247002024";
	String Candidate = args[0];
	DirCopy.copyDir(Candidate);
}
*/
}
