package lib;

import svar.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Random;

import javax.print.DocFlavor.STRING;

import org.apache.commons.lang3.StringUtils;

public class DictOperations {
	Property property = new Property();
	
	
	public boolean createDict(Candidate candidateObj, CrowdInput crowdInput){
		int freeSpeechCategory = 601;
		
		int itr;
		boolean isSuccessful=true;
		String toPrint="",curLine;
		Random random = new Random();
		property.moduleVariables(candidateObj.currentModule, candidateObj.deliverModuleVersion);
		String phoneList[] = {"ah","ey","b","r","iy","v","t","ay","d","z","ih","l","aa","sh","ae","er","jh","n","aw","p","s","uw","ao","k","eh","m","ng","y","ch","w","hh","f","ow","g","dh","oy","th","uh","zh","sil","sp"};
		
		String dictLocation = property.dictLocation + candidateObj.candidateID;
		String moduleSentPath = property.commonLocation + "Module_" + candidateObj.currentModule + "/modelFiles/SentWords/" + candidateObj.categoryVersusQuestionID.get(freeSpeechCategory).get(0).toString() + ".txt";
		String module100Path = property.commonLocation + "Module_" + candidateObj.currentModule + "/modelFiles/100Words/" + candidateObj.categoryVersusQuestionID.get(freeSpeechCategory).get(0).toString() + ".txt";
		String dictPath = property.dictLocation + candidateObj.candidateID + "/" + candidateObj.categoryVersusQuestionID.get(freeSpeechCategory).get(0).toString();
		String dictData[] = crowdInput.Transcription.toLowerCase().split(" ");
		String dictString = dictData[0]; 
		for(itr=1;itr<dictData.length;itr++){
			if(!dictData[itr].trim().equals("")){
				dictData[itr]=dictData[itr].replace("'","");
				dictString = dictString + "\n" + dictData[itr];
			}
		}
		dictString += "\n";
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(module100Path));
			while ((curLine = br.readLine()) != null) {
				dictString +=curLine+"\n";
			}
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			br = new BufferedReader(new FileReader(moduleSentPath));
			while ((curLine = br.readLine()) != null) {
				dictString +=curLine+"\n";
			}
		}
		catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		File dictFolder = new File(dictLocation);
		File dictFile = new File(dictPath);
		
		dictFolder.mkdir();
		if (!dictFile.exists()) {
			try {
				dictFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		byte data[] = dictString.getBytes();
		try {
			FileOutputStream out = new FileOutputStream(dictFile+"_0");
			out.write(data, 0, data.length);
			out.flush();
			out.close();
			//System.out.println("perl "+ property.scriptsLocation +"MissingWordsCleanup.pl "+ dictPath + "_0");
			Process process = Runtime.getRuntime().exec("perl "+ property.scriptsLocation +"MissingWordsCleanup.pl "+ dictPath+"_0");
			process.waitFor();
			
			br = new BufferedReader(new FileReader(dictPath+"_0.txt"));
			while ((curLine = br.readLine()) != null) {
				toPrint +=curLine.toLowerCase()+"\n";
			}
			br = new BufferedReader(new FileReader(property.commonLocation+"commonDict"));
			while ((curLine = br.readLine()) != null) {
				toPrint +=curLine.toLowerCase()+"\n";
			}
			String[] toPrintArr = toPrint.split("\n");

			for(itr=0;itr<toPrintArr.length;itr++){
				String[] arr = toPrintArr[itr].split("    ");
				arr = updateFromDict(arr);
				String PhoneArr[] =(arr[2].split(" "));
				for(int i=0;i<PhoneArr.length;i++){
					if(!Arrays.asList(phoneList).contains(PhoneArr[i])){
						PhoneArr[i]=phoneList[random.nextInt(phoneList.length - 1)];
					}
				}
				arr[2]=StringUtils.join(PhoneArr," ");
				toPrintArr[itr]= new String(StringUtils.join(arr,"    "));
				
			}
			toPrint= new String(StringUtils.join(toPrintArr,"\n"))+"\n";
			data = toPrint.getBytes();
			/*
			toPrint = toPrint.replace(" ax "," aa ");
			toPrint = toPrint.replace(" ax\n"," aa\n");
			toPrint = toPrint.replace(" to "," t uw ");
			toPrint = toPrint.replace(" to\n"," t uw\n");
			toPrint = toPrint.replace(" dt"," ");
			toPrint = toPrint.replace(" dt\n"," \n");
			*/
			out = new FileOutputStream(dictPath+"_0"+".txt");
			out.write(data, 0, data.length);
			out.flush();
			out.close();
			
			for(itr=1;itr < property.numberAntiModels+1; itr++){
				System.out.println("creating Antimodel"+itr);
				isSuccessful = isSuccessful & createAntiModel(dictPath, itr);
			}
		} catch (IOException x) {
			x.printStackTrace();
		    System.err.println(x);
		    return false;
		} catch (InterruptedException e) {
			e.printStackTrace();
			return false;
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return isSuccessful;
	}
	
	boolean createAntiModel(String dictPath, int itr){
		int i;
		Random random = new Random();
		String phoneList[] = {"ah","ey","b","r","iy","v","t","ay","d","z","ih","l","aa","sh","ae","er","jh","n","aw","p","s","uw","ao","k","eh","m","ng","y","ch","w","hh","f","ow","g","dh","oy","th","uh","zh","sil"};
		try {
			String curLine=null;
			FileOutputStream out = new FileOutputStream(dictPath+"_"+itr+".txt");
			BufferedReader br = new BufferedReader(new FileReader(dictPath+"_0.txt"));
			DatabaseSelect dbsel = new DatabaseSelect();
			while ((curLine = br.readLine()) != null) {
				String[] arr = curLine.split("    ");
				arr[2] = dbsel.selectDb(arr[0],itr);
				byte data[] = new String(StringUtils.join(arr,"    ")+"\n").getBytes();
				out.write(data, 0, data.length);
			}
			out.flush();
			out.close();
			return true;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
	}
	
	String[] updateFromDict(String[] line) throws ClassNotFoundException, SQLException{
		String word = line[0];
		String forDict="";
		Random random =new Random();
		DatabaseSelect dbsel= new DatabaseSelect();
		String phoneList[] = {"ah","ey","b","r","iy","v","t","ay","d","z","ih","l","aa","sh","ae","er","jh","n","aw","p","s","uw","ao","k","eh","m","ng","y","ch","w","hh","f","ow","g","dh","oy","th","uh","zh","sil","sp"};
		String phoneme = dbsel.selectDb(word, 0);
		if(phoneme.equals("")){
			String antiModel[]={word,"","","","","",""};
			String[] phoneArr = line[2].split(" ");
			for(int i=0;i<phoneArr.length;i++){
				forDict+=phoneArr[i]+" ";
				if(!phoneArr[i].equals("sp")){
					if(!Arrays.asList(phoneList).contains(phoneArr[i])){
						if(!phoneArr[i].equals("ax")){
							antiModel[1]+=phoneList[random.nextInt(phoneList.length - 1)]+" ";
						}
					}
					else{
						antiModel[1]+=phoneArr[i]+" ";
					}
					antiModel[2]+=phoneList[random.nextInt(phoneList.length - 1)]+" ";
					antiModel[3]+=phoneList[random.nextInt(phoneList.length - 1)]+" ";
					antiModel[4]+=phoneList[random.nextInt(phoneList.length - 1)]+" ";
					antiModel[5]+=phoneList[random.nextInt(phoneList.length - 1)]+" ";
					antiModel[6]+=phoneList[random.nextInt(phoneList.length - 1)]+" ";
				}
				else{
					antiModel[1]+=phoneArr[i];
					antiModel[2]+=phoneArr[i];
					antiModel[3]+=phoneArr[i];
					antiModel[4]+=phoneArr[i];
					antiModel[5]+=phoneArr[i];
					antiModel[6]+=phoneArr[i];
				}
			}
			if(dbsel.insertDB(antiModel)){
				line[2] = antiModel[1];
				return line;
			}
			else{
				return line;
			}
			
		}
		else{
			line[2]=phoneme;
			return line;
		}
	}
}
