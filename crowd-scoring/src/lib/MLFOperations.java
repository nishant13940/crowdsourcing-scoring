package lib;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.FileOutputStream;
import java.util.Arrays;
import java.util.TreeSet;

import org.apache.commons.lang3.StringUtils;

import svar.*;

public class MLFOperations {
	Property property = new Property();
	
	public boolean createMLF(Candidate candidateObj, CrowdInput crowdInput){
		int freeSpeechCategory = 601;
		int itr;
		String mlfLocation = property.mlfLocation + candidateObj.candidateID;
		String mlfPath = property.mlfLocation + candidateObj.candidateID + "/" + candidateObj.categoryVersusQuestionID.get(freeSpeechCategory).get(0).toString() + ".mlf";
		String mlfData[] = crowdInput.Transcription.split(" ");
		String mlfString = "#!MLF!#\n\"*/"+ candidateObj.categoryVersusQuestionID.get(freeSpeechCategory).get(0).toString() +"-tblQuan_"+ candidateObj.currentModule + "_1.lab\"\nsent-start";
		for(itr=0;itr<mlfData.length;itr++){
			mlfString = mlfString + "\n" +mlfData[itr];
		}
		mlfString = mlfString + "\nsent-end\n.";
		
		File mlfFolder = new File(mlfLocation);
		File mlfFile = new File(mlfPath);
		
		mlfFolder.mkdir();
		if (!mlfFile.exists()) {
			try {
				mlfFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		byte data[] = mlfString.getBytes();
		try {
			FileOutputStream out = new FileOutputStream(mlfFile);
			out.write(data, 0, data.length);
			out.flush();
			out.close();	
		} catch (IOException x) {
		    System.err.println(x);
		    return false;
		}
		return createFreeLoopModelFiles(candidateObj) && createSentWordModelFiles(candidateObj);
	}
	
	boolean createFreeLoopModelFiles(Candidate candidateObj){
		int freeSpeechCategory = 601;
		String toPrint="",curLine;
		try{
			String fLLocation = property.modelFilesLocation +"100Words/"+ candidateObj.candidateID+"/";
			String fLWordPath = fLLocation +candidateObj.categoryVersusQuestionID.get(freeSpeechCategory).get(0).toString() + ".txt";
			String fLPath = fLLocation + candidateObj.categoryVersusQuestionID.get(freeSpeechCategory).get(0).toString();
			String modulePath = property.commonLocation + "Module_" + candidateObj.currentModule + "/modelFiles/100Words/" + candidateObj.categoryVersusQuestionID.get(freeSpeechCategory).get(0).toString() + ".txt";  
			String dictPath = property.dictLocation +candidateObj.candidateID + "/" + candidateObj.categoryVersusQuestionID.get(freeSpeechCategory).get(0).toString() +"_0";
			
			File fLFolder = new File(fLLocation);
			fLFolder.mkdir();
			
			FileOutputStream out = new FileOutputStream(fLWordPath);
			BufferedReader br = new BufferedReader(new FileReader(dictPath));
			while ((curLine = br.readLine()) != null) {
				toPrint = toPrint + curLine + "\n";
			}
			br = new BufferedReader(new FileReader(modulePath));
			while ((curLine = br.readLine()) != null) {
				toPrint = toPrint + curLine + "\n";
			}
			TreeSet<String> unique = new TreeSet<String>(Arrays.asList(toPrint.split("\n")));
			toPrint = StringUtils.join(unique.toArray(new String[unique.size()]),"\n");
			byte data[] = toPrint.getBytes();
			out.write(data, 0, data.length);
			out.flush();
			out.close();
			
			Process process = Runtime.getRuntime().exec("HBuild -t sent-start sent-end "+fLWordPath+ " " + fLPath);
			process.waitFor();
			return true;
		}
		catch(Exception e){
			System.err.println(e);
		    return false;
		}
	}
	
	boolean createSentWordModelFiles(Candidate candidateObj){
		int freeSpeechCategory = 601;
		String toPrint="",curLine;
		try{
			String fLLocation = property.modelFilesLocation +"sentWords/"+ candidateObj.candidateID +"/";
			String fLWordPath = fLLocation + candidateObj.categoryVersusQuestionID.get(freeSpeechCategory).get(0).toString() + ".txt";
			String fLPath = 	fLLocation + candidateObj.categoryVersusQuestionID.get(freeSpeechCategory).get(0).toString();
			String modulePath = property.commonLocation + "Module_" + candidateObj.currentModule + "/modelFiles/SentWords/" + candidateObj.categoryVersusQuestionID.get(freeSpeechCategory).get(0).toString() + ".txt";  
			String dictPath = property.dictLocation +candidateObj.candidateID + "/" + candidateObj.categoryVersusQuestionID.get(freeSpeechCategory).get(0).toString() +"_0";
			
			File fLFolder = new File(fLLocation);
			fLFolder.mkdir();
			
			FileOutputStream out = new FileOutputStream(fLWordPath);
			BufferedReader br = new BufferedReader(new FileReader(dictPath));
			while ((curLine = br.readLine()) != null) {
				toPrint = toPrint + curLine + "\n";
			}
			br = new BufferedReader(new FileReader(modulePath));
			while ((curLine = br.readLine()) != null) {
				toPrint = toPrint + curLine + "\n";
			}
			
			TreeSet<String> unique = new TreeSet<String>(Arrays.asList(toPrint.split("\n")));
			toPrint = StringUtils.join(unique.toArray(new String[unique.size()]),"\n");
			
			byte data[] = toPrint.getBytes();
			out.write(data, 0, data.length);
			out.flush();
			out.close();
			
			System.out.println("HBuild -t sent-start sent-end "+fLWordPath+ " " + fLPath);
			Process process = Runtime.getRuntime().exec("HBuild -t sent-start sent-end "+fLWordPath+ " " + fLPath);
			process.waitFor();
			return true;
		}
		catch(Exception e){
			System.err.println(e);
		    return false;
		}
	}
}