package lib;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import svar.*;

public class NLPFeatures {
	
	public HashMap<String, Object> calcNLPFeatures(String text){
		HashMap<String, Object> nlpFeatures= new HashMap<String, Object>();
		
		int nlp_numWords         		= numberOfWords(text);
		int nlp_10000CommonWords 		= numberOfCommonWords(text,"10000");
		int nlp_1000CommonWords  		= numberOfCommonWords(text,"1000");
		int nlp_stutterOne       		= stutterByOneWord(text);
		int nlp_stutterTwo       		= stutterByTwoWords(text);
		int nlp_longestWord				= longestWord(text);
		int numberOfWordsMore			= 0;
		int numberOfWordsLess			= 0;
		float numberOfWordsMoreNorm		= 0;
		float numberOfWordsLessNorm		= 0;
		int numUniqueWords				= uniqueWords(text);
		
		nlpFeatures.put("nlp_numWords",nlp_numWords);
		
		nlpFeatures.put("nlp_1000CommonWords",nlp_1000CommonWords);
		nlpFeatures.put("nlp_1000CommonWordsNorm",(float)nlp_1000CommonWords/nlp_numWords);
		nlpFeatures.put("nlp_10000CommonWords",nlp_10000CommonWords);
		nlpFeatures.put("nlp_10000CommonWordsNorm",(float)nlp_10000CommonWords/nlp_numWords);
		
		nlpFeatures.put("nlp_1000UncommonWords",nlp_numWords - nlp_1000CommonWords);
		nlpFeatures.put("nlp_1000UncommonWordsNorm",1 - (float)nlp_1000CommonWords/nlp_numWords);
		nlpFeatures.put("nlp_10000UnommonWords",nlp_numWords - nlp_10000CommonWords);
		nlpFeatures.put("nlp_10000UnommonWordsNorm",1 - (float)nlp_10000CommonWords/nlp_numWords);
		
		nlpFeatures.put("nlp_stutterOne",nlp_stutterOne);
		nlpFeatures.put("nlp_stutterOneNorm",(float)nlp_stutterOne/nlp_numWords);
		nlpFeatures.put("nlp_stutterTwo",nlp_stutterTwo);
		nlpFeatures.put("nlp_stutterTwoNorm",(float)nlp_stutterTwo/nlp_numWords);
		
		nlpFeatures.put("nlp_longestWord",nlp_longestWord);
		nlpFeatures.put("nlp_longestWordNorm",nlp_longestWord/nlp_numWords);
		
		for(int i=1; i<11; i++){
			numberOfWordsMore    	= numberOfWordsMoreThanX(text, i);
			numberOfWordsMoreNorm 	= (float)numberOfWordsMore/nlp_numWords;
			numberOfWordsLess    	= nlp_numWords - numberOfWordsMore;
			numberOfWordsLessNorm 	= 1 - (float)numberOfWordsMore/nlp_numWords;
			
			nlpFeatures.put("nlp_numberOfWordsMoreThan_"+i,numberOfWordsMore);
			nlpFeatures.put("nlp_numberOfWordsMoreThan_"+i+"_Norm",numberOfWordsMoreNorm);
			nlpFeatures.put("nlp_numberOfWordsLessThan_"+i,numberOfWordsLess);
			nlpFeatures.put("nlp_numberOfWordsLessThan_"+i+"_Norm",numberOfWordsLessNorm);
			
		}
		
		nlpFeatures.put("nlp_numUniqueWords",numUniqueWords);
		return nlpFeatures;
	}
	
	int numberOfWords(String text){
		return text.split(" ").length;
	}
	
	int numberOfCommonWords(String text, String number){
		try{
			DatabaseSelect db_sel = new DatabaseSelect();
			String transArr[];
			transArr = text.split(" ");
			for(int i=0;i<transArr.length;i++){
				transArr[i] = "'"+transArr[i]+"'"; 
			}
			text = StringUtils.join(transArr,",");
			return db_sel.askDBForNumberOfCommonWords(text,number);
		}
		catch(Exception e){
			e.printStackTrace();
			return -1;
		}
	}
	
	int stutterByOneWord(String text){
		String prevWord="";
		int count=0;
		String textArr[]=text.split(" ");
		for(int i=0;i<textArr.length;i++){
			if(textArr[i].equals(prevWord)){
				count+=1;
			}
			prevWord = textArr[i]; 
		}
		return count;
	}
	
	int stutterByTwoWords(String text){
		String prevWord="",prev1Word="",prev2Word="";
		int count = 0;
		String textArr[] = text.split(" ");
		for(int i=0;i<textArr.length;i++){
			if(textArr[i] == prev1Word && prevWord == prev2Word){
				count+=0;
			}
			prev2Word=prev1Word;
			prev1Word=prevWord;
			prevWord=textArr[i];
		}
		return count;
	}
	
	int longestWord(String text){
		int count=0;
		String textArr[] = text.split(" ");
		
		for(int i=0;i<textArr.length;i++){
			if(textArr[i].length() > count){
				count = textArr[i].length();
			}
		}
		return count;
	}
	
	int numberOfWordsMoreThanX(String text, int x){
		int count=0;
		String textArr[] = text.split(" ");
		for (int i=0;i<textArr.length;i++){
			if(textArr[i].length()>=x){
				count +=1;
			}
		}
		return count;
	}
	
	int uniqueWords(String text){
		Set<String> textSet = new HashSet<String>(Arrays.asList(text.split(" ")));
		return textSet.size();
	}
	
}