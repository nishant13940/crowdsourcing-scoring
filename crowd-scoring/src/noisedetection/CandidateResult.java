package noisedetection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.io.*;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.sound.sampled.UnsupportedAudioFileException;

import svar.Property;

public class CandidateResult {


	HashMap<Integer, Integer> noisyCandCount = new HashMap<Integer, Integer>();	
	HashMap<Integer, ArrayList<ArrayList<Double>>> noiseCandFeature = new HashMap<Integer, ArrayList<ArrayList<Double>>>();
	
	public HashMap<Integer, ArrayList<ArrayList<Double>>> getNoiseCandFeature() {
		return noiseCandFeature;
	}

	public void setNoiseCandFeature(
			HashMap<Integer, ArrayList<ArrayList<Double>>> noiseCandFeature) {
		this.noiseCandFeature = noiseCandFeature;
	}

	public CandidateResult(String file,HashMap<Integer,ArrayList<Integer>> candInfo,String moduleId,PropertyNoise propertyNoise) {
		for(Map.Entry<Integer, ArrayList<Integer>> entry:candInfo.entrySet())
		{
			ArrayList<Integer > arrayList = entry.getValue();
			ArrayList<ArrayList<Double>> feature = new ArrayList<ArrayList<Double>>();
			
			int sum = 0;
			for(int i=0;i<arrayList.size();i++)	
			{
					try{
					 double [] signal = commonFunctions.returnArray(new File(file+'/'+arrayList.get(i)+"-tblQuan_"+moduleId+"_1.wav"));
					 sum+= UtteranceResult.getResult(signal,propertyNoise);
					 feature.add(UtteranceResult.getFeatures(signal,propertyNoise));
//					 Feature.getFeatures();
					}
					catch(IOException e)
					{
						// No Such Wav File Exists, hence anaylsis cannot be done
					} catch (UnsupportedAudioFileException e) {
						// No Such Wav File Exists, hence anaylsis cannot be done
										
					}
			}
			noisyCandCount.put(entry.getKey(),sum);
			noiseCandFeature.put(entry.getKey(),feature);
		}
	}
	

	public boolean getLiberal(PropertyNoise propertyNoise, Property property)
	{
		String liberal = propertyNoise.liberalFilter;
		String liberaleval = "";
		boolean flag = false;
		String val = "";
		for(int i=0;i<liberal.length();i++)
		{
			if(flag==true && liberal.charAt(i)=='$')
			{
				if(val.equals("RS"))
					liberaleval+= noisyCandCount.get(property.RSCategory);
				else if(val.equals("LR"))
					liberaleval+= noisyCandCount.get(property.LRCategory);
				flag = false;
			}
			else if(flag==true)
			{
				val+= liberal.charAt(i);
			}
			else if(liberal.charAt(i)=='$')
			{
				flag = true;
			}
			else
			{
				liberaleval+= liberal.charAt(i);
				val = "";
			}	
		}
		
		ScriptEngineManager manager = new ScriptEngineManager();
		ScriptEngine engine = manager.getEngineByName("js");
		boolean result = false;
		try {
			result = (Boolean) engine.eval(liberaleval);
		} catch (ScriptException e) {
			e.printStackTrace();
		}
		
		// Calling getResult here to get Liberal Result.
		return result;
	}	
	
	public boolean getConservative(PropertyNoise propertyNoise, Property property)
	{
		// Calling getResult here to get Conservative Result for 3,2
		String conservative = propertyNoise.conservativeFilter;
		String conservativeeval = "";
		boolean flag = false;
		String val = "";
		
		for(int i=0;i<conservative.length();i++)
		{
			if(flag==true && conservative.charAt(i)=='$')
			{
				if(val.equals("RS"))
					conservativeeval+= noisyCandCount.get(property.RSCategory);
				else if(val.equals("LR"))
					conservativeeval+= noisyCandCount.get(property.LRCategory);
				flag = false;
			}
			else if(flag==true)
			{
				val+= conservative.charAt(i);
			}
			else if(conservative.charAt(i)=='$')
			{
				flag = true;
			}
			else
			{
				conservativeeval+= conservative.charAt(i);
				val = "";
			}	
		}
		
		ScriptEngineManager scriptEngineManager = new ScriptEngineManager();
		ScriptEngine engine = scriptEngineManager.getEngineByName("js");
		boolean result = false;
		try {
			result = (Boolean) engine.eval(conservativeeval);
		} catch (ScriptException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}


}
