package noisedetection;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.*;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.*;

public class commonFunctions {
	
	public static double[] readWav(File file,int nbFrames) throws UnsupportedAudioFileException, IOException{
		
		 int BUFFER_LENGTH = nbFrames; 
		 AudioInputStream inputAIS=null;
	     AudioFormat audioFormat = null;
	     inputAIS = AudioSystem.getAudioInputStream(file);
	     audioFormat = inputAIS.getFormat();
	     ByteArrayOutputStream baos = new ByteArrayOutputStream();

	     // Read the audio data into a memory buffer.
	     int nBufferSize = BUFFER_LENGTH * audioFormat.getFrameSize();


	        byte[] abBuffer = new byte[nBufferSize];
	        int[] firstSample = new int[nbFrames];
	        double[] x = new double[nbFrames];
	        try{
	    	 while (true) {

		        	int nBytesRead = inputAIS.read(abBuffer);
		            if (nBytesRead == -1) {
		                break;
		            }
		            baos.write(abBuffer, 0, nBytesRead);
		        }
	        }
	        catch(IOException e2){
	        	double[] d={-2.0,-2.0};
		    	 return d;
	        }
		        byte[] abAudioData = baos.toByteArray();
		        
		        
		        for(int i=0;i<nbFrames;i++){
		        	
			        ByteBuffer bb = ByteBuffer.wrap(abAudioData);
			        bb.order(ByteOrder.LITTLE_ENDIAN);
			        ShortBuffer sb = bb.asShortBuffer();
			        
			        firstSample[i] = sb.get(i);
			         
		        //firstSample[i] = (abAudioData[0]&0xFF) | (abAudioData[1]<<8);
		        x[i] = (double)firstSample[i]/(int) Math.pow(2,15);		        

	     }
	     return x;
	    }
	
	public static double[] returnArray(File file) throws UnsupportedAudioFileException, IOException
	{
		  AudioSampleReader sampleReader = new AudioSampleReader(file);
		  long nbSamples = sampleReader.getSampleCount(); 
		  double[] samples = new double[(int)nbSamples]; 
		  samples = commonFunctions.readWav(file, (int) nbSamples);		
		  return samples;
	}
	 
    public static double standardDeviationCalculate ( double[] data )
	{
		final int n = data.length;
		if ( n < 2 )
		{
			return Double.NaN;
		}
		double avg = data[0];
		double sum = 0;
		for ( int i = 1; i < data.length; i++ )
		{
			double newavg = avg + ( data[i] - avg ) / ( i + 1 );
			sum += ( data[i] - avg ) * ( data [i] -newavg ) ;
			avg = newavg;
		}
	// Change to ( n - 1 ) to n if you have complete data instead of a sample.
		return Math.sqrt( sum / ( n ) );
	}
    

}
	