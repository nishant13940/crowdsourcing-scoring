package noisedetection;

import edu.emory.mathcs.jtransforms.fft.DoubleFFT_1D;

public class FeatureClass {
		
	double power;
	double std_fft;
	double spectral_entropy;

	public double getPower(double[] signal)
	{
		double power = 0;
		for(int i=0;i<signal.length;i++)
		{
			power+= Math.pow(signal[i], 2);
		}
		power = power/signal.length;
		return Math.log(power);
	}
	
	
	public double[] getFFT(double []signal)
	{
		DoubleFFT_1D fft = new DoubleFFT_1D(signal.length);
		double[] y2 = new double[signal.length*2];
		
		for(int i=0;i<signal.length;i++)
			y2[i]=signal[i];

		fft.realForwardFull(y2);		

		double abs_y[] = new double[y2.length/2];
		for(int i=0;i<y2.length/2;i++)
		{
			abs_y[i] = Math.sqrt(Math.pow(y2[2*i],2) + Math.pow(y2[2*i+1], 2));
		}
		return abs_y;

	}
	
	public  double getSpectralEntropy(double[] signal)
	{		
		double fft_required[] = getFFT(signal);
		double pdf[] = new double[400];
		int const_fft = 400;  
				
		for(int i=0;i<400;i++){
			if(i<25)
				pdf[i]=0;
			else
			pdf[i] = fft_required[i]/const_fft;
		}		
		double spectral_entropy = 0;
		for(int i=0;i<pdf.length;i++)
		{
		    if(pdf[i]!=0)
		    spectral_entropy+= pdf[i]*Math.log(pdf[i]);
		}				
		return spectral_entropy;
	}
	

	public double getstdFFT(double []signal)
	{
		DoubleFFT_1D fft = new DoubleFFT_1D(signal.length);

		double[] y2 = new double[signal.length*2];
		
		for(int i=0;i<signal.length;i++)
			y2[i]=signal[i];

		fft.realForwardFull(y2);
		

		double real_y[] = new double[y2.length/2];
		double imag_y[] = new double[y2.length/2];
		
		for(int i=0;i<y2.length/2;i++)
		{
			real_y[i] = y2[2*i];
			imag_y[i] = y2[2*i+1];
		}
		
		double std_fft = Math.sqrt(Math.pow(commonFunctions.standardDeviationCalculate(real_y),2) + Math.pow(commonFunctions.standardDeviationCalculate(imag_y),2));
		
		return  std_fft;
	}
		
	
}
