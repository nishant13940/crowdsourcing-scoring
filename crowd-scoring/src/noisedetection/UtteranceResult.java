package noisedetection;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.concurrent.Callable;

import javax.sound.sampled.UnsupportedAudioFileException;

import libsvm.svm;
import libsvm.svm_model;
import libsvm.svm_node;

public class UtteranceResult {

	public static int getResult(double[] signal,PropertyNoise propertyNoise) throws UnsupportedAudioFileException, IOException{

		FeatureClass featureClass = new FeatureClass();

		double fft_ex[] = new double[800];
		
		if(signal.length<propertyNoise.endTime*8)
			return 0;
		
		for(int i=propertyNoise.startTime*8;i<propertyNoise.endTime*8;i++)
			fft_ex[i-propertyNoise.startTime*8] = signal[i];


		svm_model LoadModel = svm.svm_load_model(propertyNoise.svmModel);				
		svm_node[] x = new svm_node[]{new_svm_node(1,featureClass.getPower(fft_ex)),new_svm_node(2,featureClass.getstdFFT(fft_ex)),new_svm_node(3, featureClass.getSpectralEntropy(fft_ex))};

		// Dummy value needed for SVM class, Bad design of class, bad Libsvm library design 
		int predictedLabel = (int) svm.svm_predict_values(LoadModel,x,new double[1]);

		
		return predictedLabel;
	}


	// Static - Fake constructor of svm_node
	public static svm_node new_svm_node(int i,double v)
	{
		svm_node x= new svm_node();
		x.index = i;
		x.value = v;

		return x;
	}


	public static ArrayList<Double> getFeatures(double[] signal,
			PropertyNoise propertyNoise) {
		// TODO Auto-generated method stub
		FeatureClass featureClass = new FeatureClass();
		ArrayList<Double> utterance = new ArrayList<Double>();
		double fft_ex[] = new double[800];
		
		if(signal.length<propertyNoise.endTime*8)
			return utterance;
		
		for(int i=propertyNoise.startTime*8;i<propertyNoise.endTime*8;i++)
			fft_ex[i-propertyNoise.startTime*8] = signal[i];

		utterance.add(featureClass.getPower(fft_ex));
		return utterance;
	}



}
