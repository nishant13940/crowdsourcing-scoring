package noisedetection;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import svar.Property;

import javax.sound.sampled.UnsupportedAudioFileException;

public class NoiseDetectionResult {

	// We should re-achitecture it, should apply singleton pattern 
	PropertyNoise propertyNoise;
	Property property;
	public NoiseDetectionResult(String path, Property property1) {
		propertyNoise = new PropertyNoise(path);
		property = property1;
	}
	
	public String getResult(String file,HashMap<Integer,ArrayList<Integer>> info, String moduleId) throws UnsupportedAudioFileException, IOException
	{
		CandidateResult candidateResult = new CandidateResult(file, info,moduleId,propertyNoise);
		if(candidateResult.getLiberal(propertyNoise, property) && candidateResult.getConservative(propertyNoise, property))
		return "Noisy";
		else if(candidateResult.getConservative(propertyNoise, property) || candidateResult.getLiberal(propertyNoise, property))
		return "Maybe Noisy";
		else
		return "Noiseless";
	}
		
	

}
