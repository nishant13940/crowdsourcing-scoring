package svarUpdated;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;

public class HTKOperations {

		
	public double[][] readHTK(File file) throws IOException
	{
		
		RandomAccessFile randomAccessFile = new RandomAccessFile(file,"r");
		
		Integer nf =  randomAccessFile.readInt();
		Double fp = (Double)(randomAccessFile.readInt()*1.E-7);		
		Short by =  (short) randomAccessFile.readUnsignedShort();
		Short tc = (short) randomAccessFile.readUnsignedShort();
		 
		 if(tc<0)
			 tc=(short) (tc + 65536);
	
		 String cc = "ENDACZK0VT";		 
		 int nhb = cc.length();		 
		 int ndt = 6;
		 
		 int[] hb = new int[nhb+1];
		 int[] hd = new int[nhb+1];
		 
		 int k = 0;
		 for(int i=-(ndt+nhb);i<=-ndt;i++)			 
		 {
			 hb[k] = (int) Math.floor(tc*Math.pow(2,i));
			 k++;
		 }
		 		 
		 k=0;
		 for(int i=nhb;i>=1;i--)
		 {
			 hd[k] = hb[i] - 2*hb[i-1];
			 k++;
		 }
		 

		 long dt=(long) (tc-hb[hb.length-1]*Math.pow(2,ndt));          //  % low six bits of tc represent data type
		 
		 double[][] d = new double[nf][by/4]; 
         for(int i=0;i<nf;i++)
         {
        	 for(int j=0;j<by/4;j++)
        	 {
        		 d[i][j] = randomAccessFile.readFloat();
        	 }
         }	 			 	
		 
		 randomAccessFile.close();
		 
		 return d;
	}
	
	
	public void writeHTK(File file,double[][] features,double sampPeriod,short parmKind) throws IOException
	{
		int nSamples = features.length;
		short sampSize = (short) features[0].length;
	
	    FileOutputStream fos = new FileOutputStream(file);    
		DataOutputStream dw = new DataOutputStream(fos);
		
		dw.writeInt(nSamples);
		dw.writeInt((int) (sampPeriod*1E7));
		dw.writeShort(4*sampSize);		
		dw.writeShort(parmKind);
		
		//ps.write(features[i][j]);
		
		for(int i=0;i<features.length;i++)
		{
			for(int j=0;j<features[0].length;j++)
			{
				dw.writeFloat((float) features[i][j]);
			}
		}
		dw.close();
//		RandomAccessFile randomAccessFile = new RandomAccessFile(file,"r");
		
	}
}
