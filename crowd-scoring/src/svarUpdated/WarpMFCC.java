package svarUpdated;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import svar.Candidate;
import svar.Property;

public class WarpMFCC {

	static ArrayList<String> constList = new ArrayList<String>();
	
	public static void warpMFCC(String mfccPath,String recPath,String candID,HashMap<Integer, ArrayList<Integer>> map,String moduleID, String deliverModuleVersion)
	{
		svar.Property property = new Property();
		property.moduleVariables(moduleID, deliverModuleVersion);
		try {
			buildConstList(property.HTKConstantPath+"vowelList.csv");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		ArrayList<Integer> utterance = new ArrayList<Integer>(map.get(property.FSCategory));


		for(int i=0;i<utterance.size();i++)
		{
			String currRec = recPath+candID+"/"+Integer.toString(utterance.get(i))+"align0"+".mlf";			
			normalizeVowel(mfccPath,candID,utterance.get(i)+"-tblQuan_"+moduleID+"_1.mfcc",currRec);
		}
		
				
	}
	

	public static void normalizeVowel(String mfccPath,String candID,String utteranceID,String recFile)
	{
		
		String htkPath = mfccPath+candID+"/"+utteranceID;
		
		try{
			
			ArrayList<ArrayList<String>> terms = formatString(recFile);
			
			
			// Read MFCC getMFCC(path)
			HTKOperations htkOperations = new HTKOperations();
			
			double mfcc[][] = htkOperations.readHTK(new File(htkPath));
			double[][] locn = new double[terms.size()][2];
			
			for(int i=0,k=0;i<terms.size();i++)
			{
				if(constList.contains(terms.get(i).get(2)))
				{
						locn[k][0] = Double.parseDouble(terms.get(i).get(0))/100000;
						locn[k][1] = Double.parseDouble(terms.get(i).get(1))/100000;
						k++;
				}
			}
		
			
			ArrayList<ArrayList<Double>> finalNorm = new ArrayList<ArrayList<Double>>();
			
			ArrayList<Double> res = new ArrayList<Double>();
			for(int i=0;i<locn.length && locn[i][0]!=0;i++)
			{
				
				for(double j=0;j<mfcc[0].length;j++)
				{
					res.add(mean(mfcc,locn[i][0]-3,locn[i][1]+3,j));
				}
				finalNorm.add(new ArrayList<Double>(res));
				res.clear();
			}
			
			ArrayList<Double> meanVmfcc = new ArrayList<Double>();
			double temp = 0;
			
			ArrayList<Double> meanNorm = new ArrayList<Double>();
			
			for(int j=0;j<finalNorm.get(0).size();j++)
			{
				
				double tempval = 0;
				for(int i=0;i<finalNorm.size();i++)
				{					
					tempval+= finalNorm.get(i).get(j);
				}
				meanNorm.add(tempval/finalNorm.size()); // Should be 1*39
			}
						

			double[][] finalMfcc = new double[mfcc.length][mfcc[0].length];
			
			for(int i=0;i<mfcc.length;i++)
			{
				for(int j=0;j<mfcc[0].length;j++)
				{
					finalMfcc[i][j] = mfcc[i][j] - meanNorm.get(j);
				}
			}
//			process = Runtime.getRuntime().exec("mkdir -p "+property.RECLocation+org+"/"+candidateID);
			
			short paramMFCC  = 6+256+512+2048+8192;
			
			Path p1 = Paths.get((mfccPath+candID+"/"+utteranceID)); 
			Files.deleteIfExists(p1);
			htkOperations.writeHTK(new File(mfccPath+candID+"/"+utteranceID),finalMfcc,10E-3,paramMFCC);
			
		}catch (Exception e) {
			//System.out.println("Align File Does Not Exist");
		}
		
		
		
	}
	
	public static double mean(double arr[][],double start,double end,double dim)
	{
		ArrayList<Double> res = new ArrayList<Double>();
		double result;
		
		
		for(double j=start;j<=end;j++)
		{
			res.add(arr[(int)j][(int) dim]);
		}
		
		result = sum(res)/res.size();
		
		return result;		
	}
	
	public static double sum(ArrayList<Double> arr)
	{
		double res = 0;		
		for(int i=0;i<arr.size();i++)
			res+=arr.get(i);
		
		return res;
	}
		
	public static ArrayList<ArrayList<String>> formatString(String file) throws FileNotFoundException
	{
		
		Scanner scanner = new Scanner(new File(file));
		ArrayList<String> terms = new ArrayList<String>();
		ArrayList<ArrayList<String>> result = new ArrayList<ArrayList<String>>();
		
		while(scanner.hasNextLine())
		{
			String line = scanner.nextLine();
			String temp[] = line.split(" ");
			
			if(temp.length>=4)
			{
				for(int i=0;i<4;i++)
				{
					terms.add(temp[i]);
				}
				result.add(new ArrayList<String>(terms));
				terms.clear();
			}
			
		}
		return result;		
		
		
	}
	
	public static void buildConstList(String path) throws FileNotFoundException
	{
		Scanner scanner = new Scanner(new File(path));
//		ArrayList<ArrayList<String>> constList = new ArrayList<ArrayList<String>>();
		
		while(scanner.hasNextLine())
		{
			String[] temp = scanner.nextLine().split(",");
			constList.add(temp[0]);		
//			constMap.put(temp[0],temp[1]);			
		}		
	}

}
