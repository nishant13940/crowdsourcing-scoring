package svarUpdated;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import svar.Candidate;
import svar.Property;
import svar.feature_total;
public class SentenceWise {

	public static int runMFCCForceAlign(Property property, HashMap<Integer, ArrayList<Integer>> info,Candidate candObj,String forceAlignFilePath,String moduleID,String outputPath) throws IOException, InterruptedException
	{					
		String candidateID = candObj.candidateID;
		 convertRecToLab(forceAlignFilePath,candidateID,"align"+"0"+".mlf","forceAlign"+"0"+".mlf");

		 // Assuming we have got mfcc
		 for(int i = 0; i<property.numberAntiModels+1; ++i)
			{				
				
				String commandLine = "HVite -m -o N -C "+property.commonLocation+"config -H "+property.commonLocation+
				"hmmdefs -H "+property.commonLocation+"macros -i "+property.RECLocation+outputPath+"/"+candidateID+"/align"+i+".mlf"+
				" -I " +forceAlignFilePath+candidateID+"/forceAlign0"+".mlf "+
				property.dictLocation+candidateID+"/"+candObj.categoryVersusQuestionID.get(601).get(0).toString()+"_"+i+".txt "+
				property.commonLocation+"tiedlist" +" -S "+property.listMFCCLocation+candidateID+"/list.txt";
								
				System.out.println(commandLine);
				Runtime rt = Runtime.getRuntime();
				Process pr = rt.exec(commandLine);
				pr.waitFor();
		//		System.out.println("Here");
		 }
	//	 concatenateFeatures(property.RECLocation+candidateID+"/",property);
		 return 1;
	}
	
	
	private static void convertRecToLab(String folderPath,String candidateID,String fileName,String fileOutput) throws IOException {
	//		System.out.println("eherr");	
		Scanner scanner = new Scanner(new File(folderPath+candidateID+"/"+fileName));
		PrintWriter pw = new PrintWriter(folderPath+candidateID+"/"+fileOutput);
		while(scanner.hasNextLine())
		{
			String temp = scanner.nextLine();			
			pw.println(temp.replaceAll("\\.rec", "\\.lab"));
		}
		pw.close();
	}
	public static int runMFCC(Property property, HashMap<Integer, ArrayList<Integer>> info,String candidateID,String pathModel,String moduleID,String outputPath) throws IOException, InterruptedException
	{
		
		ArrayList<Integer> mfccIDList = new ArrayList<Integer>();
		
		mfccIDList = new ArrayList<Integer>(info.get(property.FSCategory));

		 // Assuming we have got mfcc
		 for(int i = 0; i<1; ++i)
			{
			 for(int mfccID:mfccIDList)
			 {
				
//				For Word Generation after that will do Word Alignment				
				String commandLine = "HVite -o NMST -q Admntl -C "+property.commonLocation+"config -H "+property.commonLocation+
				"hmmdefs -H "+property.commonLocation+"macros -i "+property.RECLocation+outputPath+"/"+candidateID+"/temp"+mfccID+"_"+i+".mlf"+
				" -w " +pathModel+"/"+mfccID  + " " +property.dictLocation+candidateID+"/"+ mfccID+"_"+i+".txt "+property.commonLocation+"tiedlist" +
				" "+property.MFCCLocation+candidateID+"/"+mfccID+"-tblQuan_"+moduleID+"_1"+".mfcc";
								
				
				System.out.println(commandLine);
				Runtime rt = Runtime.getRuntime();
				Process pr = rt.exec(commandLine);
				pr.waitFor();
		//		System.out.println("Here");
			}
		 }
		 concatenateFeatures(property.RECLocation+outputPath+"/"+candidateID+"/",property,"0");
		 return 1;
	}
	// With help from utkarsh

	public static void concatenateFeatures(String path,Property property,final String change) throws IOException
	{
		File root = new File(path);

		FilenameFilter filter = new FilenameFilter(){	
				@Override
				public boolean accept(File dir, String name){
					return name.contains("_"+change+".mlf");
				}
			};
		
			String[] mlf_list=root.list(filter);
			String fileOutput="";
			String filetempPath = path+"temp_"+"align"+change+""+".mlf";
			String fileOutputPath = path+"align"+change+".mlf";
			BufferedWriter wr = new BufferedWriter(new FileWriter(new File(filetempPath)));
			
				for (int i=0;i<mlf_list.length;i++)
				{
					Scanner scanner = new Scanner(new File(path+mlf_list[i]));
					while(scanner.hasNext())
					{
						fileOutput= scanner.nextLine();
						wr.write(fileOutput);
						wr.newLine();
					}
						wr.newLine();
				}
				wr.close();
				cleanMLF(new File(filetempPath),new File(fileOutputPath));
		
		String[] b = new String[] {"/bin/sh", "-c", "rm -r "+path+"t*.mlf"}; 		
		Runtime.getRuntime().exec(b);  //		rf.exec(cmd); // Delete temp files

	}
	
	// Clean this code should be integrated in the above funtion;
	public static void cleanMLF(File in,File out) throws IOException
	{
	    BufferedReader reader = new BufferedReader(new FileReader(in));
	    PrintWriter writer = new PrintWriter(new FileWriter(out));
	    String line = null;
	    
	    // 1st Special condition	    
	    if((line=reader.readLine())!=null)
	    	writer.println("#!MLF!#");
	    
	    
	    while ((line = reader.readLine()) != null)
	    {
	        	
			String output = "";
			String temparr[] = line.split(" ");
			if(temparr.length>=5){
				for(int i=0;i<5;i++)
					output+=temparr[i]+" ";
			}
			else
				output = line;			

	        writer.println(output.replaceAll("#!MLF!#",""));
	    }
	    reader.close();
	    writer.close();
			    
	}
		
}
