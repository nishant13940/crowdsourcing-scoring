package svarUpdated;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

import svar.Property;


public class ParsePhoneFile {
	
	
	public static HashMap<String, String> mapImpWords = new HashMap<String, String>(); 
	

	public ArrayList<ArrayList<String>> getStateFeatures(String inputFolder,String outputFolder,String candID,svar.Property property) throws Exception
	{
				
		ArrayList<ArrayList<String>> result = new ArrayList<ArrayList<String>>();		
		ArrayList<String> utteranceID = makeTempFiles(inputFolder,candID, "align", ".mlf",property);
		mainParser(inputFolder,candID, utteranceID, "align", ".mlf",outputFolder,candID+".txt",property);
		result = FeaturesPhoneFile.getFeatures(new File(outputFolder+candID+".txt"),property);
		
//		FeaturesPhoneFile featuresPhoneFile = new FeaturesPhoneFile();
//		FeaturesPhoneFile.print(result,"/mnt/Disk-2/SVAR/features/state/"+candID+".csv");
		return result;
	}

	public static void getVowelFile(String inputFolder, String candID)
	throws Exception {
		ArrayList<String> utteranceID = makeTempFiles(inputFolder, candID,
		"align", ".mlf");
	}

	
	
	/** Make Utterance file for each Utterance, File will be named as Utterance+const+ext */
	
	public ArrayList<String> makeTempFiles(String folder,String candID,String constName,String ext,svar.Property property) throws IOException
	{		
		ArrayList<String> allUtterance = new ArrayList<String>();
		for (int i=0;i<=property.numberAntiModels;i++)
		{
			Scanner scanner = new Scanner(new File(folder+candID+"/"+constName+i+ext));
			String utteranceID = "";
			boolean writeFlag = false;
			while(scanner.hasNextLine())
			{
				String line = scanner.nextLine();
				if(line.contains(".rec"))
				{
					String[] temp=line.split("/");
					String[] utterance_id=temp[temp.length-1].split("\\.");
					int idx = utterance_id[0].indexOf('-');
					utteranceID= utterance_id[0].substring(0,idx);
					if(i==0)		// As we need Utterance for original align only rest will be same.
					allUtterance.add(utteranceID);
					
					new File(folder+candID+"/"+utteranceID+constName+i+ext).delete();
				}
				else if(line.contains("sent-start"))
					writeFlag = true;			// Start Writing
				
				if(writeFlag==true)
				{
					FileWriter fw = new FileWriter(new File(folder+candID+"/"+utteranceID+constName+i+ext),true);
					fw.write(line+"\n");
					fw.close();
				}
				
				if(line.contains("sent-end"))
				{
					
					writeFlag=false;	// Stop Writing
				}
			}
		}		
		
		return allUtterance;
	}
	
	public static ArrayList<String> makeTempFiles(String folder, String candID,
			String constName, String ext) throws IOException {
		ArrayList<String> allUtterance = new ArrayList<String>();
		for (int i = 0; i <= 0; i++) {
			Scanner scanner = new Scanner(new File(folder + candID + "/"
					+ constName + i + ext));
			String utteranceID = "";
			boolean writeFlag = false;
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				if (line.contains(".rec")) {
					String[] temp = line.split("/");
					String[] utterance_id = temp[temp.length - 1].split("\\.");
					int idx = utterance_id[0].indexOf('-');
					utteranceID = utterance_id[0].substring(0, idx);
					if (i == 0) // As we need Utterance for original align only
								// rest will be same.
						allUtterance.add(utteranceID);

					new File(folder + candID + "/" + utteranceID + constName
							+ i + ext).delete();
				} else if (line.contains("sent-start"))
					writeFlag = true; // Start Writing

				if (writeFlag == true) {
					FileWriter fw = new FileWriter(new File(folder + candID
							+ "/" + utteranceID + constName + i + ext), true);
					fw.write(line + "\n");
					fw.close();
				}

				if (line.contains("sent-end"))
					writeFlag = false; // Stop Writing
			}
		}

		return allUtterance;
	}

	

	public void mainParser(String folder,String candID,ArrayList<String> utteranceID,String constName,String ext,String outputFolder,String outputName,svar.Property property) throws IOException
	{
		PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(new File(outputFolder+outputName))));

		for (int p=0;p<utteranceID.size();p++)
		{
			pw.println("\nUtteranceStartID:"+utteranceID.get(p));
			ArrayList<ArrayList<ArrayList<String>>> allParsedFile = new ArrayList<ArrayList<ArrayList<String>>>();
			for(int i=0;i<=property.numberAntiModels;i++)
				allParsedFile.add(parseFile(new File(folder+candID+"/"+utteranceID.get(p)+constName+i+".mlf")));
	
			
			String startTime = "";
			String endTime = "";
			boolean flag = false;
			String currPhone = "";
			String currPhoneValue = "";
			boolean flagImpWords = true; 
			
			for (int j=0;j<allParsedFile.get(0).size();j++)
			{
				//				allParsedFile.get(i).get(j).get(0);   // Starting Word For First Sentence and so on.
				
					if(allParsedFile.get(0).get(j).size()>4)
					{
						startTime = allParsedFile.get(0).get(j).get(0); // Start time as below we are checking for +1 pos code would work, hmm should improve it
						currPhone = allParsedFile.get(0).get(j).get(4);
						currPhoneValue = allParsedFile.get(0).get(j).get(5);
						flagImpWords = true;
					}
					if(allParsedFile.get(0).size()>j+1 && allParsedFile.get(0).get(j+1).size()>4)
					{
						endTime = allParsedFile.get(0).get(j).get(1);	// End Time
						flag = true;						// Now Can Search Elements
					}
					if(flag && flagImpWords)		// IF We have got both start and end Position
					{
						ArrayList<String> LL = new ArrayList<String>();
						for (int k=1;k<=property.numberAntiModels;k++)                               // From 2nd List Onwards
						{
							LL.add(getApproximateStateLL(allParsedFile.get(k), startTime,endTime));
						}
			
		//				for(int k=0;k<allParsedFile.get(0).get(j).size();k++)
		//					pw.print(allParsedFile.get(0).get(j).get(k)+" ");
						pw.print(startTime+" "+endTime+" "+currPhone+" "+currPhoneValue+" ");
						for(int k=0;k<LL.size();k++)
							pw.print(LL.get(k)+" ");
						pw.println();
//						System.out.println("Here");
						flag=false;
						flagImpWords = true;
					}
			}
			pw.println("UtteranceEnd");
		}
		pw.close();
//		System.out.println("hold");		
	}
	
	

	public static String getApproximateStateLL(ArrayList<ArrayList<String>> timeList,String startTime,String endTime)
	{
		ArrayList<Double> timeDiff = new ArrayList<Double>();
		double sumLL = 0;
		for(ArrayList<String> arrList:timeList)
			timeDiff.add(Math.abs(Double.parseDouble(arrList.get(0))-Double.parseDouble(startTime))); // Start Pos
		
		int startPos = minPos(timeDiff);
		
		timeDiff.clear();
		for(ArrayList<String> arrList:timeList)
			timeDiff.add(Math.abs(Double.parseDouble(arrList.get(1))-Double.parseDouble(endTime)));
		
		int endPos = minPos(timeDiff);
		
		
		if(startPos>endPos)		// Hack Bcz there may be case where start pos is higher than end-Pos
			endPos=startPos;
		
		for(int i=startPos;i<=endPos;i++)
			sumLL+=Double.parseDouble(timeList.get(i).get(3));
		
		
		return String.valueOf(sumLL/(endPos-startPos+1));		// Returning Mean
	}
	
	public static String getApproximateStateLL(ArrayList<ArrayList<String>> endTimeList,String timeCheck)
	{
		ArrayList<Double> timeDiff = new ArrayList<Double>();
		for(ArrayList<String> arrList:endTimeList)
		{
			timeDiff.add(Math.abs(Double.parseDouble(arrList.get(1))-Double.parseDouble(timeCheck)));
		}
		int pos = minPos(timeDiff);
		return endTimeList.get(pos).get(3);	
	} 

	public static int minPos(ArrayList<Double> arr)
	{
		double min=Double.MAX_VALUE;
		int pos = 0;
		for(int i=0;i<arr.size();i++)
		{
			if(min>arr.get(i))
			{
				min=arr.get(i);
				pos = i;
			}
		}				
		return pos;
	}

	private static ArrayList<ArrayList<String>> parseFile(File file) throws FileNotFoundException {
	
		ArrayList<String> data = new ArrayList<String>(10);
		ArrayList<ArrayList<String>> compData = new ArrayList<ArrayList<String>>();
		java.util.Scanner scanner = new Scanner(file);
		while(scanner.hasNext()){
			String[] temp = scanner.nextLine().split(" ");
			data = new ArrayList<String>( Arrays.asList(temp));
			compData.add(data);
		}
	
		return compData;
	}
	

	public static void buildMapImpWords(String path) throws FileNotFoundException
	{
		Scanner scanner = new Scanner(new File(path));
		
		while(scanner.hasNextLine())
		{
	//		mapImpWords.put(temp[0], temp[1]);
		//	String temp[] = scanner.nextLine().split(",");
		}		
	}
	
	/*
	 * Code to generate features from text files
	 * written by ankit @ 2 Sep
	public static void main(String arg[])
	{
		String outputFolder = "/mnt/Disk-2/SVAR/FeatureDump/state/";
		Property property = new Property();
		String []cids = {"10010602743232","10010742120327","10010742275966","10010742291062","10010742315453","10010742657272","10010742767713","10010743511641","10010743592037","10010743631037","10010743825174","10010743879576","10010745391614","10010752445423","10010758226044","10010760350789","10010760410193","10010769003933","10010772171158","10010778397250","10010791403098","10010793080234","10010795039439","10010807173313","10010864498196","10010899312645","10010907111294","10010917057893","10010917289281","10010917442665","10010926235324","10010931489136","10010931825718","10010931975245","10010932468418","10010932551651","10010962307882","10010967310064","10010969010963","10010969278417","10010969308513","10010972458699","10011076140105","10011081314839","10011083274771","10011107367173","10011111115907","10011132132886","10160002563907","10160002582230","10160002700206","10160002712684","10650001129305","10710001797254","10710003082774","11520001252355","12170001881669","12440002145073","12440002152425","12440002158783","12440002176663","12440003799580","12440003949312","10010722128379","10010732491713","10010736394935","10010742167315","10011176034674","10011508076317","10011885138407","10011912794244","10012025133008","10012096109072","10012149137665","10012176291642","10012215040921","10012269133868","10012269439976","10012269501961","10012325524235","10012500295709","10012646076190","10012682297149","10012758558944","11100038433163","11100042657613","12650007649133","12650010705381","12800018433496","12800042597108","12800066301095","12800070604025","13030008068548","13030014019978","10012452796419","10012528182813","10012794659434","10012228106432","10012758462742","10012694239634","10012398241618","10011772150571","10011888268231","10012528148692","10012001251507","10012740762529","10012671289123","10012001299720","10012373045244","10012350241662","10012533061218","10011662416152","10012564342619","10012373069468","10012358147693","10011872526360","10012851215294","10011989095444","10012667603647","10011864124978","10012572330105","11100020233959","11100044063435","11100019562771","13030008188633","13030008952875","13030015143743","12650009850646","12650009916440","12650010829661","12650010777051","12650011249195","12650011183735","12650011519924","10012883622721","10012001295022","10012528147686","10012001139733","10012248581991","12800091028103","11100044342950","10012405078878","10012001541529","10012335578702","10012642600431","10011651060274","14430004317811","10012001461419","11100041339887","10012226310917"};
		for(String candID : cids)
		{
			ArrayList<ArrayList<String>> result = new ArrayList<ArrayList<String>>();
			try {
				result = FeaturesPhoneFile.getFeatures(new File(outputFolder+candID+".txt"),property);
				printFeature(result,outputFolder+candID+".csv");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}
	}
	
	static void printFeature(ArrayList<ArrayList<String>> features, String pathName) throws IOException
	{
		PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(pathName)));
		for(int i=0;i<features.size();i++)
		{
			for(int j=0;j<features.get(i).size();j++)
				pw.print(features.get(i).get(j)+",");
				pw.println();
		}
		pw.close();	
	}*/
	
}                                                                                                                                                                                    
