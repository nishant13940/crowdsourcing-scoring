package svarUpdated;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

//import com.sun.xml.internal.bind.v2.runtime.property.Property;
//import com.sun.xml.internal.bind.v2.runtime.unmarshaller.XsiNilLoader.Array;

public class FeaturesPhoneFile {
		
	static HashMap<String, String> constMap = new HashMap<String, String>();
	static HashMap<String, String> constMapError = new HashMap<String, String>();
	
	public static ArrayList<ArrayList<String>> getFeatures(File file, svar.Property property) throws IOException
	{
		buildConstMap(property.HTKConstantPath+"constList.csv");
		buildConstMapError(property.HTKConstantPath+"constList2.csv");
		
		Scanner scanner = new Scanner(file);
		ArrayList<String> features = new ArrayList<String>();
		
		ArrayList<Double> diffArrAll = new ArrayList<Double>();
		ArrayList<Double> diffArr2All = new ArrayList<Double>();
		ArrayList<Double> diffArr3All = new ArrayList<Double>();
		ArrayList<Double> diffArr4All = new ArrayList<Double>();

		ArrayList<Double> diffArr = new ArrayList<Double>();
		ArrayList<Double> diffArr2 = new ArrayList<Double>();
		ArrayList<Double> diffArr3 = new ArrayList<Double>();
		ArrayList<Double> diffArr4 = new ArrayList<Double>();
				
		ArrayList<Double> diffArrV = new ArrayList<Double>();
		ArrayList<Double> diffArr2V = new ArrayList<Double>();
		ArrayList<Double> diffArr3V = new ArrayList<Double>();
		ArrayList<Double> diffArr4V = new ArrayList<Double>();
		
		ArrayList<Double> diffArrC = new ArrayList<Double>();
		ArrayList<Double> diffArr2C = new ArrayList<Double>();
		ArrayList<Double> diffArr3C = new ArrayList<Double>();
		ArrayList<Double> diffArr4C = new ArrayList<Double>();
		
		ArrayList<Double> diffArrS = new ArrayList<Double>();
		ArrayList<Double> diffArr2S = new ArrayList<Double>();
		ArrayList<Double> diffArr3S = new ArrayList<Double>();
		ArrayList<Double> diffArr4S = new ArrayList<Double>();
		
		ArrayList<Double> diffArrE = new ArrayList<Double>();
		ArrayList<Double> diffArr2E = new ArrayList<Double>();
		ArrayList<Double> diffArr3E = new ArrayList<Double>();
		ArrayList<Double> diffArr4E = new ArrayList<Double>();		
		
		ArrayList<Double> diffArrE2 = new ArrayList<Double>();
		ArrayList<Double> diffArr2E2 = new ArrayList<Double>();
		ArrayList<Double> diffArr3E2 = new ArrayList<Double>();
		ArrayList<Double> diffArr4E2 = new ArrayList<Double>();		

		ArrayList<ArrayList<String>> allfeatures = new ArrayList<ArrayList<String>>();
		
		boolean parseFile = false;
		while(scanner.hasNextLine())
		{
			String line = scanner.nextLine();
			String currentUtterance = "";
			
			if(line.contains("UtteranceStartID"))
			{	
				currentUtterance = line.split(":")[1];
				features.add(currentUtterance); // Adding utterance ID
				
				diffArrAll.clear();		// Cleaning the arraylist
				diffArr2All.clear();
				diffArr3All.clear();
				diffArr4All.clear();
				
				diffArr.clear();		// Cleaning the arraylist
				diffArr2.clear();
				diffArr3.clear();
				diffArr4.clear();
				
				
				diffArrV.clear();		// Cleaning the arraylist
				diffArr2V.clear();
				diffArr3V.clear();
				diffArr4V.clear();
				
				diffArrC.clear();		// Cleaning the arraylist
				diffArr2C.clear();
				diffArr3C.clear();
				diffArr4C.clear();
				
				diffArrS.clear();		// Cleaning the arraylist
				diffArr2S.clear();
				diffArr3S.clear();
				diffArr4S.clear();


				diffArrE.clear();		// Cleaning the arraylist
				diffArr2E.clear();
				diffArr3E.clear();
				diffArr4E.clear();
//								

				diffArrE2.clear();		// Cleaning the arraylist
				diffArr2E2.clear();
				diffArr3E2.clear();
				diffArr4E2.clear();

				parseFile = true;	// Start parsing
			}			
			else if(line.contains("UtteranceEnd")) // File Read
			{			
				features.add((String.valueOf(mean(diffArrAll))));
				features.add(String.valueOf(mean(diffArr2All)));
				features.add(String.valueOf(mean(diffArr3All)));
				features.add(String.valueOf(mean(diffArr4All)));

				features.add((String.valueOf(sum(diffArrAll))));
				features.add(String.valueOf(sum(diffArr2All)));
				features.add(String.valueOf(sum(diffArr3All)));
				features.add(String.valueOf(sum(diffArr4All)));
				
				features.add((String.valueOf(mean(diffArr))));
				features.add(String.valueOf(mean(diffArr2)));
				features.add(String.valueOf(mean(diffArr3)));
				features.add(String.valueOf(mean(diffArr4)));

				features.add((String.valueOf(sum(diffArr))));
				features.add(String.valueOf(sum(diffArr2)));
				features.add(String.valueOf(sum(diffArr3)));
				features.add(String.valueOf(sum(diffArr4)));
								
				features.add((String.valueOf(mean(diffArrV))));
				features.add(String.valueOf(mean(diffArr2V)));
				features.add(String.valueOf(mean(diffArr3V)));
				features.add(String.valueOf(mean(diffArr4V)));

				features.add((String.valueOf(sum(diffArrV))));
				features.add(String.valueOf(sum(diffArr2V)));
				features.add(String.valueOf(sum(diffArr3V)));
				features.add(String.valueOf(sum(diffArr4V)));

				features.add((String.valueOf(mean(diffArrC))));
				features.add(String.valueOf(mean(diffArr2C)));
				features.add(String.valueOf(mean(diffArr3C)));
				features.add(String.valueOf(mean(diffArr4C)));

				features.add((String.valueOf(sum(diffArrC))));
				features.add(String.valueOf(sum(diffArr2C)));
				features.add(String.valueOf(sum(diffArr3C)));
				features.add(String.valueOf(sum(diffArr4C)));

				features.add((String.valueOf(mean(diffArrS))));
				features.add(String.valueOf(mean(diffArr2S)));
				features.add(String.valueOf(mean(diffArr3S)));
				features.add(String.valueOf(mean(diffArr4S)));

				features.add((String.valueOf(sum(diffArrS))));
				features.add(String.valueOf(sum(diffArr2S)));
				features.add(String.valueOf(sum(diffArr3S)));
				features.add(String.valueOf(sum(diffArr4S)));

				
				features.add((String.valueOf(mean(diffArrE))));
				features.add(String.valueOf(mean(diffArr2E)));
				features.add(String.valueOf(mean(diffArr3E)));
				features.add(String.valueOf(mean(diffArr4E)));

				features.add((String.valueOf(sum(diffArrE))));
				features.add(String.valueOf(sum(diffArr2E)));
				features.add(String.valueOf(sum(diffArr3E)));
				features.add(String.valueOf(sum(diffArr4E)));

				features.add((String.valueOf(mean(diffArrE2))));
				features.add(String.valueOf(mean(diffArr2E2)));
				features.add(String.valueOf(mean(diffArr3E2)));
				features.add(String.valueOf(mean(diffArr4E2)));

				features.add((String.valueOf(sum(diffArrE2))));
				features.add(String.valueOf(sum(diffArr2E2)));
				features.add(String.valueOf(sum(diffArr3E2)));
				features.add(String.valueOf(sum(diffArr4E2)));

				parseFile = false; // Disable parsing				
				
				allfeatures.add(new ArrayList<String>(features));
				features.clear();
			}
			else if(parseFile)
			{
				double llOther = 0;
				String temp[] = line.split(" ");
				for(int i=4;i<temp.length;i++)
					llOther+=Double.parseDouble(temp[i]);
				
				String phoneType = (constMap.get(temp[2]));
				String phone = temp[2];

				String phoneType2 = (constMapError.get(temp[2]));
	
				diffArrAll.add(Double.parseDouble(temp[3])/(llOther));
				diffArr2All.add(Double.parseDouble(temp[3])-(llOther));
				diffArr3All.add(Double.parseDouble(temp[3])-(llOther/(temp.length-4)));
				diffArr4All.add(Double.parseDouble(temp[3])/((llOther)/(temp.length-4)));				
				
				if(!(phone.equals("sil") | phone.equals("sp")))		
				{
					diffArr.add(Double.parseDouble(temp[3])/(llOther));
					diffArr2.add(Double.parseDouble(temp[3])-(llOther));
					diffArr3.add(Double.parseDouble(temp[3])-(llOther/(temp.length-4)));
					diffArr4.add(Double.parseDouble(temp[3])/((llOther)/(temp.length-4)));				
				}
				
				if(phoneType.equals("N"))
				{
					diffArrS.add(Double.parseDouble(temp[3])/(llOther));
					diffArr2S.add(Double.parseDouble(temp[3])-(llOther));
					diffArr3S.add(Double.parseDouble(temp[3])-(llOther/(temp.length-4)));
					diffArr4S.add(Double.parseDouble(temp[3])/((llOther)/(temp.length-4)));					
				}
				if(phoneType.equals("V"))
				{
					diffArrV.add(Double.parseDouble(temp[3])/(llOther));
					diffArr2V.add(Double.parseDouble(temp[3])-(llOther));
					diffArr3V.add(Double.parseDouble(temp[3])-(llOther/(temp.length-4)));
					diffArr4V.add(Double.parseDouble(temp[3])/((llOther)/(temp.length-4)));					
				}
				if(phoneType.equals("C"))
				{
					diffArrC.add(Double.parseDouble(temp[3])/(llOther));
					diffArr2C.add(Double.parseDouble(temp[3])-(llOther));
					diffArr3C.add(Double.parseDouble(temp[3])-(llOther/(temp.length-4)));
					diffArr4C.add(Double.parseDouble(temp[3])/((llOther)/(temp.length-4)));					
				}
//				System.out.println(phoneType2);
				if(phoneType2!=null && phoneType2.equals("E"))
				{
					diffArrE.add(Double.parseDouble(temp[3])/(llOther));
					diffArr2E.add(Double.parseDouble(temp[3])-(llOther));
					diffArr3E.add(Double.parseDouble(temp[3])-(llOther/(temp.length-4)));
					diffArr4E.add(Double.parseDouble(temp[3])/((llOther)/(temp.length-4)));					
				}				
				else if(phoneType2!=null && phoneType2.equals("E2"))
				{
					diffArrE2.add(Double.parseDouble(temp[3])/(llOther));
					diffArr2E2.add(Double.parseDouble(temp[3])-(llOther));
					diffArr3E2.add(Double.parseDouble(temp[3])-(llOther/(temp.length-4)));
					diffArr4E2.add(Double.parseDouble(temp[3])/((llOther)/(temp.length-4)));					
				}				
								
			}
		}		
		//print(allfeatures, output);
		
		return allfeatures;
	}
	
	private static double sum(ArrayList<Double> arr)
	{
		double sum=0;
		for(double num:arr)
			sum+=num;
		
		return sum;
	}
	

	private static ArrayList<String> orderMap(HashMap<Double,Double> arr,String utteranceID)
	{
		ArrayList<String> temp = new ArrayList<String>();
		temp.add(utteranceID);						
			for(double i=1;i<=39;i++)
			{
				if(arr.get(i)!=null)
				{
					temp.add(arr.get(i).toString());
				}
				else
					temp.add("0");
			}
		return temp;
	}

	
	private static double mean(ArrayList<Double> arr)
	{
		double sum=0;
		for(double num:arr)
			sum+=num;
		
		return sum/arr.size();
	}
	
	public static void print(ArrayList<ArrayList<String>> features,String output) throws IOException
	{
		PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(output)));
		
		for(int i=0;i<features.size();i++)
		{
			for(int j=0;j<features.get(i).size();j++)
				pw.print(features.get(i).get(j)+",");
			pw.println();
		}
		pw.close();
		
	}
	
	public static void buildConstMap(String path) throws FileNotFoundException
	{
		Scanner scanner = new Scanner(new File(path));
		
		while(scanner.hasNextLine())
		{
			String[] temp = scanner.nextLine().split(",");
			constMap.put(temp[0],temp[1]);			
		}
	}
	
	public static void buildConstMapError(String path) throws FileNotFoundException
	{
		Scanner scanner = new Scanner(new File(path));
		
		while(scanner.hasNextLine())
		{
			String[] temp = scanner.nextLine().split(",");
			constMapError.put(temp[0],temp[1]);			
		}
	}
	
	
}
